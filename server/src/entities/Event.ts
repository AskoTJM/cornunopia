import {
    Entity,
    Column,
    PrimaryGeneratedColumn,
    ManyToOne,
    DeleteDateColumn,
    OneToMany,
} from "typeorm";
import { IsString, IsDate, IsOptional } from "class-validator";
import { Type } from "class-transformer";
import { Account } from "./Account";
import { EventAccount } from "./EventAccount";
import { EventGroup } from "./EventGroup";

@Entity()
export class Event {
    @PrimaryGeneratedColumn()
    event_id: number;

    @ManyToOne(() => Account, (account) => account.events_owned)
    owner: Account;

    @IsString()
    @Column({ nullable: false })
    name: string;

    @IsOptional()
    @IsString()
    @Column({ nullable: true })
    description: string;

    @Type(() => Date)
    @IsDate()
    @Column({ nullable: false, type: "timestamptz" })
    start_timedate: Date;

    @Type(() => Date)
    @IsOptional()
    @IsDate()
    @Column({ nullable: true, type: "timestamptz" })
    end_timedate: Date;

    @IsOptional()
    @IsString()
    @Column({ nullable: true })
    category: string;

    @IsOptional()
    @IsString()
    @Column({ nullable: true })
    visibility: string;

    @IsOptional()
    @IsString()
    @Column({ nullable: true })
    location: string;

    @OneToMany(() => EventAccount, (eventAccount) => eventAccount.event, {
        cascade: true,
    })
    participants: EventAccount[];

    @IsOptional()
    @IsString()
    @Column({ nullable: true })
    cancelled: string;

    @DeleteDateColumn()
    deleted_date: Date;

    @OneToMany(() => EventGroup, (eventGroup) => eventGroup.event, {
        cascade: true,
    })
    groups: EventGroup[];
}
