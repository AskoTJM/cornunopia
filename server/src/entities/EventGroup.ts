import { Entity, ManyToOne, ManyToMany, JoinTable } from "typeorm";
import { Account } from "./Account";
import { Event } from "./Event";
import { Group } from "./Group";

@Entity()
export class EventGroup {
    @ManyToOne(() => Event, (event) => event.groups, {
        primary: true,
        orphanedRowAction: "delete",
    })
    event: Event;

    @ManyToOne(() => Group, (group) => group.group_events, {
        primary: true,
        orphanedRowAction: "delete",
    })
    group: Group;

    @ManyToMany(
        () => Account,
        (account) => account.events_participated_via_group,
    )
    @JoinTable()
    participating_accounts: Account[];
}
