import { IsOptional } from "class-validator";
import { Column, Entity, ManyToOne } from "typeorm";
import { Account } from "./Account";
import { Group } from "./Group";

@Entity()
export class GroupAccount {
    @ManyToOne(() => Account, (account) => account.groups_participated, {
        primary: true,
        orphanedRowAction: "delete",
    })
    account: Account;

    @ManyToOne(() => Group, (group) => group.participants, {
        primary: true,
        orphanedRowAction: "delete",
    })
    group: Group;

    @ManyToOne(() => Account, (account) => account.account_id)
    inviter?: Account;

    @IsOptional()
    @Column({ nullable: true })
    invite?: boolean;

    @IsOptional()
    @Column({ nullable: true })
    participating?: boolean;

    @IsOptional()
    @Column({ nullable: true })
    blacklisted?: boolean;
}
