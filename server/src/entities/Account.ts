import {
    Entity,
    Column,
    PrimaryGeneratedColumn,
    OneToMany,
    ManyToMany,
} from "typeorm";
import {
    IsString,
    IsEmail,
    MaxLength,
    IsDate,
    IsOptional,
} from "class-validator";
import { Type } from "class-transformer";
import { MAX_NAME_LENGTH } from "../controllers/utils";
import { Event } from "./Event";
import { Group } from "./Group";
import { EventAccount } from "./EventAccount";
import { GroupAccount } from "./GroupAccount";
import { entityDateTransformer } from "./entityUtils";
import { EventGroup } from "./EventGroup";

@Entity()
export class Account {
    @PrimaryGeneratedColumn()
    account_id: number;

    @IsString()
    @IsEmail()
    @MaxLength(100)
    @Column({ nullable: false, unique: true })
    email: string;

    @MaxLength(MAX_NAME_LENGTH)
    @Column({ nullable: false })
    name: string;

    @Type(() => Date)
    @IsDate()
    @Column({
        nullable: false,
        type: "date",
        transformer: entityDateTransformer,
    })
    date_of_birth: Date;

    @IsOptional()
    @IsString()
    @Column({ nullable: true })
    sex: string;

    @IsOptional()
    @IsString()
    @Column({ nullable: true })
    description: string;

    @IsOptional()
    @IsDate()
    @Column({ nullable: true, type: "timestamptz" })
    last_login: Date | null;

    @IsOptional()
    @IsString()
    @Column({ nullable: true })
    profile_image: string | null;

    @OneToMany(() => EventAccount, (eventAccount) => eventAccount.account)
    @IsOptional()
    events_participated: EventAccount[];

    @OneToMany(() => EventAccount, (eventAccount) => eventAccount.inviter)
    @IsOptional()
    events_sent_invites: EventAccount[];

    @OneToMany(() => Event, (event) => event.owner)
    @IsOptional()
    events_owned: Event[];

    @OneToMany(() => GroupAccount, (groupAccount) => groupAccount.account)
    @IsOptional()
    groups_participated: GroupAccount[];

    @OneToMany(() => GroupAccount, (groupAccount) => groupAccount.inviter)
    @IsOptional()
    groups_sent_invites: GroupAccount[];

    @OneToMany(() => Group, (group) => group.owner)
    @IsOptional()
    groups_owned: Group[];

    @IsString()
    @Column({ select: false })
    hash: string;

    @IsString()
    @Column({ select: false })
    salt: string;

    @ManyToMany(
        () => EventGroup,
        (eventGroup) => eventGroup.participating_accounts,
    )
    events_participated_via_group: EventGroup[];
}
