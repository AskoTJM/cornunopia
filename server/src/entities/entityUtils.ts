export const entityDateTransformer = {
    from: (databaseValue: string): Date => new Date(databaseValue),
    to: (entityValue: Date | null): string =>
        entityValue instanceof Date ? entityValue.toISOString() : null,
};
