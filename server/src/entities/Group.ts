import {
    Entity,
    Column,
    PrimaryGeneratedColumn,
    ManyToOne,
    DeleteDateColumn,
    OneToMany,
} from "typeorm";
import { IsString, IsDate, MaxLength, IsOptional } from "class-validator";
import { Account } from "./Account";
import { GroupAccount } from "./GroupAccount";
import { EventGroup } from "./EventGroup";

@Entity()
export class Group {
    @PrimaryGeneratedColumn()
    group_id: number;

    @ManyToOne(() => Account, (account) => account.groups_owned)
    owner: Account;

    @IsString()
    @MaxLength(100)
    @Column({ nullable: false })
    group_name: string;

    @IsOptional()
    @IsString()
    @Column({ nullable: true })
    group_description: string;

    @IsString()
    @Column()
    category: string;

    @OneToMany(() => GroupAccount, (groupAccount) => groupAccount.group, {
        cascade: true,
    })
    participants: GroupAccount[];

    @IsString()
    @Column({ nullable: true })
    visibility: string;

    @IsDate()
    @IsOptional()
    @Column({ nullable: true, type: "timestamptz" })
    last_modified: Date | null;

    @DeleteDateColumn()
    deleted_date: Date;

    @OneToMany(() => EventGroup, (eventGroup) => eventGroup.group)
    group_events: EventGroup[];
}
