import { IsOptional } from "class-validator";
import { Column, Entity, ManyToOne } from "typeorm";
import { Account } from "./Account";
import { Event } from "./Event";

@Entity()
export class EventAccount {
    @ManyToOne(() => Account, (account) => account.events_participated, {
        primary: true,
        orphanedRowAction: "delete",
    })
    account: Account;

    @ManyToOne(() => Event, (event) => event.participants, {
        primary: true,
        orphanedRowAction: "delete",
    })
    event: Event;

    @ManyToOne(() => Account, (account) => account.account_id)
    inviter?: Account;

    @IsOptional()
    @Column({ nullable: true })
    invite?: boolean;

    @IsOptional()
    @Column({ nullable: true })
    participating?: boolean;

    @IsOptional()
    @Column({ nullable: true })
    blacklisted?: boolean;
}
