import passport from "passport";
import { Strategy as JwtStrategy, ExtractJwt } from "passport-jwt";
import { getRepository } from "typeorm";
import { Account } from "../entities/Account";
import { JWT_ALGORITHM } from "../constants";
import * as dotenv from "dotenv";
dotenv.config();

const options = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: process.env.SECRET,
    algorithms: [JWT_ALGORITHM],
};

export const verifyCallback = (
    jwtPayload: { sub: string },
    done: (error: unknown, user?: unknown) => void,
): void => {
    const accountId = jwtPayload.sub;
    getRepository(Account)
        .findOneOrFail(accountId)
        .then((account) => done(null, account))
        .catch(() => done(null, false));
};

passport.use(new JwtStrategy(options, verifyCallback));
