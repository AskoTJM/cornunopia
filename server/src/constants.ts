export enum ErrorMessages {
    SaveFailed = "Failed to save",
    UserNotFound = "User not found",
    ValidationFailed = "Validation failed",
    UnknownEndpoint = "Unknown endpoint",
    EmailIsTaken = "Email is taken",
    NameIsTaken = "Name is taken",
    MissingParameters = "Missing parameters",
    InternalServerError = "Internal server error",
    EventNotFound = "Event not found",
    CannotDelete = "Cannot delete",
    GroupNotFound = "Group not found",
    Unauthorized = "Unauthorized",
    Blacklisted = "Blacklisted",
    NotFound = "Not found",
    Forbidden = "Forbidden",
}

export enum StatusCodes {
    OK = 200,
    Created = 201,
    NoContent = 204,

    BadRequest = 400,
    Unauthorized = 401,
    Forbidden = 403,
    NotFound = 404,
    Conflict = 409,

    InternalServerError = 500,
}

export const JWT_ALGORITHM = "HS256";
export const PASSPORT_STRATEGY = "jwt";

export enum Visibility {
    Public = "Public",
    Private = "Private",
}
