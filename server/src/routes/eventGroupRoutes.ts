import { Router } from "express";
import { createEventGroup } from "../controllers/eventGroupControllers";
import isAuth from "./authMiddleware";

const getEventGroupRoutes = (): Router => {
    const router = Router();

    router.post("/event_group/:group_id/:event_id", isAuth(), createEventGroup);

    return router;
};

export default getEventGroupRoutes;
