import passport from "passport";
import { PASSPORT_STRATEGY } from "../constants";
import { Middleware } from "../types";

const isAuth = (): Middleware => {
    const middleware = passport.authenticate(PASSPORT_STRATEGY, {
        session: false,
    }) as Middleware;
    return middleware;
};

export default isAuth;
