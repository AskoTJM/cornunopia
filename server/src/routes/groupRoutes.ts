import { Router } from "express";
import {
    createGroup,
    updateGroup,
    getGroups,
    deleteGroup,
    getGroupById,
    getGroupParticipants,
} from "../controllers/groupControllers";
import isAuth from "./authMiddleware";

const getGroupRoutes = (): Router => {
    const router = Router();
    router.post("/group", isAuth(), createGroup);
    router.put("/group/:id", isAuth(), updateGroup);
    router.get("/group", isAuth(), getGroups);
    router.get("/group/:id", isAuth(), getGroupById);
    router.delete("/group/:id", isAuth(), deleteGroup);
    router.get("/group/:id/participants", isAuth(), getGroupParticipants);
    return router;
};

export default getGroupRoutes;
