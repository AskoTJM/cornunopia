import { Router } from "express";
import {
    createEvent,
    getEvents,
    getEventById,
    updateEvent,
    deleteEvent,
    cancelEvent,
    getNumberOfParticipants,
} from "../controllers/eventControllers";
import isAuth from "./authMiddleware";

const getEventRoutes = (): Router => {
    const router = Router();

    router.post("/event", isAuth(), createEvent);
    router.get("/event", isAuth(), getEvents);
    router.get("/event/:id", isAuth(), getEventById);
    router.put("/event/:id", isAuth(), updateEvent);
    router.patch("/event/:id", isAuth(), updateEvent);
    router.delete("/event/:id", isAuth(), deleteEvent);
    router.patch("/event/:id/cancel", isAuth(), cancelEvent);
    router.get(
        "/event/:id/num_participants",
        isAuth(),
        getNumberOfParticipants,
    );

    return router;
};

export default getEventRoutes;
