import { Router } from "express";
import {
    participateInEvent,
    unparticipateInEvent,
    declineInvite,
} from "../controllers/eventAccountControllers";
import isAuth from "./authMiddleware";

const getEventAccountRoutes = (): Router => {
    const router = Router();

    router.post(
        "/event_account/participate/:event_id",
        isAuth(),
        participateInEvent,
    );

    router.delete(
        "/event_account/participate/:event_id",
        isAuth(),
        unparticipateInEvent,
    );

    router.delete(
        "/event_account/decline_invite/:event_id",
        isAuth(),
        declineInvite,
    );

    return router;
};

export default getEventAccountRoutes;
