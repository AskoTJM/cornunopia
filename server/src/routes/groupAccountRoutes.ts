import { Router } from "express";
import {
    participateInGroup,
    unparticipateInGroup,
    declineInvite,
} from "../controllers/groupAccountControllers";
import isAuth from "./authMiddleware";

const getGroupAccountRoutes = (): Router => {
    const router = Router();

    router.post(
        "/group_account/participate/:group_id",
        isAuth(),
        participateInGroup,
    );
    router.delete(
        "/group_account/participate/:group_id",
        isAuth(),
        unparticipateInGroup,
    );
    router.delete(
        "/group_account/decline_invite/:group_id",
        isAuth(),
        declineInvite,
    );

    return router;
};

export default getGroupAccountRoutes;
