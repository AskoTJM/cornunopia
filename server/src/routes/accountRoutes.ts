import { Router } from "express";
import {
    getAccountById,
    createAccount,
    patchAccount,
    searchByEmailOrName,
    loginUser,
    getEventsParticipatedByAccount,
    getEventsWithPendingInvites,
    getGroupsWithPendingInvites,
} from "../controllers/accountControllers";
import isAuth from "./authMiddleware";

const getAccountRoutes = (): Router => {
    const router = Router();

    router.get("/user/:id", isAuth(), getAccountById);
    router.get("/search_user", isAuth(), searchByEmailOrName);
    router.post("/user", (request, response) => {
        void (async () => {
            await createAccount(request, response);
        })();
    });
    router.patch("/user/:id", isAuth(), patchAccount);
    router.post("/login", loginUser);
    router.get(
        "/user/:id/events_participated",
        isAuth(),
        getEventsParticipatedByAccount,
    );
    router.get(
        "/user/:id/pending_event_invites",
        isAuth(),
        getEventsWithPendingInvites,
    );
    router.get(
        "/user/:id/pending_group_invites",
        isAuth(),
        getGroupsWithPendingInvites,
    );

    return router;
};

export default getAccountRoutes;
