import { Request, Response, NextFunction } from "express";

export type Middleware = (
    request: Request,
    response: Response,
    next: NextFunction,
) => void;

export interface ILoginData {
    email: string;
    password: string;
}
