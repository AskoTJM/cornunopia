import "reflect-metadata";
import { createConnection } from "typeorm";
import express from "express";
import cors from "cors";
import getAccountRoutes from "./routes/accountRoutes";
import getEventRoutes from "./routes/eventRoutes";
import getGroupRoutes from "./routes/groupRoutes";
import { ErrorMessages, StatusCodes } from "./constants";
import passport from "passport";
import getEventAccountRoutes from "./routes/eventAccountRoutes";
import getGroupAccountRoutes from "./routes/groupAccountRoutes";
import getEventGroupRoutes from "./routes/eventGroupRoutes";

const PORT = 5000;

createConnection()
    .then(async () => {
        const app = express();
        await import("./config/passport");
        app.use(passport.initialize());
        app.use(express.json());
        app.use(express.urlencoded({ extended: true }));
        app.use(cors());

        app.use(getAccountRoutes());
        app.use(getEventRoutes());
        app.use(getGroupRoutes());
        app.use(getEventAccountRoutes());
        app.use(getGroupAccountRoutes());
        app.use(getEventGroupRoutes());

        app.use((request, response) => {
            response
                .status(StatusCodes.NotFound)
                .send(ErrorMessages.UnknownEndpoint);
        });
        app.listen(PORT);
    })
    .catch((error) => console.error("TypeORM connection error: ", error));
