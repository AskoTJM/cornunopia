import { getRepository, Repository } from "typeorm";
import { Request, Response } from "express";
import { Account } from "../entities/Account";
import { Event } from "../entities/Event";
import { plainToClass } from "class-transformer";
import {
    sendNotFound,
    sendBadRequest,
    sendInternalServerError,
    sendConflict,
    sendError,
    getUserIdFromRequest,
} from "./utils";
import { validate } from "class-validator";
import { ErrorMessages, StatusCodes } from "../constants";
import { EventAccount } from "../entities/EventAccount";

interface IEventWithInviteIds {
    event: Event;
    invitedAccountIds?: number[];
}

const createInvitedAccounts = (inviteIds: number[], event: Event) =>
    inviteIds && inviteIds.length > 0
        ? getRepository(Account)
              .findByIds(inviteIds)
              .then((invitedAccounts) =>
                  invitedAccounts.length !== inviteIds.length
                      ? Promise.reject(ErrorMessages.UserNotFound)
                      : invitedAccounts.map((account) =>
                            plainToClass(EventAccount, {
                                account,
                                event,
                                inviter: event.owner,
                                invite: true,
                            }),
                        ),
              )
        : Promise.resolve([] as EventAccount[]);

const createOrUpdateEvent = (requestBody: IEventWithInviteIds, event: Event) =>
    validate(event)
        .then((validationErrors) =>
            validationErrors.length > 0
                ? Promise.reject(ErrorMessages.ValidationFailed)
                : getRepository(Event).save(event),
        )
        .then((savedEvent) => {
            const invitedAccountsPromise = createInvitedAccounts(
                requestBody.invitedAccountIds,
                savedEvent,
            );
            return Promise.all([invitedAccountsPromise, savedEvent]);
        })
        .then(([invitedAccounts, savedEvent]) => {
            savedEvent.participants = invitedAccounts;
            return getRepository(Event).save(savedEvent);
        });

export const createEvent = (request: Request, response: Response): void => {
    const accountId = getUserIdFromRequest(request);
    const requestBody = request.body as IEventWithInviteIds;
    const event = plainToClass(Event, requestBody.event);
    getRepository(Account)
        .findOneOrFail(accountId)
        .then((ownerAccount) => {
            event.owner = ownerAccount;
            return createOrUpdateEvent(requestBody, event);
        })
        .then(() => response.status(StatusCodes.Created).send(event))
        .catch((error) => sendError(error, response));
};

export const getEvents = (request: Request, response: Response): void =>
    void getRepository(Event)
        .createQueryBuilder("event")
        .leftJoinAndSelect("event.owner", "owner")
        .leftJoinAndSelect("event.participants", "participants")
        .leftJoinAndSelect("participants.account", "account")
        .select([
            "event",
            "owner.name",
            "owner.account_id",
            "participants",
            "account.account_id",
            "account.name",
            "account.email",
        ])
        .orderBy("event.start_timedate", "DESC")
        .getMany()
        .then((events) => {
            response.send(events);
        })
        .catch(() => {
            sendInternalServerError(
                ErrorMessages.InternalServerError,
                response,
            );
        });

export const getEventById = (request: Request, response: Response): void => {
    const id = request.params.id;
    return id
        ? void getRepository(Event)
              .findOneOrFail(id)
              .then((event) => {
                  response.send(event);
              })
              .catch(() => {
                  sendNotFound(ErrorMessages.EventNotFound, response);
              })
        : sendBadRequest(ErrorMessages.MissingParameters, response);
};

export const updateEvent = (request: Request, response: Response): void => {
    const requestBody = request.body as IEventWithInviteIds;
    const newEvent = plainToClass(Event, requestBody.event);
    const eventRepository = getRepository(Event);
    const eventId = Number(request.params.id);
    eventRepository
        .findOneOrFail({
            where: { event_id: eventId },
            relations: ["owner"],
        })
        .then((event) => {
            const accountId = getUserIdFromRequest(request);
            const mergedEvent = eventRepository.merge(event, newEvent);
            return event.owner.account_id === accountId
                ? createOrUpdateEvent(requestBody, mergedEvent)
                : Promise.reject(ErrorMessages.Forbidden);
        })
        .then((savedEvent) =>
            response.status(StatusCodes.Created).send(savedEvent),
        )
        .catch((error) => sendError(error, response));
};

const softDeleteEventInDatabase = (
    eventId: number,
    eventRepository: Repository<Event>,
    response: Response,
) => {
    eventRepository
        .softDelete(eventId)
        .then(() => response.status(StatusCodes.NoContent).end())
        .catch(() =>
            sendInternalServerError(
                ErrorMessages.InternalServerError,
                response,
            ),
        );
};

export const deleteEvent = (request: Request, response: Response): void => {
    const eventRepository = getRepository(Event);
    const eventId = Number(request.params.id);
    eventRepository
        .findOneOrFail({
            where: { event_id: eventId },
            relations: ["owner", "participants"],
        })
        .then((event) => {
            const accountId = getUserIdFromRequest(request);
            return event.owner.account_id === accountId &&
                event.participants.length === 0
                ? softDeleteEventInDatabase(eventId, eventRepository, response)
                : sendConflict(ErrorMessages.CannotDelete, response);
        })
        .catch(() => {
            sendNotFound(ErrorMessages.EventNotFound, response);
        });
};

export const cancelEvent = (request: Request, response: Response): void => {
    const reasonToCancel = (request.body as { reasonToCancel: string })
        .reasonToCancel;
    const newEventData = { cancelled: reasonToCancel };
    const eventRepository = getRepository(Event);
    eventRepository
        .findOneOrFail({
            where: { event_id: Number(request.params.id) },
            relations: ["owner"],
        })
        .then((event) => {
            const mergedEvent = eventRepository.merge(event, newEventData);
            const accountId = getUserIdFromRequest(request);
            return event.owner.account_id === accountId
                ? eventRepository.save(mergedEvent)
                : Promise.reject(ErrorMessages.Forbidden);
        })
        .then(() => response.status(StatusCodes.OK).send(newEventData))
        .catch((error) => sendError(error, response));
};

export const getNumberOfParticipants = (
    request: Request,
    response: Response,
): void => {
    getRepository(Event)
        .createQueryBuilder("event")
        .select(["event", "eventaccount"])
        .innerJoin("event.participants", "eventaccount")
        .where("event.event_id = :eventId", { eventId: request.params.id })
        .andWhere("eventaccount.participating IS TRUE")
        .getOne()
        .then((event) =>
            response.send({
                numParticipants:
                    event && Array.isArray(event.participants)
                        ? event.participants.length
                        : 0,
            }),
        )
        .catch(() =>
            sendInternalServerError(
                ErrorMessages.InternalServerError,
                response,
            ),
        );
};
