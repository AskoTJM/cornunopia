import { getRepository } from "typeorm";
import { ErrorMessages, StatusCodes, Visibility } from "../constants";
import { GroupAccount } from "../entities/GroupAccount";
import { Request, Response } from "express";
import {
    getUserIdFromRequest,
    sendForbidden,
    sendInternalServerError,
    sendNotFound,
} from "./utils";
import { Account } from "../entities/Account";
import { Group } from "../entities/Group";

const saveGroupAccountToDatabase = (
    groupAccount: GroupAccount,
    response: Response,
) => {
    getRepository(GroupAccount)
        .save(groupAccount)
        .then(() => response.status(StatusCodes.Created).send(groupAccount))
        .catch(() =>
            sendInternalServerError(ErrorMessages.SaveFailed, response),
        );
};

const getGroupAndAccount = (
    groupId: number,
    accountId: number,
): Promise<[Group, Account]> => {
    const groupPromise = getRepository(Group).findOneOrFail(groupId);
    const accountPromise = getRepository(Account).findOneOrFail(accountId);
    return Promise.all([groupPromise, accountPromise]);
};

const canParticipateInGroup = (group: Group, groupAccount: GroupAccount) =>
    !groupAccount.blacklisted &&
    (group.visibility === Visibility.Public ||
        (group.visibility === Visibility.Private && groupAccount.invite));

const participateInPublicGroup = (
    response: Response,
    group: Group,
    account: Account,
) => {
    const groupAccount = getRepository(GroupAccount).create({
        group,
        account,
        inviter: account,
        participating: true,
        blacklisted: false,
    });
    return canParticipateInGroup(group, groupAccount)
        ? saveGroupAccountToDatabase(groupAccount, response)
        : sendForbidden(response);
};

const addParticipation = (groupAccount: GroupAccount) =>
    getRepository(GroupAccount).create({
        ...groupAccount,
        participating: true,
    });

const participateInPrivateGroup = (
    response: Response,
    group: Group,
    accountId: number,
) => {
    getRepository(GroupAccount)
        .findOneOrFail({
            where: { group: group.group_id, account: accountId },
            relations: ["group", "account"],
        })
        .then((groupAccount) =>
            canParticipateInGroup(group, groupAccount)
                ? saveGroupAccountToDatabase(
                      addParticipation(groupAccount),
                      response,
                  )
                : sendForbidden(response),
        )
        .catch(() => sendNotFound(ErrorMessages.NotFound, response));
};

export const participateInGroup = (
    request: Request,
    response: Response,
): void => {
    const groupId = Number(request.params.group_id);
    const accountId = getUserIdFromRequest(request);
    getGroupAndAccount(groupId, accountId)
        .then(([group, account]) =>
            group.visibility === Visibility.Public
                ? participateInPublicGroup(response, group, account)
                : participateInPrivateGroup(response, group, accountId),
        )
        .catch(() => sendNotFound(ErrorMessages.NotFound, response));
};

const deleteFoundGroupAccount = (
    foundGroupAccount: GroupAccount,
    response: Response,
): Promise<void> =>
    getRepository(GroupAccount)
        .delete(foundGroupAccount)
        .then(() => response.status(StatusCodes.NoContent).end())
        .catch(() =>
            sendInternalServerError(ErrorMessages.SaveFailed, response),
        );

export const unparticipateInGroup = (
    request: Request,
    response: Response,
): Promise<void> => {
    const groupId = Number(request.params.group_id);
    const accountId = getUserIdFromRequest(request);
    return getRepository(GroupAccount)
        .findOneOrFail({
            where: { group: groupId, account: accountId },
            relations: ["group", "account"],
        })
        .then((groupToDelete) =>
            deleteFoundGroupAccount(groupToDelete, response),
        )
        .catch(() => sendNotFound(ErrorMessages.NotFound, response));
};

const canDeclineInvite = (groupAccount: GroupAccount) =>
    groupAccount.group.visibility === Visibility.Private && groupAccount.invite;

const setInviteToFalse = (groupAccount: GroupAccount, response: Response) =>
    canDeclineInvite(groupAccount)
        ? saveGroupAccountToDatabase(
              { ...groupAccount, invite: false },
              response,
          )
        : sendForbidden(response);

export const declineInvite = (request: Request, response: Response): void => {
    const groupId = Number(request.params.group_id);
    const accountId = getUserIdFromRequest(request);
    getRepository(GroupAccount)
        .findOneOrFail({
            where: { group: groupId, account: accountId },
            relations: ["group", "account"],
        })
        .then((groupAccount) => setInviteToFalse(groupAccount, response))
        .catch(() => sendNotFound(ErrorMessages.NotFound, response));
};
