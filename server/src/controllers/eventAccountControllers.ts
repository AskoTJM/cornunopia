import { getRepository } from "typeorm";
import { ErrorMessages, StatusCodes, Visibility } from "../constants";
import { EventAccount } from "../entities/EventAccount";
import { Request, Response } from "express";
import {
    getUserIdFromRequest,
    sendError,
    sendForbidden,
    sendInternalServerError,
    sendNotFound,
} from "./utils";
import { Account } from "../entities/Account";
import { Event } from "../entities/Event";
import { EventGroup } from "../entities/EventGroup";
import { plainToClass } from "class-transformer";

const saveEventAccountToDatabase = (
    eventAccount: EventAccount,
    response: Response,
) => {
    getRepository(EventAccount)
        .save(eventAccount)
        .then(() => response.status(StatusCodes.Created).send(eventAccount))
        .catch(() =>
            sendInternalServerError(ErrorMessages.SaveFailed, response),
        );
};

const getEventAndAccount = (
    eventId: number,
    accountId: number,
): Promise<[Event, Account]> => {
    const eventPromise = getRepository(Event).findOneOrFail(eventId);
    const accountPromise = getRepository(Account).findOneOrFail(accountId);
    return Promise.all([eventPromise, accountPromise]);
};

const canParticipateInEvent = (event: Event, eventAccount: EventAccount) =>
    !eventAccount.blacklisted &&
    !event.cancelled &&
    (event.visibility === Visibility.Public ||
        (event.visibility === Visibility.Private && eventAccount.invite));

const participateInPublicEvent = (
    response: Response,
    event: Event,
    account: Account,
) => {
    const eventAccount = getRepository(EventAccount).create({
        event,
        account,
        inviter: account,
        participating: true,
        blacklisted: false,
    });
    return canParticipateInEvent(event, eventAccount)
        ? saveEventAccountToDatabase(eventAccount, response)
        : sendForbidden(response);
};

const addParticipation = (eventAccount: EventAccount) =>
    getRepository(EventAccount).create({
        ...eventAccount,
        participating: true,
    });

const participateInPrivateEvent = (
    response: Response,
    event: Event,
    accountId: number,
) => {
    getRepository(EventAccount)
        .findOneOrFail({
            where: { event: event.event_id, account: accountId },
            relations: ["event", "account"],
        })
        .then((eventAccount) =>
            canParticipateInEvent(event, eventAccount)
                ? saveEventAccountToDatabase(
                      addParticipation(eventAccount),
                      response,
                  )
                : sendForbidden(response),
        )
        .catch(() => sendNotFound(ErrorMessages.NotFound, response));
};

export const participateInEvent = (
    request: Request,
    response: Response,
): void => {
    const eventId = Number(request.params.event_id);
    const accountId = getUserIdFromRequest(request);
    getEventAndAccount(eventId, accountId)
        .then(([event, account]) =>
            event.visibility === Visibility.Public
                ? participateInPublicEvent(response, event, account)
                : participateInPrivateEvent(response, event, accountId),
        )
        .catch(() => sendNotFound(ErrorMessages.NotFound, response));
};

const unparticipateEventGroup = (
    response: Response,
    eventId: number,
    accountId: number,
) => {
    const eventGroupRepository = getRepository(EventGroup);
    return eventGroupRepository
        .createQueryBuilder("eventgroup")
        .innerJoinAndSelect("eventgroup.event", "event")
        .innerJoinAndSelect("eventgroup.group", "group")
        .innerJoinAndSelect("eventgroup.participating_accounts", "account")
        .where("event.event_id = :eventId", { eventId })
        .getOne()
        .then((eventGroup) => {
            const newEventGroup = plainToClass(EventGroup, {
                ...eventGroup,
                participating_accounts:
                    eventGroup.participating_accounts.filter(
                        (acc) => acc.account_id !== accountId,
                    ),
            });
            return eventGroupRepository.save(newEventGroup);
        })
        .then((savedEventGroup) => response.send(savedEventGroup));
};

const deleteFoundEventAcc = (
    foundEventAccount: EventAccount,
    response: Response,
): Promise<void> =>
    getRepository(EventAccount)
        .delete(foundEventAccount)
        .then(() => response.status(StatusCodes.NoContent).end())
        .catch(() =>
            sendInternalServerError(ErrorMessages.SaveFailed, response),
        );

export const unparticipateInEvent = (
    request: Request,
    response: Response,
): Promise<void> => {
    const eventId = Number(request.params.event_id);
    const accountId = getUserIdFromRequest(request);
    return void getRepository(EventAccount)
        .findOneOrFail({
            where: { event: eventId, account: accountId },
            relations: ["event", "account"],
        })
        .then((eventToDelete) => deleteFoundEventAcc(eventToDelete, response))
        .catch(() => unparticipateEventGroup(response, eventId, accountId))
        .catch((error) => sendError(error, response));
};

const canDeclineInvite = (eventAccount: EventAccount) =>
    eventAccount.event.visibility === Visibility.Private && eventAccount.invite;

const setInviteToFalse = (eventAccount: EventAccount, response: Response) =>
    canDeclineInvite(eventAccount)
        ? saveEventAccountToDatabase(
              { ...eventAccount, invite: false },
              response,
          )
        : sendForbidden(response);

export const declineInvite = (request: Request, response: Response): void => {
    const eventId = Number(request.params.event_id);
    const accountId = getUserIdFromRequest(request);
    getRepository(EventAccount)
        .findOneOrFail({
            where: { event: eventId, account: accountId },
            relations: ["event", "account"],
        })
        .then((eventAccount) => setInviteToFalse(eventAccount, response))
        .catch(() => sendNotFound(ErrorMessages.NotFound, response));
};
