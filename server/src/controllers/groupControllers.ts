import { getRepository, Repository } from "typeorm";
import { Request, Response } from "express";
import { Account } from "../entities/Account";
import { Group } from "../entities/Group";
import { plainToClass } from "class-transformer";
import {
    sendBadRequest,
    sendNotFound,
    sendInternalServerError,
    sendConflict,
    getUserIdFromRequest,
    sendError,
} from "./utils";
import { validate } from "class-validator";
import { ErrorMessages, StatusCodes } from "../constants";
import { GroupAccount } from "../entities/GroupAccount";

interface IGroupWithParticipantIds {
    group: Group;
    invitedAccountIds: number[] | false;
    participatingAccountIds: number[] | false;
}

const combineAndRemoveDuplicates = (array1: number[], array2: number[]) => {
    const set = new Set([...array1, ...array2]);
    return Array.from(set.values());
};

const createGroupAccount = (
    group: Group,
    account: Account,
    inviteIds: number[],
    participatingIds: number[],
) => {
    const accountId = account.account_id;
    const participating = participatingIds.includes(accountId);
    const invite = inviteIds.includes(accountId);
    const plain: GroupAccount = {
        account,
        group,
        inviter: group.owner,
        invite,
        participating,
    };
    return plainToClass(GroupAccount, plain);
};

const createParticipants = (
    requestBody: IGroupWithParticipantIds,
    group: Group,
) => {
    const inviteIds = requestBody.invitedAccountIds || [];
    const participatingIds = requestBody.participatingAccountIds || [];
    const ids = combineAndRemoveDuplicates(inviteIds, participatingIds);
    return ids.length > 0
        ? getRepository(Account)
              .findByIds(ids)
              .then((invitedAccounts) =>
                  invitedAccounts.length !== ids.length
                      ? Promise.reject(ErrorMessages.UserNotFound)
                      : invitedAccounts.map((account) =>
                            createGroupAccount(
                                group,
                                account,
                                inviteIds,
                                participatingIds,
                            ),
                        ),
              )
        : Promise.resolve([] as GroupAccount[]);
};

const createOrUpdateGroup = (
    requestBody: IGroupWithParticipantIds,
    group: Group,
) =>
    validate(group)
        .then((validationErrors) =>
            validationErrors.length > 0
                ? Promise.reject(ErrorMessages.ValidationFailed)
                : getRepository(Group).save(group),
        )
        .then((savedGroup) => {
            const participantsPromise =
                requestBody.invitedAccountIds === false &&
                requestBody.participatingAccountIds === false
                    ? Promise.resolve(savedGroup.participants)
                    : createParticipants(requestBody, savedGroup);
            return Promise.all([participantsPromise, savedGroup]);
        })
        .then(([participants, savedGroup]) => {
            savedGroup.participants = participants;
            return getRepository(Group).save(savedGroup);
        });

export const createGroup = (request: Request, response: Response): void => {
    const accountId = getUserIdFromRequest(request);
    const requestBody = request.body as IGroupWithParticipantIds;
    const group = plainToClass(Group, requestBody.group);
    getRepository(Account)
        .findOneOrFail(accountId)
        .then((ownerAccount) => {
            group.owner = ownerAccount;
            return createOrUpdateGroup(requestBody, group);
        })
        .then(() => response.status(StatusCodes.Created).send(group))
        .catch((error) => sendError(error, response));
};

export const getGroups = (request: Request, response: Response): void =>
    void getRepository(Group)
        .createQueryBuilder("group")
        .leftJoinAndSelect("group.owner", "owner")
        .leftJoinAndSelect("group.participants", "participants")
        .leftJoinAndSelect("participants.account", "account")
        .select([
            "group",
            "owner.name",
            "owner.account_id",
            "participants",
            "account.account_id",
            "account.name",
            "account.email",
        ])
        .orderBy("group.group_name")
        .getMany()
        .then((groups) => {
            response.send(groups);
        })
        .catch(() => {
            sendInternalServerError(
                ErrorMessages.InternalServerError,
                response,
            );
        });

export const getGroupById = (request: Request, response: Response): void => {
    const id = request.params.id;
    return id
        ? void getRepository(Group)
              .findOneOrFail(id)
              .then((group) => {
                  response.send(group);
              })
              .catch(() => {
                  sendNotFound(ErrorMessages.GroupNotFound, response);
              })
        : sendBadRequest(ErrorMessages.MissingParameters, response);
};

export const updateGroup = (request: Request, response: Response): void => {
    const requestBody = request.body as IGroupWithParticipantIds;
    const newGroup = plainToClass(Group, requestBody.group);
    const groupRepository = getRepository(Group);
    const groupId = Number(request.params.id);
    groupRepository
        .findOneOrFail({
            where: { group_id: groupId },
            relations: [
                "owner",
                "participants",
                "participants.account",
                "participants.group",
            ],
        })
        .then((group) => {
            const accountId = getUserIdFromRequest(request);
            const mergedGroup = groupRepository.merge(group, newGroup);
            return group.owner.account_id === accountId
                ? createOrUpdateGroup(requestBody, mergedGroup)
                : Promise.reject(ErrorMessages.Forbidden);
        })
        .then((savedGroup) =>
            response.status(StatusCodes.Created).send(savedGroup),
        )
        .catch((error) => sendError(error, response));
};

const softDeleteGroupInDatabase = (
    groupId: number,
    groupRepository: Repository<Group>,
    response: Response,
) => {
    groupRepository
        .softDelete(groupId)
        .then(() => response.status(StatusCodes.NoContent).end())
        .catch(() =>
            sendInternalServerError(
                ErrorMessages.InternalServerError,
                response,
            ),
        );
};

export const deleteGroup = (request: Request, response: Response): void => {
    const groupRepository = getRepository(Group);
    const groupId = Number(request.params.id);
    groupRepository
        .findOneOrFail({
            where: { group_id: groupId },
            relations: ["owner", "participants"],
        })
        .then((group) => {
            const accountId = getUserIdFromRequest(request);
            return group.owner.account_id === accountId &&
                group.participants.length === 0
                ? softDeleteGroupInDatabase(groupId, groupRepository, response)
                : sendConflict(ErrorMessages.CannotDelete, response);
        })
        .catch(() => {
            sendNotFound(ErrorMessages.GroupNotFound, response);
        });
};

const sendParticipants = (
    request: Request,
    response: Response,
    group?: Group,
) => {
    const accountId = getUserIdFromRequest(request);
    return !group
        ? response.send([])
        : group.owner.account_id !== accountId
        ? response.status(StatusCodes.Unauthorized).end()
        : response.send(group.participants);
};

export const getGroupParticipants = (
    request: Request,
    response: Response,
): void => {
    const groupId = Number(request.params.id);
    getRepository(Group)
        .createQueryBuilder("group")
        .select([
            "group",
            "groupaccount",
            "owner",
            "account.account_id",
            "account.name",
            "account.email",
        ])
        .innerJoin("group.participants", "groupaccount")
        .innerJoin("groupaccount.account", "account")
        .innerJoin("group.owner", "owner")
        .where("group.group_id = :groupId", { groupId })
        .andWhere("groupaccount.participating IS TRUE")
        .getOne()
        .then((group) => sendParticipants(request, response, group))
        .catch(() =>
            sendInternalServerError(
                ErrorMessages.InternalServerError,
                response,
            ),
        );
};
