import { Repository } from "typeorm";
import { Response, Request } from "express";
import { ErrorMessages, JWT_ALGORITHM, StatusCodes } from "../constants";
import crypto from "crypto";
import { Account } from "../entities/Account";
import jsonwebtoken from "jsonwebtoken";
import * as dotenv from "dotenv";
dotenv.config();

export const MAX_NAME_LENGTH = 20;

export const isValueInUse = <T>(
    propName: string,
    value: string,
    repository: Repository<T>,
): Promise<boolean> =>
    repository
        .findOneOrFail({ [propName]: value })
        .then(() => true)
        .catch(() => false);

export const sendBadRequest = (message: string, response: Response): void => {
    response.status(StatusCodes.BadRequest).send(message);
};

export const sendNotFound = (message: string, response: Response): void => {
    response.status(StatusCodes.NotFound).send(message);
};

export const sendConflict = (message: string, response: Response): void => {
    response.status(StatusCodes.Conflict).send(message);
};

export const sendInternalServerError = (
    message: string,
    response: Response,
): void => {
    response.status(StatusCodes.InternalServerError).send(message);
};

export const sendForbidden = (response: Response): void => {
    response.status(StatusCodes.Forbidden).end();
};

const HASH_ITERATIONS = 10000;
const HASH_LENGTH = 64;
const HASH_ALGORITHM = "sha512";
const HASH_STRING_ENCODING = "hex";
const SALT_LENGTH = 32;

export const generatePassword = (
    password: string,
): { salt: string; hash: string } => {
    const salt = crypto.randomBytes(SALT_LENGTH).toString(HASH_STRING_ENCODING);
    const hash = crypto
        .pbkdf2Sync(
            password,
            salt,
            HASH_ITERATIONS,
            HASH_LENGTH,
            HASH_ALGORITHM,
        )
        .toString(HASH_STRING_ENCODING);
    return {
        salt,
        hash,
    };
};

export const validatePassword = (
    password: string,
    hash: string,
    salt: string,
): boolean => {
    const hashVerify = crypto
        .pbkdf2Sync(
            password,
            salt,
            HASH_ITERATIONS,
            HASH_LENGTH,
            HASH_ALGORITHM,
        )
        .toString(HASH_STRING_ENCODING);
    return hash === hashVerify;
};

export const issueJWT = (
    account: Account,
): { token: string; account: Account } => {
    const expiresIn = process.env.JWT_EXPIRE;
    const payload = {
        sub: account.account_id,
        iat: Math.floor(Date.now() / 1000),
    };
    const signedToken = jsonwebtoken.sign(payload, process.env.SECRET, {
        expiresIn,
        algorithm: JWT_ALGORITHM,
    });
    return {
        token: "Bearer " + signedToken,
        account,
    };
};

export const getUserIdFromRequest = (request: Request): number => {
    const HEADER_NAME = "Authorization";
    const SPACE = " ";
    const authorizationHeader = String(request.get(HEADER_NAME));
    const encodedToken = authorizationHeader.split(SPACE)[1];
    const decodedToken = jsonwebtoken.decode(encodedToken, { complete: true });
    const payload = decodedToken.payload as { sub: string };
    return parseInt(payload.sub);
};

export const sendError = (error: unknown, response: Response): void => {
    switch (String(error)) {
        case ErrorMessages.ValidationFailed:
            sendBadRequest(ErrorMessages.ValidationFailed, response);
            break;
        case ErrorMessages.NotFound:
            sendNotFound(ErrorMessages.NotFound, response);
            break;
        case ErrorMessages.UserNotFound:
            sendNotFound(ErrorMessages.UserNotFound, response);
            break;
        case ErrorMessages.Forbidden:
            sendForbidden(response);
            break;
        default:
            sendInternalServerError(
                ErrorMessages.InternalServerError,
                response,
            );
            break;
    }
};
