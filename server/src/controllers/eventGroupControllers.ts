import { Group } from "../entities/Group";
import { Event } from "../entities/Event";
import { EventGroup } from "../entities/EventGroup";
import { getRepository } from "typeorm";
import { Request, Response } from "express";
import {
    sendInternalServerError,
    getUserIdFromRequest,
    sendError,
    sendForbidden,
} from "./utils";
import { ErrorMessages } from "../constants";

const addEventGroup = (group: Group, event: Event, response: Response) => {
    const eventGroupRepository = getRepository(EventGroup);
    const eventGroup = eventGroupRepository.create({
        event,
        group,
        participating_accounts: group.participants.map(
            (groupAccount) => groupAccount.account,
        ),
    });
    return eventGroupRepository
        .save(eventGroup)
        .then(() => response.send(eventGroup))
        .catch(() =>
            sendInternalServerError(
                ErrorMessages.InternalServerError,
                response,
            ),
        );
};

export const createEventGroup = (
    request: Request,
    response: Response,
): void => {
    const groupId = Number(request.params.group_id);
    const eventId = Number(request.params.event_id);
    const eventPromise = getRepository(Event).findOneOrFail(eventId);
    const groupPromise = getRepository(Group).findOneOrFail({
        where: { group_id: groupId },
        relations: ["owner", "participants", "participants.account"],
    });
    Promise.all([groupPromise, eventPromise])
        .then(([group, event]) => {
            const accountId = getUserIdFromRequest(request);
            return group.owner.account_id === accountId
                ? addEventGroup(group, event, response)
                : sendForbidden(response);
        })
        .catch((error) => sendError(error, response));
};
