import { getRepository, Repository } from "typeorm";
import { Request, Response } from "express";
import { validate } from "class-validator";
import { Account } from "../entities/Account";
import { plainToClass } from "class-transformer";
import {
    isValueInUse,
    sendBadRequest,
    sendNotFound,
    sendConflict,
    validatePassword,
    issueJWT,
    sendInternalServerError,
    getUserIdFromRequest,
} from "./utils";
import { ErrorMessages, StatusCodes } from "../constants";
import { generatePassword } from "./utils";
import { ILoginData } from "../types";
import { Event } from "../entities/Event";
import { Group } from "../entities/Group";

enum InviteMode {
    Invited,
    Participating,
}

export const getAccountById = (request: Request, response: Response): void => {
    const id = request.params.id;
    return id
        ? void getRepository(Account)
              .findOneOrFail(id)
              .then((account) => {
                  response.send(account);
              })
              .catch(() => {
                  sendNotFound(ErrorMessages.UserNotFound, response);
              })
        : sendBadRequest(ErrorMessages.MissingParameters, response);
};

const saveAccountInDatabase = (
    account: Account,
    response: Response,
    accountRepository: Repository<Account>,
) => {
    accountRepository
        .save(account)
        .then(() => {
            account.hash = null;
            account.salt = null;
            response.status(StatusCodes.Created).send(account);
        })
        .catch(() => sendBadRequest(ErrorMessages.SaveFailed, response));
};

const createAccountEntity = (accountData: Account) =>
    plainToClass(Account, {
        email: accountData.email,
        name: accountData.name,
        date_of_birth: new Date(accountData.date_of_birth),
        sex: accountData.sex || "",
        description: accountData.description || "",
        last_login: new Date(),
        profile_image: accountData.profile_image,
        salt: accountData.salt,
        hash: accountData.hash,
    });

const createNewAccount = (
    accountData: Account,
    response: Response,
    accountRepository: Repository<Account>,
) => {
    const account = createAccountEntity(accountData);
    saveAccountInDatabase(account, response, accountRepository);
};

export const createAccount = async (
    request: Request,
    response: Response,
): Promise<void> => {
    const account = plainToClass(Account, request.body);
    const { salt, hash } = generatePassword(
        (request.body as ILoginData).password,
    );
    account.salt = salt;
    account.hash = hash;
    const accountRepository = getRepository(Account);
    return (await validate(account)).length > 0
        ? sendBadRequest(ErrorMessages.ValidationFailed, response)
        : (await isValueInUse("email", account.email, accountRepository))
        ? sendConflict(ErrorMessages.EmailIsTaken, response)
        : (await isValueInUse("name", account.name, accountRepository))
        ? sendConflict(ErrorMessages.NameIsTaken, response)
        : createNewAccount(account, response, accountRepository);
};

const validateAndUpdateAccount = async (
    mergedAccount: Account,
    response: Response,
    accountRepository: Repository<Account>,
) => {
    validate(mergedAccount)
        .then((validatorErrors) =>
            validatorErrors.length > 0
                ? sendBadRequest(ErrorMessages.ValidationFailed, response)
                : saveAccountInDatabase(
                      mergedAccount,
                      response,
                      accountRepository,
                  ),
        )
        .catch(() => undefined);
};

const isEmailInUseByOtherAccounts = async (
    accountId: number,
    newAccount: Account,
    accountRepository: Repository<Account>,
): Promise<boolean> =>
    newAccount.email
        ? await accountRepository
              .findOneOrFail({ email: newAccount.email })
              .then(
                  (otherAccountWithSameEmail) =>
                      accountId !== otherAccountWithSameEmail.account_id,
              )
              .catch(() => false)
        : false;

const createAccountDataForPatching = (requestBody: unknown) => {
    const account = plainToClass(Account, requestBody);
    delete account.hash;
    delete account.salt;
    if (account.date_of_birth && typeof account.date_of_birth === "string")
        account.date_of_birth = new Date(account.date_of_birth);
    return account;
};

const findAccountWithHashAndSalt = (
    accountId: number,
    accountRepository: Repository<Account>,
): Promise<Account> =>
    accountRepository
        .createQueryBuilder("account")
        .where("account.account_id = :accountId", { accountId })
        .addSelect("account.hash")
        .addSelect("account.salt")
        .getOne();

export const patchAccount = (request: Request, response: Response): void => {
    const newAccount = createAccountDataForPatching(request.body);
    const accountRepository = getRepository(Account);
    const accountId = Number(request.params.id);
    return accountId && accountId === getUserIdFromRequest(request)
        ? void findAccountWithHashAndSalt(accountId, accountRepository)
              .then(async (account) =>
                  (await isEmailInUseByOtherAccounts(
                      accountId,
                      newAccount,
                      accountRepository,
                  ))
                      ? sendConflict(ErrorMessages.EmailIsTaken, response)
                      : void validateAndUpdateAccount(
                            accountRepository.merge(account, newAccount),
                            response,
                            accountRepository,
                        ),
              )
              .catch(() => sendNotFound(ErrorMessages.UserNotFound, response))
        : response.status(StatusCodes.Unauthorized).end();
};

export const searchByEmailOrName = (
    request: Request,
    response: Response,
): void => {
    const maxNumberOfResults = 7;
    const searchString = String(request.query.search);
    return searchString
        ? void getRepository(Account)
              .createQueryBuilder("account")
              .where("account.name ILIKE :name OR account.email ILIKE :email", {
                  name: `${searchString}%`,
                  email: `${searchString}%`,
              })
              .orderBy("account.name", "ASC")
              .limit(maxNumberOfResults)
              .getMany()
              .then((accounts) => {
                  response.send(accounts);
              })
        : void response.send([]);
};

const checkPasswordAndGetJWT = (
    account: Account,
    password: string,
    response: Response,
) => {
    const hash = account.hash;
    const salt = account.salt;
    delete account.hash;
    delete account.salt;
    return !hash || !salt
        ? sendInternalServerError(ErrorMessages.InternalServerError, response)
        : validatePassword(password, hash, salt)
        ? response.status(StatusCodes.OK).json(issueJWT(account))
        : response.status(StatusCodes.Unauthorized).end();
};

export const loginUser = (request: Request, response: Response): void => {
    const loginData = request.body as ILoginData;
    const email = loginData.email;
    const password = loginData.password;
    return email && password
        ? void getRepository(Account)
              .createQueryBuilder("account")
              .where("account.email = :email", { email })
              .addSelect("account.hash")
              .addSelect("account.salt")
              .getOne()
              .then((account) =>
                  checkPasswordAndGetJWT(account, password, response),
              )
              .catch(() => sendNotFound(ErrorMessages.UserNotFound, response))
        : sendBadRequest(ErrorMessages.MissingParameters, response);
};

const extractEventsParticipated = (
    inviteMode: InviteMode,
    account?: Account,
): Event[] => {
    const eventsViaGroup =
        account &&
        inviteMode === InviteMode.Participating &&
        account.groups_participated
            ? account.groups_participated
                  .map((groupAccount) =>
                      groupAccount.group.group_events.map(
                          (eventGroup) => eventGroup.event,
                      ),
                  )
                  .flat()
            : [];
    const basicEvents = account
        ? account.events_participated.map((eventAccount) => eventAccount.event)
        : [];
    return [...basicEvents, ...eventsViaGroup];
};

const getEventParticipantsData = (
    response: Response,
    accountId: number,
    inviteMode: InviteMode,
) => {
    const queryBuilder = getRepository(Account)
        .createQueryBuilder("account")
        .select([
            "account",
            "eventaccount",
            "event",
            "owner.name",
            "groupaccount",
            "group",
            "eventgroup",
            "eventviagroup",
        ])
        .innerJoin("account.events_participated", "eventaccount")
        .innerJoin("eventaccount.event", "event")
        .innerJoin("event.owner", "owner")
        .innerJoin("account.groups_participated", "groupaccount")
        .innerJoin("groupaccount.group", "group")
        .innerJoin("group.group_events", "eventgroup")
        .leftJoin("eventgroup.event", "eventviagroup")
        .where("account.account_id = :accountId", { accountId })
        .andWhere("eventaccount.blacklisted IS NOT TRUE")
        .andWhere(
            `eventaccount.participating ${
                inviteMode === InviteMode.Participating
                    ? "IS TRUE"
                    : "IS NOT TRUE"
            }`,
        );

    if (inviteMode === InviteMode.Invited)
        queryBuilder.andWhere("eventaccount.invite IS TRUE");

    queryBuilder
        .orderBy("event.start_timedate", "DESC")
        .getOne()
        .then((account) =>
            response.send(extractEventsParticipated(inviteMode, account)),
        )
        .catch(() =>
            sendInternalServerError(
                ErrorMessages.InternalServerError,
                response,
            ),
        );
};

export const getEventsParticipatedByAccount = (
    request: Request,
    response: Response,
): void => {
    const accountId = Number(request.params.id);
    return accountId && accountId === getUserIdFromRequest(request)
        ? getEventParticipantsData(
              response,
              accountId,
              InviteMode.Participating,
          )
        : response.status(StatusCodes.Unauthorized).end();
};

export const getEventsWithPendingInvites = (
    request: Request,
    response: Response,
): void => {
    const accountId = Number(request.params.id);
    return accountId && accountId === getUserIdFromRequest(request)
        ? getEventParticipantsData(response, accountId, InviteMode.Invited)
        : response.status(StatusCodes.Unauthorized).end();
};

const extractGroupsParticipated = (account?: Account): Group[] =>
    account
        ? account.groups_participated.map((groupAccount) => groupAccount.group)
        : [];

export const getGroupsWithPendingInvites = (
    request: Request,
    response: Response,
): void => {
    const accountId = Number(request.params.id);
    getRepository(Account)
        .createQueryBuilder("account")
        .select(["account", "groupaccount", "group", "owner.name"])
        .innerJoin("account.groups_participated", "groupaccount")
        .innerJoin("groupaccount.group", "group")
        .innerJoin("group.owner", "owner")
        .where("account.account_id = :accountId", { accountId })
        .andWhere("groupaccount.invite IS TRUE")
        .andWhere("groupaccount.blacklisted IS NOT TRUE")
        .andWhere("groupaccount.participating IS NOT TRUE")
        .orderBy("group.group_name")
        .getOne()
        .then((account) => response.send(extractGroupsParticipated(account)))
        .catch(() =>
            sendInternalServerError(
                ErrorMessages.InternalServerError,
                response,
            ),
        );
};
