import { typeORMMockRepository, getMockQueryBuilder } from "../test/testUtils";

export const getRepository = jest.fn().mockReturnValue(typeORMMockRepository);
export const Entity = (): unknown => jest.fn();
export const PrimaryGeneratedColumn = (): unknown => jest.fn();
export const Column = (): unknown => jest.fn();
export const ManyToOne = (): unknown => jest.fn();
export const OneToMany = (): unknown => jest.fn();
export const ManyToMany = (): unknown => jest.fn();
export const JoinTable = (): unknown => jest.fn();
export const DeleteDateColumn = (): unknown => jest.fn();
export const createQueryBuilder = (): unknown => getMockQueryBuilder([{}]);
