import { ValidationError } from "class-validator";

export const validate = jest.fn().mockResolvedValue([] as ValidationError[]);
export const IsString = (): unknown => jest.fn();
export const IsOptional = (): unknown => jest.fn();
export const IsDate = (): unknown => jest.fn();
export const MaxLength = (): unknown => jest.fn();
export const IsEmail = (): unknown => jest.fn();
