const mockAuthenticate = jest.fn();
jest.mock("passport", () => ({
    authenticate: mockAuthenticate,
}));

import { PASSPORT_STRATEGY } from "../src/constants";
import isAuth from "../src/routes/authMiddleware";

test("Should check if is authenticated", () => {
    isAuth();

    expect(mockAuthenticate).toBeCalledTimes(1);
    const args = mockAuthenticate.mock.calls[0] as unknown[];
    expect(args[0]).toBe(PASSPORT_STRATEGY);
});
