jest.mock("../src/controllers/utils.ts", () => ({
    getUserIdFromRequest: jest.fn(() => ACCOUNT_ID),
}));

import "reflect-metadata";
import {
    declineInvite,
    participateInGroup,
    unparticipateInGroup,
} from "../src/controllers/groupAccountControllers";
import { Request } from "express";
import { Group } from "../src/entities/Group";
import {
    flushPromises,
    mockResponse,
    mockSend,
    typeORMMockRepository,
    mockStatus,
} from "./testUtils";
import { getRepository, Repository } from "typeorm";
import { Account } from "../src/entities/Account";
import { GroupAccount } from "../src/entities/GroupAccount";
import { StatusCodes, Visibility } from "../src/constants";

const ACCOUNT_ID = 1;
const mockAccount = new Account();
mockAccount.account_id = ACCOUNT_ID;
mockAccount.email = "mail@mail.com";
mockAccount.name = "Test User";

const GROUP_ID = 1;
const mockGroup = new Group();
mockGroup.group_id = GROUP_ID;
mockGroup.owner = mockAccount;
mockGroup.group_name = "Test group";
mockGroup.visibility = Visibility.Public;
mockGroup.participants = [];

const mockGroupAccount = new GroupAccount();
mockGroupAccount.account = mockAccount;
mockGroupAccount.group = mockGroup;

beforeEach(() => {
    jest.clearAllMocks();
});

const mockRequest = {
    params: {
        group_id: GROUP_ID,
    },
} as unknown as Request;

const mockAccountFindOneOrFail = jest.fn().mockResolvedValue(mockAccount);
const mockAccountRepository = {
    findOneOrFail: mockAccountFindOneOrFail,
} as unknown as Repository<Account>;

const applyMockGetRepository = (
    groupRepositoryArg: Repository<Group>,
    groupAccountRepositoryArg: Repository<GroupAccount>,
) => {
    (getRepository as jest.Mock).mockImplementation((entity: unknown) =>
        entity === Group
            ? groupRepositoryArg
            : entity === Account
            ? mockAccountRepository
            : groupAccountRepositoryArg,
    );
};

test("Should participate in a private group", async () => {
    const mockGroupAccountWithInvite: GroupAccount = {
        account: mockAccount,
        group: null,
        invite: true,
    };
    const mockGroupPrivate: Group = {
        ...mockGroup,
        visibility: Visibility.Private,
        participants: [mockGroupAccountWithInvite],
    };
    mockGroupAccountWithInvite.group = mockGroupPrivate;

    const mockGroupRepository = {
        findOneOrFail: jest.fn().mockResolvedValue(mockGroupPrivate),
    } as unknown as Repository<Group>;

    const mockGroupAccountRepository = {
        create: jest.fn((entity: GroupAccount) => ({ ...entity })),
        save: jest.fn().mockResolvedValue({}),
        findOneOrFail: jest.fn().mockResolvedValue(mockGroupAccountWithInvite),
    } as unknown as Repository<GroupAccount>;

    applyMockGetRepository(mockGroupRepository, mockGroupAccountRepository);

    participateInGroup(mockRequest, mockResponse);
    await flushPromises();

    expect(mockResponse.send).toHaveBeenCalledTimes(1);
    const sentGroupAccount = mockSend.mock.calls[0][0] as GroupAccount;
    expect(sentGroupAccount.account).toBe(mockAccount);
    expect(sentGroupAccount.group).toBe(mockGroupPrivate);
    expect(sentGroupAccount.participating).toBe(true);
});

test("Should participate in a public group", async () => {
    const mockGroupRepository = {
        findOneOrFail: jest.fn().mockResolvedValue(mockGroup),
    } as unknown as Repository<Group>;

    const mockGroupAccountRepository = {
        create: jest.fn((entity: GroupAccount) => ({ ...entity })),
        save: jest.fn().mockResolvedValue({}),
    } as unknown as Repository<GroupAccount>;

    applyMockGetRepository(mockGroupRepository, mockGroupAccountRepository);

    participateInGroup(mockRequest, mockResponse);
    await flushPromises();

    expect(mockResponse.send).toHaveBeenCalledTimes(1);
    const sentGroupAccount = mockSend.mock.calls[0][0] as GroupAccount;
    expect(sentGroupAccount.account).toBe(mockAccount);
    expect(sentGroupAccount.group).toBe(mockGroup);
    expect(sentGroupAccount.participating).toBe(true);
});

test("Should opt out participation in a group", async () => {
    const mockRepository = {
        ...typeORMMockRepository,
        delete: jest.fn().mockResolvedValue(mockGroupAccount),
    };

    (getRepository as jest.Mock).mockImplementation(() => mockRepository);

    void unparticipateInGroup(mockRequest, mockResponse);
    await flushPromises();

    expect(mockRepository.findOneOrFail.mock.calls.length).toBe(1);
    expect(mockRepository.delete.mock.calls.length).toBe(1);
    expect(mockStatus.mock.calls[0][0]).toBe(StatusCodes.NoContent);
});

test("Should decline an invite to a group", async () => {
    const mockGroupAccountInvited = new GroupAccount();
    const mockGroupPrivate: Group = {
        ...mockGroup,
        visibility: Visibility.Private,
    };
    mockGroupAccountInvited.account = mockAccount;
    mockGroupAccountInvited.group = mockGroupPrivate;
    mockGroupAccountInvited.invite = true;

    const mockRepository = {
        ...typeORMMockRepository,
        save: jest.fn().mockResolvedValue(mockGroupAccountInvited),
        findOneOrFail: jest.fn().mockResolvedValue(mockGroupAccountInvited),
    };

    (getRepository as jest.Mock).mockImplementation(() => mockRepository);

    declineInvite(mockRequest, mockResponse);
    await flushPromises();

    const sentGroupAccount = mockSend.mock.calls[0][0] as GroupAccount;
    expect(sentGroupAccount.invite).toBe(false);
    expect(sentGroupAccount.group).toBe(mockGroupPrivate);
});
