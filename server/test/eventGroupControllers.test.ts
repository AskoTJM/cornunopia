jest.mock("../src/controllers/utils.ts", () => ({
    getUserIdFromRequest: jest.fn(() => OWNER_ID),
}));

import "reflect-metadata";
import { createEventGroup } from "../src/controllers/eventGroupControllers";
import {
    flushPromises,
    mockResponse,
    mockSend,
    typeORMMockRepository,
} from "./testUtils";
import { Request } from "express";
import { getRepository } from "typeorm";
import { Event } from "../src/entities/Event";
import { Group } from "../src/entities/Group";
import { EventGroup } from "../src/entities/EventGroup";

const OWNER_ID = 1;
const EVENT_ID = 1;
const mockEvent = {
    event_id: EVENT_ID,
    name: "Event name",
    start_timedate: new Date(),
    owner: { account_id: OWNER_ID },
} as Event;

const GROUP_ID = 2;
const mockGroup = {
    group_id: GROUP_ID,
    group_name: "Group name",
    owner: { account_id: OWNER_ID },
    participants: [],
} as Group;

test("Should create an EventGroup in database", async () => {
    const mockRequest = {
        params: {
            group_id: GROUP_ID,
            event_id: EVENT_ID,
        },
    } as unknown as Request;

    const mockGroupRepository = {
        findOneOrFail: jest.fn().mockResolvedValue(mockGroup),
    };
    const mockEventRepository = {
        findOneOrFail: jest.fn().mockResolvedValue(mockEvent),
    };
    const mockEventGroupRepository = typeORMMockRepository;

    (getRepository as jest.Mock).mockImplementation((entity: unknown) =>
        entity === Event
            ? mockEventRepository
            : entity === Group
            ? mockGroupRepository
            : entity === EventGroup
            ? mockEventGroupRepository
            : {},
    );

    createEventGroup(mockRequest, mockResponse);
    await flushPromises();

    expect(mockSend).toHaveBeenCalledTimes(1);
    const sentEventGroup = mockSend.mock.calls[0][0] as EventGroup;
    expect(sentEventGroup.event.name).toEqual(mockEvent.name);
    expect(sentEventGroup.group.group_name).toEqual(mockGroup.group_name);
});
