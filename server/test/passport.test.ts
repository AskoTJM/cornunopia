jest.mock("passport", () => ({
    use: jest.fn(),
}));
jest.mock("passport-jwt", () => ({
    Strategy: class {} as typeof passport.Strategy,
    ExtractJwt: {
        fromAuthHeaderAsBearerToken: jest.fn(),
    },
}));

import "reflect-metadata";
import { Account } from "../src/entities/Account";
import { flushPromises, typeORMMockRepository } from "./testUtils";
import { getRepository, Repository } from "typeorm";
import { verifyCallback } from "../src/config/passport";
import passport from "passport";

const ACCOUNT_ID = 1;
const ACCOUNT_ID_STRING = `${ACCOUNT_ID}`;
const mockAccount = {
    account_id: ACCOUNT_ID,
    name: "Test User",
    email: "test@mail.com",
    date_of_birth: new Date(0),
} as Account;

beforeEach(() => {
    jest.clearAllMocks();
});

test("Should verify account in passport callback", async () => {
    const mockJWTPayload = {
        sub: ACCOUNT_ID_STRING,
    };

    const mockFindOneOrFail = jest.fn().mockResolvedValue(mockAccount);
    const mockRepository = {
        ...typeORMMockRepository,
        findOneOrFail: mockFindOneOrFail,
    } as unknown as Repository<Event>;

    (getRepository as jest.Mock).mockImplementation(() => mockRepository);
    const mockDone = jest.fn();

    verifyCallback(mockJWTPayload, mockDone);
    await flushPromises();

    expect(mockFindOneOrFail).toHaveBeenCalledWith(ACCOUNT_ID_STRING);
    expect(mockDone).toHaveBeenCalledWith(null, mockAccount);
});
