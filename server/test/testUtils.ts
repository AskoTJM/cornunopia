import { Response } from "express";
import { QueryBuilder } from "typeorm";

export const flushPromises = (): Promise<unknown> =>
    new Promise((resolve) => setImmediate(resolve));

export const mockEnd = jest.fn(() => undefined);

export const mockSend = jest.fn((_body?: unknown) => undefined);

export const mockJson = jest.fn();

export const mockStatus = jest.fn((_code: number) => ({
    end: mockEnd,
    send: mockSend,
    json: mockJson,
}));

export const mockResponse = {
    status: mockStatus,
    send: mockSend,
} as unknown as Response;

export const typeORMMockRepository = {
    findOneOrFail: jest.fn().mockResolvedValue({}),
    merge: jest.fn(
        (entity1, entity2) => ({ ...entity1, ...entity2 } as unknown),
    ),
    save: jest.fn(async (entity: unknown) => entity),
    softDelete: jest.fn(async (entity: unknown) => entity),
    create: jest.fn((object: Record<string, unknown>) => ({ ...object })),
    findByIds: jest.fn().mockResolvedValue({}),
};

export const getMockQueryBuilder = <T>(
    mockReturnMany: T[],
): QueryBuilder<T> => {
    const mockQueryBuilder = {
        leftJoinAndSelect: jest.fn().mockReturnThis(),
        leftJoin: jest.fn().mockReturnThis(),
        innerJoin: jest.fn().mockReturnThis(),
        innerJoinAndSelect: jest.fn().mockReturnThis(),
        select: jest.fn().mockReturnThis(),
        getMany: jest.fn().mockResolvedValue(mockReturnMany),
        getOne: jest.fn().mockResolvedValue(mockReturnMany[0]),
        where: jest.fn().mockReturnThis(),
        andWhere: jest.fn().mockReturnThis(),
        addSelect: jest.fn().mockReturnThis(),
        limit: jest.fn().mockReturnThis(),
        orderBy: jest.fn().mockReturnThis(),
    };
    return mockQueryBuilder as unknown as QueryBuilder<T>;
};
