jest.mock("../src/controllers/utils.ts", () => ({
    getUserIdFromRequest: jest.fn(() => ACCOUNT_ID),
}));

import "reflect-metadata";
import {
    declineInvite,
    participateInEvent,
    unparticipateInEvent,
} from "../src/controllers/eventAccountControllers";
import { Request } from "express";
import { Event } from "../src/entities/Event";
import {
    flushPromises,
    mockResponse,
    mockSend,
    typeORMMockRepository,
    mockStatus,
} from "./testUtils";
import { getRepository, Repository } from "typeorm";
import { Account } from "../src/entities/Account";
import { EventAccount } from "../src/entities/EventAccount";
import { StatusCodes, Visibility } from "../src/constants";

const ACCOUNT_ID = 1;
const mockAccount = new Account();
mockAccount.account_id = ACCOUNT_ID;
mockAccount.email = "mail@mail.com";
mockAccount.name = "Test User";

const EVENT_ID = 1;
const mockEvent = new Event();
mockEvent.event_id = EVENT_ID;
mockEvent.owner = mockAccount;
mockEvent.name = "Test event";
mockEvent.start_timedate = new Date();
mockEvent.visibility = Visibility.Public;
mockEvent.cancelled = null;
mockEvent.participants = [];

const mockEventAccount = new EventAccount();
mockEventAccount.account = mockAccount;
mockEventAccount.event = mockEvent;

beforeEach(() => {
    jest.clearAllMocks();
});

const mockRequest = {
    params: {
        event_id: EVENT_ID,
    },
} as unknown as Request;

const mockAccountFindOneOrFail = jest.fn().mockResolvedValue(mockAccount);
const mockAccountRepository = {
    findOneOrFail: mockAccountFindOneOrFail,
} as unknown as Repository<Account>;

const applyMockGetRepository = (
    eventRepositoryArg: Repository<Event>,
    eventAccountRepositoryArg: Repository<EventAccount>,
) => {
    (getRepository as jest.Mock).mockImplementation((entity: unknown) =>
        entity === Event
            ? eventRepositoryArg
            : entity === Account
            ? mockAccountRepository
            : eventAccountRepositoryArg,
    );
};

test("Should participate in a private event", async () => {
    const mockEventAccountWithInvite: EventAccount = {
        account: mockAccount,
        event: null,
        invite: true,
    };
    const mockEventPrivate: Event = {
        ...mockEvent,
        visibility: Visibility.Private,
        participants: [mockEventAccountWithInvite],
    };
    mockEventAccountWithInvite.event = mockEventPrivate;

    const mockEventRepository = {
        findOneOrFail: jest.fn().mockResolvedValue(mockEventPrivate),
    } as unknown as Repository<Event>;

    const mockEventAccountRepository = {
        create: jest.fn((entity: EventAccount) => ({ ...entity })),
        save: jest.fn().mockResolvedValue({}),
        findOneOrFail: jest.fn().mockResolvedValue(mockEventAccountWithInvite),
    } as unknown as Repository<EventAccount>;

    applyMockGetRepository(mockEventRepository, mockEventAccountRepository);

    participateInEvent(mockRequest, mockResponse);
    await flushPromises();

    expect(mockResponse.send).toHaveBeenCalledTimes(1);
    const sentEventAccount = mockSend.mock.calls[0][0] as EventAccount;
    expect(sentEventAccount.account).toBe(mockAccount);
    expect(sentEventAccount.event).toBe(mockEventPrivate);
    expect(sentEventAccount.participating).toBe(true);
});

test("Should participate in a public event", async () => {
    const mockEventRepository = {
        findOneOrFail: jest.fn().mockResolvedValue(mockEvent),
    } as unknown as Repository<Event>;

    const mockEventAccountRepository = {
        create: jest.fn((entity: EventAccount) => ({ ...entity })),
        save: jest.fn().mockResolvedValue({}),
    } as unknown as Repository<EventAccount>;

    applyMockGetRepository(mockEventRepository, mockEventAccountRepository);

    participateInEvent(mockRequest, mockResponse);
    await flushPromises();

    expect(mockResponse.send).toHaveBeenCalledTimes(1);
    const sentEventAccount = mockSend.mock.calls[0][0] as EventAccount;
    expect(sentEventAccount.account).toBe(mockAccount);
    expect(sentEventAccount.event).toBe(mockEvent);
    expect(sentEventAccount.participating).toBe(true);
});

test("Should delete an account as a participant in an event", async () => {
    const mockRepository = {
        ...typeORMMockRepository,
        delete: jest.fn().mockResolvedValue(mockEventAccount),
    };

    (getRepository as jest.Mock).mockImplementation(() => mockRepository);

    void unparticipateInEvent(mockRequest, mockResponse);
    await flushPromises();

    expect(mockRepository.findOneOrFail.mock.calls.length).toBe(1);
    expect(mockRepository.delete.mock.calls.length).toBe(1);
    expect(mockStatus.mock.calls[0][0]).toBe(StatusCodes.NoContent);
});

test("Should decline an invite", async () => {
    const mockEventAccountInvited = new EventAccount();
    const mockEventPrivate: Event = {
        ...mockEvent,
        visibility: Visibility.Private,
    };
    mockEventAccountInvited.account = mockAccount;
    mockEventAccountInvited.event = mockEventPrivate;
    mockEventAccountInvited.invite = true;

    const mockRepository = {
        ...typeORMMockRepository,
        save: jest.fn().mockResolvedValue(mockEventAccountInvited),
        findOneOrFail: jest.fn().mockResolvedValue(mockEventAccountInvited),
    };

    (getRepository as jest.Mock).mockImplementation(() => mockRepository);

    declineInvite(mockRequest, mockResponse);
    await flushPromises();

    const sentEventAccount = mockSend.mock.calls[0][0] as EventAccount;
    expect(sentEventAccount.invite).toBe(false);
    expect(sentEventAccount.event).toBe(mockEventPrivate);
});
