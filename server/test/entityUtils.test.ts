import { entityDateTransformer } from "../src/entities/entityUtils";

describe("Entity utility functions test", () => {
    test("Should transform a date using 'from' function", () => {
        const dateString = "2021-08-17 10:14:46.374+03";
        const correctDate = new Date(dateString);

        const resultDate = entityDateTransformer.from(dateString);

        expect(resultDate.getTime()).toEqual(correctDate.getTime());
    });

    test("Should transform a date using 'to' function", () => {
        const dateString = "2021-05-06";
        const date = new Date(dateString);

        const resultDateString = entityDateTransformer.to(date);

        expect(resultDateString).toEqual(date.toISOString());
    });

    test("Should transform a null value using 'to' function", () => {
        const resultDateString = entityDateTransformer.to(null);

        expect(resultDateString).toEqual(null);
    });
});
