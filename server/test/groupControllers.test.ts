jest.mock("../src/controllers/utils.ts", () => ({
    getUserIdFromRequest: jest.fn(() => ACCOUNT_ID),
}));

import "reflect-metadata";
import { Request } from "express";
import {
    getGroupById,
    deleteGroup,
    updateGroup,
    getGroups,
    createGroup,
    getGroupParticipants,
} from "../src/controllers/groupControllers";
import { getRepository, Repository } from "typeorm";
import { Group } from "../src/entities/Group";
import { StatusCodes } from "../src/constants";
import {
    flushPromises,
    mockSend,
    mockStatus,
    mockResponse,
    typeORMMockRepository,
    getMockQueryBuilder,
} from "./testUtils";
import { Account } from "../src/entities/Account";

const ACCOUNT_ID = 2;
const ACCOUNT_ID_2 = 3;
const GROUP_ID = 1;

const mockGroup = {
    group_id: GROUP_ID,
    owner: { account_id: ACCOUNT_ID },
    group_name: "name",
    group_description: "",
    category: "",
    participants: [],
    visibility: "",
} as Group;

const mockAccount = {
    account_id: ACCOUNT_ID,
    name: "Test User",
    email: "mail@mail.net",
    date_of_birth: new Date(0),
    hash: "00",
    salt: "00",
} as Account;

const mockAccount2 = {
    account_id: ACCOUNT_ID_2,
    name: "Test User 2",
    email: "mail2@mail.net",
    date_of_birth: new Date(0),
    hash: "00",
    salt: "00",
} as Account;

beforeEach(() => {
    jest.clearAllMocks();
});

test("Should create a group", async () => {
    const mockRequest = {
        body: {
            group: mockGroup,
            invitedAccountIds: [ACCOUNT_ID],
            participatingAccountIds: [ACCOUNT_ID_2],
        },
    } as Request;

    const mockAccountRepository = {
        ...typeORMMockRepository,
        findByIds: jest.fn().mockResolvedValue([mockAccount, mockAccount2]),
        findOneOrFail: jest.fn().mockResolvedValue(mockAccount),
    };

    (getRepository as jest.Mock).mockImplementation(
        () => mockAccountRepository,
    );

    createGroup(mockRequest, mockResponse);
    await flushPromises();

    expect(mockSend).toBeCalledTimes(1);
    const sentGroup = mockSend.mock.calls[0][0] as Group;
    expect(sentGroup.group_id).toBe(mockGroup.group_id);
    expect(sentGroup.group_name).toBe(mockGroup.group_name);

    expect(sentGroup.participants).toHaveLength(2);
    const invited = sentGroup.participants[0];
    const participant = sentGroup.participants[1];

    expect(invited.invite).toBe(true);
    expect(invited.participating).toBe(false);
    expect(invited.account).toEqual(mockAccount);
    expect(invited.group.group_name).toBe(mockGroup.group_name);

    expect(participant.invite).toBe(false);
    expect(participant.participating).toBe(true);
    expect(participant.account).toEqual(mockAccount2);
    expect(participant.group.group_name).toBe(mockGroup.group_name);
});

test("Should update a group", async () => {
    const updateData = {
        group_name: "Updated name",
        group_description: "Updated description",
    };
    const mockRequest = {
        params: {
            id: GROUP_ID,
        },
        body: {
            group: updateData,
            invitedAccountIds: [ACCOUNT_ID],
            participatingAccountIds: [ACCOUNT_ID_2],
        },
    } as unknown as Request;

    const mockAccountRepository = {
        ...typeORMMockRepository,
        findByIds: jest.fn().mockResolvedValue([mockAccount, mockAccount2]),
        findOneOrFail: jest.fn().mockResolvedValue(mockAccount),
    };

    const mockGroupRepository = {
        ...typeORMMockRepository,
        findOneOrFail: jest.fn().mockResolvedValue(mockGroup),
    };

    (getRepository as jest.Mock).mockImplementation((entity: unknown) =>
        entity === Group
            ? mockGroupRepository
            : entity === Account
            ? mockAccountRepository
            : {},
    );

    updateGroup(mockRequest, mockResponse);
    await flushPromises();

    expect(mockSend).toBeCalledTimes(1);
    const sentGroup = mockSend.mock.calls[0][0] as Group;
    expect(sentGroup.group_name).toBe(updateData.group_name);
    expect(sentGroup.group_description).toBe(updateData.group_description);

    expect(sentGroup.participants).toHaveLength(2);
    const invited = sentGroup.participants[0];
    const participant = sentGroup.participants[1];

    expect(invited.invite).toBe(true);
    expect(invited.participating).toBe(false);
    expect(invited.account).toEqual(mockAccount);
    expect(invited.group.group_name).toBe(updateData.group_name);

    expect(participant.invite).toBe(false);
    expect(participant.participating).toBe(true);
    expect(participant.account).toEqual(mockAccount2);
    expect(participant.group.group_name).toBe(updateData.group_name);
});

test("Should delete a group", async () => {
    const mockRequest = {
        params: {
            id: GROUP_ID,
        },
    } as unknown as Request;

    const mockRepository = {
        ...typeORMMockRepository,
        findOneOrFail: jest.fn().mockResolvedValue(mockGroup),
    };
    (getRepository as jest.Mock).mockImplementation(() => mockRepository);

    deleteGroup(mockRequest, mockResponse);
    await flushPromises();

    expect(mockRepository.findOneOrFail.mock.calls.length).toBe(1);
    expect(mockRepository.softDelete.mock.calls.length).toBe(1);
    expect(mockStatus.mock.calls[0][0]).toBe(StatusCodes.NoContent);
});

test("Should get an group by id", async () => {
    const mockRequest = {
        params: {
            id: GROUP_ID,
        },
    } as unknown as Request;

    const mockFindOneOrFail = jest.fn().mockResolvedValue(mockGroup);
    const mockRepository = {
        findOneOrFail: mockFindOneOrFail,
    } as unknown as Repository<Group>;

    (getRepository as jest.Mock).mockImplementation(() => mockRepository);

    getGroupById(mockRequest, mockResponse);
    await flushPromises();

    expect(mockFindOneOrFail).toHaveBeenCalledWith(GROUP_ID);
    expect(mockResponse.send).toHaveBeenCalledWith(mockGroup);
});

test("Should get all groups", async () => {
    const mockRequest = {} as Request;
    const mockRepository = {
        ...typeORMMockRepository,
        createQueryBuilder: () => getMockQueryBuilder([mockGroup]),
    } as unknown as Repository<Group>;

    (getRepository as jest.Mock).mockImplementation(() => mockRepository);

    getGroups(mockRequest, mockResponse);
    await flushPromises();

    expect(mockSend).toHaveBeenCalledWith([mockGroup]);
});

test("Should get the participants in a group", async () => {
    const mockGroupWithParticipants = {
        ...mockGroup,
        participants: [
            { account: mockAccount, group: mockGroup, participating: true },
            { account: mockAccount, group: mockGroup, participating: true },
        ],
    };
    const mockRequest = {
        params: { id: GROUP_ID },
    } as unknown as Request;

    const mockGroupRepository = {
        ...typeORMMockRepository,
        createQueryBuilder: () =>
            getMockQueryBuilder([mockGroupWithParticipants]),
    };

    (getRepository as jest.Mock).mockImplementation(() => mockGroupRepository);

    getGroupParticipants(mockRequest, mockResponse);
    await flushPromises();

    const sentAccounts = mockSend.mock.calls[0][0] as Account[];
    expect(sentAccounts[0]).toBe(mockGroupWithParticipants.participants[0]);
    expect(sentAccounts[1]).toBe(mockGroupWithParticipants.participants[1]);
});
