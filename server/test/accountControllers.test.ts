jest.mock("../src/controllers/utils");

import "reflect-metadata";
import {
    loginUser,
    getAccountById,
    searchByEmailOrName,
    getEventsParticipatedByAccount,
    patchAccount,
    getEventsWithPendingInvites,
    getGroupsWithPendingInvites,
} from "../src/controllers/accountControllers";
import { Request } from "express";
import { Account } from "../src/entities/Account";
import {
    getMockQueryBuilder,
    flushPromises,
    mockResponse,
    typeORMMockRepository,
    mockJson,
    mockStatus,
    mockSend,
} from "./testUtils";
import { getRepository, Repository } from "typeorm";
import {
    issueJWT,
    validatePassword,
    getUserIdFromRequest,
} from "../src/controllers/utils";
import { StatusCodes } from "../src/constants";
import { EventAccount } from "../src/entities/EventAccount";
import { Event } from "../src/entities/Event";
import { GroupAccount } from "../src/entities/GroupAccount";
import { Group } from "../src/entities/Group";

const ACCOUNT_ID = 1;
const EMAIL = "test@mail.com";
const PASSWORD = "1234";
const HASH = "abcd";
const SALT = "efgh";

const mockAccount = {
    account_id: ACCOUNT_ID,
    name: "Test User",
    email: EMAIL,
    date_of_birth: new Date(0),
    hash: HASH,
    salt: SALT,
} as Account;

const mockUserAccount = {
    account_id: 2,
    email: "user2@email.not",
    name: "testuser",
    date_of_birth: "1992-02-02",
    sex: "other",
    description: "SQL User Script Test 2",
    last_login: "2021-07-26T12:05:30.647Z",
    profile_image: null,
} as unknown as Account;

beforeEach(() => {
    jest.clearAllMocks();
});

describe("Account controller test", () => {
    test("Should log in user", async () => {
        const mockRequest = {
            body: { email: EMAIL, password: PASSWORD },
        } as unknown as Request;

        const mockRepository = {
            ...typeORMMockRepository,
            createQueryBuilder: () => getMockQueryBuilder([mockAccount]),
        } as unknown as Repository<Event>;

        (getRepository as jest.Mock).mockImplementation(() => mockRepository);

        const mockValidatePassword = (
            validatePassword as jest.Mock
        ).mockImplementation(() => true);

        const mockTokenObject = { token: "Bearer 123", account: mockAccount };

        const mockIssueJWT = (issueJWT as jest.Mock).mockImplementation(
            () => mockTokenObject,
        );

        loginUser(mockRequest, mockResponse);
        await flushPromises();

        expect(mockValidatePassword).toHaveBeenCalledWith(PASSWORD, HASH, SALT);
        expect(mockStatus).toHaveBeenCalledWith(StatusCodes.OK);
        expect(mockIssueJWT).toHaveBeenCalledWith(mockAccount);
        expect(mockJson).toHaveBeenCalledWith(mockTokenObject);
    });

    test("Should get user data by id", async () => {
        const mockRequest = {
            params: { id: 2 },
        } as unknown as Request;

        const mockFindOneOrFail = jest.fn().mockResolvedValue(mockUserAccount);

        const mockRepository = {
            findOneOrFail: mockFindOneOrFail,
        } as unknown as Repository<Account>;

        (getRepository as jest.Mock).mockImplementation(() => mockRepository);

        getAccountById(mockRequest, mockResponse);
        await flushPromises();

        expect(mockFindOneOrFail).toHaveBeenCalledWith(
            mockUserAccount.account_id,
        );
        expect(mockResponse.send).toHaveBeenCalledWith(mockUserAccount);
    });

    test("Should return found user by username or email", async () => {
        const mockRequest = {
            query: { search: "testuser" },
        } as unknown as Request;

        const mockRepository = {
            ...typeORMMockRepository,
            createQueryBuilder: () => getMockQueryBuilder([mockUserAccount]),
        } as unknown as Repository<Account>;

        (getRepository as jest.Mock).mockImplementation(() => mockRepository);

        searchByEmailOrName(mockRequest, mockResponse);
        await flushPromises();

        expect(mockResponse.send).toHaveBeenCalledWith([mockUserAccount]);
    });

    test("Should get events in which a user has participated", async () => {
        const mockRequest = {
            params: { id: ACCOUNT_ID },
        } as unknown as Request;

        const mockEvent = {
            event_id: 3,
            name: "Event name",
        } as Event;

        const mockEventAccount: EventAccount = {
            account: mockAccount,
            event: mockEvent,
            participating: true,
            blacklisted: false,
        };

        (getUserIdFromRequest as jest.Mock).mockImplementation(
            () => ACCOUNT_ID,
        );

        const mockAccountWithEventsParticipated = {
            ...mockAccount,
            events_participated: [mockEventAccount],
        } as Account;

        const mockRepository = {
            createQueryBuilder: () =>
                getMockQueryBuilder([mockAccountWithEventsParticipated]),
        } as unknown as Repository<Account>;

        (getRepository as jest.Mock).mockImplementation(() => mockRepository);

        getEventsParticipatedByAccount(mockRequest, mockResponse);
        await flushPromises();

        expect(mockResponse.send).toHaveBeenCalledWith([mockEvent]);
    });

    test("Should update an account", async () => {
        const mockBody = {
            email: "upated.email@mail.com",
            name: "Updated Name",
            description: "Updated description",
            date_of_birth: "2000-02-03",
        };

        const mockRequest = {
            params: { id: ACCOUNT_ID },
            body: mockBody,
        } as unknown as Request;

        const mockRepository = {
            ...typeORMMockRepository,
            createQueryBuilder: () => getMockQueryBuilder([mockAccount]),
            findOneOrFail: jest.fn().mockRejectedValue(null),
        } as unknown as Repository<Account>;

        (getRepository as jest.Mock).mockImplementation(() => mockRepository);

        (getUserIdFromRequest as jest.Mock).mockImplementation(
            () => ACCOUNT_ID,
        );

        patchAccount(mockRequest, mockResponse);
        await flushPromises();

        expect(mockStatus).toHaveBeenCalledWith(StatusCodes.Created);
        expect(mockSend).toHaveBeenCalledTimes(1);
        const updatedAccount = mockSend.mock.calls[0][0] as Account;
        expect(updatedAccount.email).toBe(mockBody.email);
        expect(updatedAccount.name).toBe(mockBody.name);
        expect(updatedAccount.description).toBe(mockBody.description);
        expect(updatedAccount.date_of_birth).toEqual(
            new Date(mockBody.date_of_birth),
        );
    });

    test("Should get account's pending event invites", async () => {
        const mockRequest = {
            params: { id: ACCOUNT_ID },
        } as unknown as Request;

        const mockEvent = {
            event_id: 3,
            name: "Event name",
        } as Event;

        const mockEventAccount: EventAccount = {
            account: mockAccount,
            event: mockEvent,
            invite: true,
            participating: false,
            blacklisted: false,
            inviter: mockAccount,
        };

        (getUserIdFromRequest as jest.Mock).mockImplementation(
            () => ACCOUNT_ID,
        );

        const mockAccountWithEventsParticipated = {
            ...mockAccount,
            events_participated: [mockEventAccount],
        } as Account;

        const mockRepository = {
            createQueryBuilder: () =>
                getMockQueryBuilder([mockAccountWithEventsParticipated]),
        } as unknown as Repository<Account>;

        (getRepository as jest.Mock).mockImplementation(() => mockRepository);

        (getUserIdFromRequest as jest.Mock).mockImplementation(
            () => ACCOUNT_ID,
        );

        getEventsWithPendingInvites(mockRequest, mockResponse);
        await flushPromises();

        expect(mockSend).toHaveBeenCalledTimes(1);
        const sentEvents = mockSend.mock.calls[0][0] as Event[];
        expect(sentEvents).toEqual([mockEvent]);
    });

    test("Should get account's pending group invites", async () => {
        const mockRequest = {
            params: { id: ACCOUNT_ID },
        } as unknown as Request;

        const mockGroup = {
            group_id: 3,
            group_name: "Group name",
        } as Group;

        const mockGroupAccount: GroupAccount = {
            account: mockAccount,
            group: mockGroup,
            invite: true,
            participating: false,
            blacklisted: false,
            inviter: mockAccount,
        };

        (getUserIdFromRequest as jest.Mock).mockImplementation(
            () => ACCOUNT_ID,
        );

        const mockAccountWithGroupsParticipated = {
            ...mockAccount,
            groups_participated: [mockGroupAccount],
        } as Account;

        const mockRepository = {
            createQueryBuilder: () =>
                getMockQueryBuilder([mockAccountWithGroupsParticipated]),
        } as unknown as Repository<Account>;

        (getRepository as jest.Mock).mockImplementation(() => mockRepository);

        (getUserIdFromRequest as jest.Mock).mockImplementation(
            () => ACCOUNT_ID,
        );

        getGroupsWithPendingInvites(mockRequest, mockResponse);
        await flushPromises();

        expect(mockSend).toHaveBeenCalledTimes(1);
        const sentGroups = mockSend.mock.calls[0][0] as Group[];
        expect(sentGroups).toEqual([mockGroup]);
    });
});
