jest.mock("crypto", () => ({
    randomBytes: jest.fn(() => Buffer.from(mockRandomBytes)),
    pbkdf2Sync: jest.fn(() => Buffer.from(mockPBKDF2Bytes)),
}));
jest.mock("jsonwebtoken", () => ({
    sign: jest.fn(() => mockToken),
    decode: jest.fn(() => ({ payload: { sub: ACCOUNT_ID } })),
}));

import {
    sendForbidden,
    sendConflict,
    generatePassword,
    validatePassword,
    issueJWT,
    getUserIdFromRequest,
    sendError,
} from "../src/controllers/utils";
import { ErrorMessages, StatusCodes } from "../src/constants";
import { mockEnd, mockSend, mockStatus, mockResponse } from "./testUtils";
import crypto from "crypto";
import { Account } from "../src/entities/Account";
import jsonwebtoken from "jsonwebtoken";
import { Request } from "express";

const HEX_ENCODING = "hex";
const mockRandomBytes = "ABCD";
const mockPBKDF2Bytes = "EFGH";
const mockPBKDF2BytesHex = Buffer.from(mockPBKDF2Bytes).toString(HEX_ENCODING);
const mockToken = "12345";
const mockEncodedToken = "56789";
const ACCOUNT_ID = 1;

beforeEach(() => {
    jest.clearAllMocks();
});

test("Should send status forbidden", () => {
    sendForbidden(mockResponse);
    expect(mockEnd.mock.calls.length).toBe(1);
    expect(mockStatus.mock.calls.length).toBe(1);
    expect(mockStatus.mock.calls[0][0]).toBe(StatusCodes.Forbidden);
});

test("Should send status conflict", () => {
    const mockMessage = "Mock message";
    sendConflict(mockMessage, mockResponse);

    expect(mockSend.mock.calls.length).toBe(1);
    expect(mockStatus.mock.calls.length).toBe(1);

    expect(mockStatus.mock.calls[0][0]).toBe(StatusCodes.Conflict);
    expect(mockSend.mock.calls[0][0]).toBe(mockMessage);
});

test("Should generate password hash and salt", () => {
    const password = "1234";
    const mockRandomBytesHex =
        Buffer.from(mockRandomBytes).toString(HEX_ENCODING);

    const { salt, hash } = generatePassword(password);

    expect(salt).toBe(mockRandomBytesHex);
    expect(hash).toBe(mockPBKDF2BytesHex);
    expect(crypto.pbkdf2Sync as jest.Mock).toBeCalledTimes(1);
    const args = (crypto.pbkdf2Sync as jest.Mock).mock.calls[0] as unknown[];
    expect(args[0]).toBe(password);
    expect(args[1]).toBe(salt);
});

test("Should validate password", () => {
    const password = "1234";
    const hash = mockPBKDF2BytesHex;
    const salt = "efgh";

    const isValid = validatePassword(password, hash, salt);

    expect(isValid).toBe(true);
    expect(crypto.pbkdf2Sync as jest.Mock).toBeCalledTimes(1);
    const args = (crypto.pbkdf2Sync as jest.Mock).mock.calls[0] as unknown[];
    expect(args[0]).toBe(password);
    expect(args[1]).toBe(salt);
});

test("Should issue a JWT token", () => {
    const mockAccount = { account_id: 1 } as Account;

    const result = issueJWT(mockAccount);

    expect(result.token).toBe("Bearer " + mockToken);
    expect(result.account.account_id).toBe(mockAccount.account_id);
    const args = (jsonwebtoken.sign as jest.Mock).mock.calls[0] as unknown[];
    expect((args[0] as { sub: number }).sub).toBe(mockAccount.account_id);
});

test("Should decode user ID from token in request", () => {
    const mockBearerHeader = "Bearer " + mockEncodedToken;
    const mockGetHeader = jest.fn(() => mockBearerHeader);
    const mockRequest = {
        get: mockGetHeader,
    } as unknown as Request;

    const userId = getUserIdFromRequest(mockRequest);

    expect(mockGetHeader).toHaveBeenCalledWith("Authorization");
    expect(userId).toBe(ACCOUNT_ID);
    const args = (jsonwebtoken.decode as jest.Mock).mock.calls[0] as unknown[];
    expect(args[0]).toBe(mockEncodedToken);
});

describe("Should send errors", () => {
    test.each([
        [ErrorMessages.ValidationFailed, StatusCodes.BadRequest],
        [ErrorMessages.NotFound, StatusCodes.NotFound],
        [ErrorMessages.UserNotFound, StatusCodes.NotFound],
        [ErrorMessages.InternalServerError, StatusCodes.InternalServerError],
    ])("Sending error %s", (errorMessage, statusCode) => {
        sendError(errorMessage, mockResponse);
        expect(mockSend).toHaveBeenCalledWith(errorMessage);
        expect(mockStatus).toHaveBeenCalledWith(statusCode);
    });

    test("Sending error Forbidden", () => {
        sendError(ErrorMessages.Forbidden, mockResponse);
        expect(mockStatus).toHaveBeenCalledWith(StatusCodes.Forbidden);
    });
});
