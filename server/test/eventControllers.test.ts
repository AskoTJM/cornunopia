jest.mock("../src/controllers/utils.ts", () => ({
    getUserIdFromRequest: jest.fn(() => ACCOUNT_ID),
}));

import "reflect-metadata";
import {
    getEvents,
    getEventById,
    createEvent,
    updateEvent,
    cancelEvent,
    getNumberOfParticipants,
} from "../src/controllers/eventControllers";
import { Request } from "express";
import { Event } from "../src/entities/Event";
import {
    flushPromises,
    getMockQueryBuilder,
    mockResponse,
    mockSend,
    typeORMMockRepository,
} from "./testUtils";
import { getRepository, Repository } from "typeorm";
import { Account } from "../src/entities/Account";

const ACCOUNT_ID = 1;
const EVENT_ID = 1;
const mockEvent = {
    event_id: EVENT_ID,
    owner: { account_id: ACCOUNT_ID },
    name: "test event",
    start_timedate: new Date(),
} as Event;
const mockEvents = [mockEvent];

const mockAccount = {
    account_id: ACCOUNT_ID,
    name: "Test User",
    email: "mail@mail.net",
    date_of_birth: new Date(0),
    hash: "00",
    salt: "00",
} as Account;

beforeEach(() => {
    jest.clearAllMocks();
});

test("Should get events", async () => {
    const mockRequest = {} as Request;
    const mockRepository = {
        ...typeORMMockRepository,
        createQueryBuilder: () => getMockQueryBuilder([mockEvent]),
    } as unknown as Repository<Event>;
    (getRepository as jest.Mock).mockImplementation(() => mockRepository);

    getEvents(mockRequest, mockResponse);
    await flushPromises();

    expect(mockResponse.send).toHaveBeenCalledWith(mockEvents);
});

test("Should get an event by id", async () => {
    const mockRequest = {
        params: {
            id: EVENT_ID,
        },
    } as unknown as Request;

    const mockFindOneOrFail = jest.fn().mockResolvedValue(mockEvent);
    const mockRepository = {
        findOneOrFail: mockFindOneOrFail,
    } as unknown as Repository<Event>;

    (getRepository as jest.Mock).mockImplementation(() => mockRepository);

    getEventById(mockRequest, mockResponse);
    await flushPromises();

    expect(mockFindOneOrFail).toHaveBeenCalledWith(EVENT_ID);
    expect(mockResponse.send).toHaveBeenCalledWith(mockEvent);
});

test("Should create an event", async () => {
    const mockRequest = {
        body: {
            event: mockEvent,
            invitedAccountIds: [ACCOUNT_ID],
        },
    } as Request;

    const mockAccountRepository = {
        ...typeORMMockRepository,
        findByIds: jest.fn().mockResolvedValue([mockAccount]),
        findOneOrFail: jest.fn().mockResolvedValue(mockAccount),
    } as unknown as Repository<Event>;

    (getRepository as jest.Mock).mockImplementation(
        () => mockAccountRepository,
    );

    createEvent(mockRequest, mockResponse);
    await flushPromises();

    expect(mockSend).toBeCalledTimes(1);
    const sentEvent = mockSend.mock.calls[0][0] as Event;
    expect(sentEvent.event_id).toBe(mockEvent.event_id);
    expect(sentEvent.name).toBe(mockEvent.name);

    expect(sentEvent.participants).toHaveLength(1);
    const participant = sentEvent.participants[0];
    expect(participant.invite).toBe(true);
    expect(participant.account).toEqual(mockAccount);
    expect(participant.event.name).toBe(mockEvent.name);
});

test("Should update an event", async () => {
    const updateData = {
        name: "Updated name",
        description: "Updated description",
    };
    const mockRequest = {
        params: {
            id: EVENT_ID,
        },
        body: {
            event: updateData,
            invitedAccountIds: [ACCOUNT_ID],
        },
    } as unknown as Request;

    const mockAccountRepository = {
        ...typeORMMockRepository,
        findByIds: jest.fn().mockResolvedValue([mockAccount]),
        findOneOrFail: jest.fn().mockResolvedValue(mockAccount),
    } as unknown as Repository<Event>;

    const mockEventRepository = {
        ...typeORMMockRepository,
        findOneOrFail: jest.fn().mockResolvedValue(mockEvent),
    } as unknown as Repository<Event>;

    (getRepository as jest.Mock).mockImplementation((entity: unknown) =>
        entity === Event
            ? mockEventRepository
            : entity === Account
            ? mockAccountRepository
            : {},
    );

    updateEvent(mockRequest, mockResponse);
    await flushPromises();

    expect(mockSend).toBeCalledTimes(1);
    const sentEvent = mockSend.mock.calls[0][0] as Event;
    expect(sentEvent.name).toBe(updateData.name);
    expect(sentEvent.description).toBe(updateData.description);

    expect(sentEvent.participants).toHaveLength(1);
    const participant = sentEvent.participants[0];
    expect(participant.invite).toBe(true);
    expect(participant.account).toEqual(mockAccount);
});

test("Should cancel an event", async () => {
    const reasonToCancel = "Reason";
    const mockRequest = {
        body: { reasonToCancel },
        params: { id: EVENT_ID },
    } as unknown as Request;

    const mockEventRepository = {
        ...typeORMMockRepository,
        findOneOrFail: jest.fn().mockResolvedValue(mockEvent),
    };

    (getRepository as jest.Mock).mockImplementation(() => mockEventRepository);

    cancelEvent(mockRequest, mockResponse);
    await flushPromises();

    const mockSave = mockEventRepository.save;
    expect(mockSave).toBeCalledTimes(1);
    const savedEvent = mockSave.mock.calls[0][0] as Event;
    expect(savedEvent.event_id).toBe(mockEvent.event_id);
    expect(savedEvent.cancelled).toBe(reasonToCancel);
});

test("Should get the number of participants in an event", async () => {
    const mockEventWithParticipants = {
        ...mockEvent,
        participants: [{}, {}],
    };
    const mockRequest = {
        params: { id: EVENT_ID },
    } as unknown as Request;

    const mockEventRepository = {
        ...typeORMMockRepository,
        createQueryBuilder: () =>
            getMockQueryBuilder([mockEventWithParticipants]),
    };

    (getRepository as jest.Mock).mockImplementation(() => mockEventRepository);

    getNumberOfParticipants(mockRequest, mockResponse);
    await flushPromises();

    const sentResponse = mockSend.mock.calls[0][0] as {
        numParticipants: number;
    };
    expect(sentResponse.numParticipants).toBe(
        mockEventWithParticipants.participants.length,
    );
});
