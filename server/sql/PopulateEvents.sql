INSERT INTO public.event(
	name, description, start_timedate, end_timedate, category, visibility, location, "ownerAccountId", cancelled)
	VALUES 
    ('SQLEvent1', 'SQL population script test 1', '2021-08-01 01:00:00+00', '2021-08-01 01:11:00+00', 'Other', 'Public', 'Finland', 1, null),
    ('SQLEvent2', 'SQL population script test 2', '2021-08-02 02:00:00+00', '2021-08-02 02:22:00+00', 'Sport', 'Private', 'Finland', 2, null),
    ('SQLEvent3', 'SQL population script test 3', '2021-08-03 03:00:00+00', '2021-08-03 03:33:00+00', 'Other', 'Public', 'Finland', 3, null),
    ('SQLEvent4', 'SQL population script test 4', '2021-08-04 04:00:00+00', '2021-08-04 04:44:00+00', 'Other', 'Private', 'Finland', 4, null),
    ('SQLEvent5', 'SQL population script test 5', '2021-08-05 05:00:00+00', '2021-08-05 05:55:00+00', 'Other', 'Public', 'Finland', 5, null),
    ('SQLEvent6', 'SQL population script test 6', '2021-08-06 06:00:00+00', '2021-08-06 06:16:00+00', 'Sport', 'Private', 'Finland', 6, null),
    ('SQLEvent7', 'SQL population script test 7', '2021-08-07 07:00:00+00', '2021-08-07 07:17:00+00', 'Other', 'Public', 'Finland', 7, null),
    ('SQLEvent8', 'SQL population script test 8', '2021-08-08 08:00:00+00', '2021-08-08 08:18:00+00', 'Other', 'Private', 'Finland', 8, null),
    ('SQLEvent9', 'SQL population script test 9', '2021-08-09 09:00:00+00', '2021-08-09 09:19:00+00', 'Other', 'Public', 'Finland', 9, null),
    ('SQLEvent10', 'SQL population script test 10', '2021-08-10 10:00:00+00', '2021-08-10 10:10:00+00', 'Other', 'Private', 'Finland', 10, null)
    ON CONFLICT (event_id) DO 
    UPDATE SET event_id  = EXCLUDED.event_id,
    name = EXCLUDED.name, description = EXCLUDED.description, start_timedate = EXCLUDED.start_timedate,
    end_timedate = EXCLUDED.end_timedate, category = EXCLUDED.category, visibility = EXCLUDED.visibility,
    location = EXCLUDED.location, "ownerAccountId" = EXCLUDED."ownerAccountId", cancelled = EXCLUDED.cancelled;
