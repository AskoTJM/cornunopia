INSERT INTO public.account(
	email, name, date_of_birth, sex, description, last_login, profile_image, salt, hash)
	VALUES 
    ('user1@email.not', 'UserName1', '1991-01-01', 'other', 'SQL User Script Test 1', '2021-07-26 12:05:30.647394+00', null,
        '52749c543c5b151ef3cc13404955e030e9114be943145204d5531cecb4b0e498',
        '39020184f0642712fd41892e16e9173043dbf803f6b41140b0fa52e85c021c78e31b1863339cafbb0815580b1af9536d97032b521c9d214dd7d796356c9686f9'),
    ('user2@email.not', 'UserName2', '1992-02-02', 'other', 'SQL User Script Test 2', '2021-07-26 12:05:30.647394+00', null, '00', '00'),
    ('user3@email.not', 'UserName3', '1993-03-03', 'other', 'SQL User Script Test 3', '2021-07-26 12:05:30.647394+00', null, '00', '00'),
    ('user4@email.not', 'UserName4', '1994-04-04', 'other', 'SQL User Script Test 4', '2021-07-26 12:05:30.647394+00', null, '00', '00'),
    ('user5@email.not', 'UserName5', '1995-05-05', 'other', 'SQL User Script Test 5', '2021-07-26 12:05:30.647394+00', null, '00', '00'),
    ('user6@email.not', 'UserName6', '1996-06-06', 'other', 'SQL User Script Test 6', '2021-07-26 12:05:30.647394+00', null, '00', '00'),
    ('user7@email.not', 'UserName7', '1997-07-07', 'other', 'SQL User Script Test 7', '2021-07-26 12:05:30.647394+00', null, '00', '00'),
    ('user8@email.not', 'UserName8', '1998-08-08', 'other', 'SQL User Script Test 8', '2021-07-26 12:05:30.647394+00', null, '00', '00'),
    ('user9@email.not', 'UserName9', '1999-09-09', 'other', 'SQL User Script Test 9', '2021-07-26 12:05:30.647394+00', null, '00', '00'),
    ('user10@email.not', 'UserName10', '2000-10-10', 'other', 'SQL User Script Test 10', '2021-07-26 12:05:30.647394+00', null, '00', '00')
    ON CONFLICT (account_id) DO 
    UPDATE SET account_id  = EXCLUDED.account_id,
    email = EXCLUDED.email, name =  EXCLUDED.name, date_of_birth = EXCLUDED.date_of_birth,
    sex = EXCLUDED.sex, description = EXCLUDED.description, last_login = EXCLUDED.last_login,
    profile_image = EXCLUDED.profile_image;