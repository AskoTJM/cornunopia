INSERT INTO public."group"(
	group_name, group_description, category, visibility, last_modified, "ownerAccountId")
	VALUES 
    ('SQLGroup1', 'SQL Group population script test 1', 'Other', 'Public', '2021-08-02 02:00:00+00', 1),
    ('SQLGroup2', 'SQL Group population script test 2', 'Other', 'Private', '2021-08-02 02:00:00+00', 2),
    ('SQLGroup3', 'SQL Group population script test 3', 'Other', 'Public', '2021-08-02 02:00:00+00', 3),
    ('SQLGroup4', 'SQL Group population script test 4', 'Other', 'Private', '2021-08-02 02:00:00+00', 4),
    ('SQLGroup5', 'SQL Group population script test 5', 'Other', 'Public', '2021-08-02 02:00:00+00', 5),
    ('SQLGroup6', 'SQL Group population script test 6', 'Other', 'Private', '2021-08-02 02:00:00+00', 6),
    ('SQLGroup7', 'SQL Group population script test 7', 'Other', 'Public', '2021-08-02 02:00:00+00', 7),
    ('SQLGroup8', 'SQL Group population script test 8', 'Other', 'Private', '2021-08-02 02:00:00+00', 8),
    ('SQLGroup9', 'SQL Group population script test 9', 'Other', 'Public', '2021-08-02 02:00:00+00', 9),
    ('SQLGroup10', 'SQL Group population script test 10', 'Other', 'Private', '2021-08-02 02:00:00+00', 10)
    ON CONFLICT (group_id) DO 
    UPDATE SET group_id  = EXCLUDED.group_id,
    group_name = EXCLUDED.group_name, group_description = EXCLUDED.group_description, category = EXCLUDED.category,
    visibility = EXCLUDED.visibility, last_modified = EXCLUDED.last_modified, "ownerAccountId" = EXCLUDED."ownerAccountId";
