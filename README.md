# Cornunopia

[![pipeline status](https://gitlab.com/AskoTJM/cornunopia/badges/master/pipeline.svg)](https://gitlab.com/AskoTJM/cornunopia/-/commits/master) [![coverage report](https://gitlab.com/AskoTJM/cornunopia/badges/master/coverage.svg)](https://gitlab.com/AskoTJM/cornunopia/-/commits/master)

## Description

Service for people to arrange get-togethers and social events.

The name "Cornunopia" is a blend of two words: _[cornucopia](https://en.wikipedia.org/wiki/Cornucopia)_, horn of plenty, and _nopia_, which means _fast_ in some Finnish dialects.

## Requirements

### Docker

-   [Install Docker Desktop on Windows](https://docs.docker.com/docker-for-windows/install/)
-   [Install Docker Engine on Ubuntu](https://docs.docker.com/engine/install/ubuntu/)

### Node.js

Versions used for development:

-   npm 6.14.14, 6.14.13
-   node v14.17.4, v14.17.0

Node.js Installation & [Download](https://nodejs.org/en/download/)

## How to run

-   Pull or download project from GitLab
-   Create and save .env file in the /server folder with the following content. Just add passwords of your choice.  
    Any `POSTGRES_PASSWORD` is fine as it's automatically used both by database and TypeORM. `SECRET` is
    used when JWT tokens are generated and verified in the backend. Use long and good passwords.
    ```
    POSTGRES_USER=postgres
    POSTGRES_HOST=db
    POSTGRES_DATABASE=cornu_database
    POSTGRES_PASSWORD=database_password_of_your_choice
    POSTGRES_PORT=5432
    JWT_EXPIRE=10h
    SECRET=jwt_password_of_your_choice
    ```
-   Create and save a .env file in the root folder with the following content. The value of `DBPASSWORD`
    must be the same as `POSTGRES_PASSWORD` in the file server/.env.
    ```
    DBPASSWORD=database_password_of_your_choice
    ```

### Start all the Docker containers

-   In the root directory of project run `npm run cornunopia` . This will first install all NPM packages and then starts Docker containers.

### Create database

-   Go to /server folder and run `npm run create:dev:database` .

### Populate with test data

-   In /server folder run `npm run populate:dev:all` to populate tables with test data.

### Test the Frontend

-   Go to `localhost:3001` with your browser.
-   To log in, register as a new user. Alternatively you can use the automatically populated test user \#1
    whose email is `user1@email.not` and password is `1234`. The rest of the populated users \#2...\#10
    don't have a valid password set, so they can't log in.

### Test the backend

-   For development purposes (like Postman), backend is reachable at `localhost:5000`

### Test the Database

-   For development purposes (like pgAdmin), the database can be reached at `localhost:5433` (Using port 5432 could cause conflicts if there is already a local database running.)

## How to run testing locally

-   To run frontend tests, go to /client folder and run `npm run test`
-   To run backend tests, go to /server folder and run `npm run test`

## Technologies used

### Frontend:

-   [React](https://reactjs.org/)
-   [Typescript](https://www.typescriptlang.org/)
-   [React Router](https://reactrouter.com/), Routing pages.
-   [Material-UI](https://material-ui.com/), for making things look prettier
-   [Material-UI Icons](https://material-ui.com/components/material-icons/), for sleek icons
-   [Material-UI/pickers](https://material-ui-pickers.dev/), for date and time pickers
-   [Date-fns](https://date-fns.org/), to help Parse, validate and display dates and times, also required by Material-Ui pickers.
-   [Axios](https://github.com/axios/axios), to transfer data with Backend
-   [Formik](https://formik.org/), for handling and validating form data
-   [ESLint](https://eslint.org/), for linting TypeScript

### Backend:

-   [NodeJS](https://nodejs.org/en/)
-   [Express](https://expressjs.com/), for easier routing
-   [TypeORM](https://typeorm.io/#/), ORM for Typescript
-   [pg](https://github.com/brianc/node-postgres), database driver to communicate with Postgresql, required by TypeORM
-   [class-validator](https://github.com/typestack/class-validator), to validate class data
-   [class-transformer](https://github.com/typestack/class-transformer), transform plain object into class and vice versa.
-   [cors](https://expressjs.com/en/resources/middleware/cors.html), to help development without CORS issues
-   [dotenv](https://github.com/motdotla/dotenv#readme), to use .env files
-   [ts-node](https://github.com/TypeStrong/ts-node), JIT that transform Typescript into Javascipt without precompiling, makes development process easier.
-   [Nodemon](https://github.com/remy/nodemon), automatically restarts backend when files change, makes development process easier.
-   [Jest](https://jestjs.io/), testing framework
-   [ESLint](https://eslint.org/), for linting TypeScript
-   [jsonwebtoken](https://www.npmjs.com/package/jsonwebtoken), for issuing and verifying JSON Web tokens
-   [passport](http://www.passportjs.org/), authentication middleware

### Database:

-   [PostgreSQL](https://www.postgresql.org/), it's a database.

## Endpoints

(Tables created/formatted with [Tables Generator](https://www.tablesgenerator.com/markdown_tables#) and [Prettier](https://prettier.io/).)

### Accounts

| Resource | Endpoint                         | Method | Description                                               |
| -------- | -------------------------------- | ------ | --------------------------------------------------------- |
| Account  | /user                            | POST   | Register new Account                                      |
| Account  | /user/{id}                       | GET    | Get user by ID                                            |
| Account  | /user/{id}                       | PATCH  | Update Account information                                |
| Account  | /search_user                     | GET    | Search by name or email, e.g.: `/search_user?search=john` |
| Account  | /login                           | POST   | Log user in                                               |
| Account  | /events_participated             | GET    | Get events participated by user                           |
| Account  | /user/{id}/pending_event_invites | GET    | Get user's pending event invites                          |
| Account  | /user/{id}/pending_group_invites | GET    | Get user's pending group invites                          |

### Events

| Resource | Endpoint                     | Method | Description                |
| -------- | ---------------------------- | ------ | -------------------------- |
| Event    | /event                       | POST   | Create Event               |
| Event    | /event                       | GET    | Get all Events             |
| Event    | /event/{id}                  | PUT    | Replace Event              |
| Event    | /event/{id}                  | PATCH  | Update Event               |
| Event    | /event/{id}                  | DELETE | Delete Event               |
| Event    | /event/{id}                  | GET    | Get Event by ID            |
| Event    | /event/{id}/cancel           | PATCH  | Cancel event               |
| Event    | /event/{id}/num_participants | GET    | Get number of participants |

### Groups

| Resource | Endpoint                 | Method | Description            |
| -------- | ------------------------ | ------ | ---------------------- |
| Group    | /group                   | POST   | Create Group           |
| Group    | /group                   | GET    | Get all Groups         |
| Group    | /group/{id}              | PUT    | Update Group           |
| Group    | /group/{id}              | GET    | Get Group by ID        |
| Group    | /group/{id}              | DELETE | Delete Group           |
| Group    | /group/{id}/participants | GET    | Get group participants |

### EventAccounts

| Resource     | Endpoint                                 | Method | Description                     |
| ------------ | ---------------------------------------- | ------ | ------------------------------- |
| EventAccount | /event_account/participate/{event_id}    | POST   | Participate in Event            |
| EventAccount | /event_account/participate/{event_id}    | DELETE | Withdraw participation in event |
| EventAccount | /event_account/decline_invite/{event_id} | DELETE | Decline event invitation        |

### GroupAccounts

| Resource     | Endpoint                                 | Method | Description              |
| ------------ | ---------------------------------------- | ------ | ------------------------ |
| GroupAccount | /group_account/participate/{group_id}    | POST   | Participate in group     |
| GroupAccount | /group_account/participate/{group_id}    | DELETE | Participate in group     |
| GroupAccount | /group_account/decline_invite/{group_id} | DELETE | Decline group invitation |
