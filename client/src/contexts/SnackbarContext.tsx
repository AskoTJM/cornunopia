import React, { useState, createContext } from "react";
import { TEXT_FOR_HIDDEN_SNACKBAR } from "../constants/constants";

interface ISnackbarContext {
    snackbarText: string;
    setSnackbarText: React.Dispatch<React.SetStateAction<string>>;
}

const SnackbarContext = createContext<ISnackbarContext>({
    snackbarText: TEXT_FOR_HIDDEN_SNACKBAR,
    setSnackbarText: () => TEXT_FOR_HIDDEN_SNACKBAR,
});

export const SnackbarProvider: React.FC = (props) => {
    const { children } = props;
    const [snackbarText, setSnackbarText] = useState(TEXT_FOR_HIDDEN_SNACKBAR);
    return (
        <SnackbarContext.Provider value={{ snackbarText, setSnackbarText }}>
            {children}
        </SnackbarContext.Provider>
    );
};

export default SnackbarContext;
