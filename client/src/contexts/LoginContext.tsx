import React, { useState, createContext, useEffect } from "react";

interface ILoginContext {
    loginAccountId: number | null;
    saveAccount: (accountID: number) => void;
}

const LoginContext = createContext<ILoginContext>({
    loginAccountId: null,
    saveAccount: () => null,
});

export const LoginProvider: React.FC = (props) => {
    const { children } = props;
    const [loginAccountId, setLoginAccount] = useState<number | null>(null);

    const saveAccount = (accountID: number) => {
        localStorage.setItem("accountID", JSON.stringify(accountID));
        setLoginAccount(accountID);
    };

    useEffect(() => {
        const loggedUser = localStorage.getItem("accountID");

        if (loggedUser) {
            const foundUser = JSON.parse(loggedUser) as number;
            setLoginAccount(foundUser);
        }
    }, []);

    return (
        <LoginContext.Provider value={{ loginAccountId, saveAccount }}>
            {children}
        </LoginContext.Provider>
    );
};

export default LoginContext;
