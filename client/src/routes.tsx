import { Route, Switch } from "react-router-dom";
import React from "react";
import LoginForm from "./components/login/LoginForm";
import RegistrationForm from "./components/registration/RegistrationForm";
import GroupView from "./components/group/GroupView";
import EventView from "./components/event/EventView";
import ProtectedRoute from "./components/navigation/ProtectedRoute";
import ProfileView from "./components/profile/ProfileView";
import MainPageView from "./components/main/MainPageView";

interface ICornuRoutes {}

const CornuRoutes: React.FC<ICornuRoutes> = () => (
    <Switch>
        <ProtectedRoute path="/" exact publicContent={<p>Everyone can see </p>}>
            <MainPageView />
        </ProtectedRoute>
        <ProtectedRoute path="/event">
            <EventView />
        </ProtectedRoute>
        <ProtectedRoute path="/group">
            <GroupView />
        </ProtectedRoute>
        <ProtectedRoute path="/post">
            <h1>Post and Messages PlaceHolder</h1>
        </ProtectedRoute>
        <ProtectedRoute path="/profile">
            <ProfileView />
        </ProtectedRoute>
        <Route path="/login">
            <LoginForm />
        </Route>
        <Route path="/register">
            <RegistrationForm />
        </Route>
    </Switch>
);

export default CornuRoutes;
