const dateString = "01-02-2021";
jest.mock("../utils.ts", () => ({
    getDateString: jest.fn(() => dateString),
    getDateStringWithDistanceToNow: jest.fn(() => dateString),
    isEventInPast: jest.fn(() => false),
    isEventOwner: jest.fn(() => true),
}));

import React from "react";
import { shallow } from "enzyme";
import { IEvent } from "../types";
import { Button } from "@material-ui/core";
import InvitesListItem from "../components/main/InvitesListItem";

describe("InvitesListItem testing", () => {
    const location = "Event location";
    const category = "Sport";
    const description = "Test description";
    const event: IEvent = {
        event_id: 1,
        name: "Name of test event",
        location,
        category,
        description,
        start_timedate: new Date(),
        owner: { account_id: 2, name: "Account Name", email: "mail@mail.com" },
        participants: [],
    };
    const secondaryText = "Text";
    const setExpandedItem = jest.fn();
    const openDetails = jest.fn();
    const acceptInvite = jest.fn();
    const declineInvite = jest.fn();
    const info = <span>Info</span>;

    const wrapper = shallow(
        <InvitesListItem
            itemId={event.event_id}
            name={event.name}
            secondaryText={secondaryText}
            expandedItem={false}
            setExpandedItem={setExpandedItem}
            openDetails={openDetails}
            acceptInvite={acceptInvite}
            declineInvite={declineInvite}
            info={info}
        />,
    );

    it("Should render InvitesListItem and simulate button clicks", () => {
        expect(wrapper.contains(event.name)).toBe(true);
        expect(wrapper.contains(secondaryText)).toBe(true);
        expect(wrapper.contains(info)).toBe(true);

        const buttons = wrapper.find(Button);

        buttons.at(0).simulate("click");
        expect(acceptInvite).toBeCalledTimes(1);

        buttons.at(1).simulate("click");
        expect(declineInvite).toBeCalledTimes(1);

        buttons.at(2).simulate("click");
        expect(openDetails).toBeCalledTimes(1);
    });
});
