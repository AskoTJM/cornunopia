jest.mock("../utils.ts", () => ({
    visibilityToUIText: jest.fn((visibility: string) => visibility),
}));

import React from "react";
import { shallow } from "enzyme";
import GroupInfoTexts from "../../src/components/group/GroupInfoTexts";
import { IGroup } from "../types";
import { Typography } from "@material-ui/core";
import { VISIBILITY_PRIVATE } from "../constants/constants";

describe("GroupInfoTexts testing", () => {
    const group = {
        group_id: 1,
        group_name: "Name of test group",
        category: "Sport",
        group_description: "Test description",
        visibility: VISIBILITY_PRIVATE,
        participants: [{ participating: true }, {}, { participating: true }],
    } as unknown as IGroup;

    it("Should render GroupInfoTexts", () => {
        const wrapper = shallow(<GroupInfoTexts group={group} />);

        expect(wrapper.contains(group.group_description as string)).toBe(true);
        expect(wrapper.contains(group.category)).toBe(true);
        expect(wrapper.contains(group.visibility)).toBe(true);
        expect(wrapper.find(Typography).at(7).props().children).toBe(2);
    });
});
