import React from "react";
import { shallow } from "enzyme";
import GroupListItem, { IGroupListItem } from "./../components/group/GroupListItem";
import { IGroup } from "../types";
import { Button } from "@material-ui/core";
import { VISIBILITY_PRIVATE } from "../constants/constants";

jest.mock("../utils.ts", () => ({
    isGroupOwner: jest.fn(() => true),
}));

describe("GroupListItem testing", () => {
    const groupDescription = "Group description";
    const group: IGroup = {
        group_id: 1,
        owner: { account_id: 11, name: "Account Name", email: "mail@mail.com" },
        group_name: "test111",
        group_description: groupDescription,
        category: "Sport",
        visibility: VISIBILITY_PRIVATE,
        participants: [],
    };

    const openGroupForEditing = jest.fn();
    const setExpandedItem = jest.fn();
    const participateInPublicGroup = jest.fn();
    const openGroupMembers = jest.fn();
    const createEventForGroup = jest.fn();

    const props: IGroupListItem = {
        group,
        expandedItem: false,
        openGroupForEditing,
        setExpandedItem,
        participateInPublicGroup,
        openGroupMembers,
        createEventForGroup,
    };

    const wrapper = shallow(<GroupListItem {...props} />);
    const buttons = wrapper.find(Button);

    it("Should render GroupListItem with passed data", () => {
        expect(wrapper.contains(group.group_name)).toEqual(true);
        expect(wrapper.contains(groupDescription)).toBe(true);
    });

    it("Should open group members", () => {
        buttons.at(0).simulate("click");
        expect(openGroupMembers).toBeCalledTimes(1);
        expect(openGroupMembers).toHaveBeenCalledWith(group);
    });

    it("Should open group editing if user is group owner", () => {
        buttons.at(1).simulate("click");
        expect(openGroupForEditing).toBeCalledTimes(1);
        expect(openGroupForEditing).toHaveBeenCalledWith(group);
    });

    it("Should create an event for the group", () => {
        buttons.at(2).simulate("click");
        expect(createEventForGroup).toBeCalledTimes(1);
    });
});
