import React from "react";
import { shallow } from "enzyme";
import { IGroup } from "../types";
import { VISIBILITY_PRIVATE } from "../constants/constants";
import GroupEventDialog from "../components/group/GroupEventDialog";
import { Button } from "@material-ui/core";

describe("GroupEventDialog testing", () => {
    const group = {
        group_id: 1,
        group_name: "Name of test group",
        category: "Sport",
        group_description: "Test description",
        visibility: VISIBILITY_PRIVATE,
        participants: [],
    } as unknown as IGroup;

    it("Should render GroupInfoTexts and test button clicks", () => {
        const hide = jest.fn();

        const wrapper = shallow(<GroupEventDialog group={group} hide={hide} />);

        const buttons = wrapper.find(Button);
        const createButton = buttons.at(0);
        const cancelButton = buttons.at(1);

        expect(wrapper.text().includes(group.group_name)).toEqual(true);
        cancelButton.simulate("click");
        expect(hide).toBeCalledTimes(1);
        hide.mockReset();
        createButton.simulate("click");
        expect(hide).toBeCalledTimes(1);
    });
});
