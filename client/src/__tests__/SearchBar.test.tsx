import React from "react";
import { shallow } from "enzyme";
import SearchBar from "../components/util/SearchBar";
import { IAccount } from "../types";
import { ListItem, ListItemText, OutlinedInput } from "@material-ui/core";

beforeAll(() => jest.useFakeTimers());

test("Should render SearchBar and test mouse clicks", () => {
    const account1: IAccount = {
        account_id: 1,
        name: "Test Name 1",
        email: "test1@mail.not",
        sex: "male",
        description: "Test description 1",
        date_of_birth: new Date("01-02-1991"),
    };
    const account2: IAccount = {
        account_id: 2,
        name: "Test Name 2",
        email: "test2@mail.not",
        sex: "male",
        description: "Test description 2",
        date_of_birth: new Date("02-03-1992"),
    };
    const searchedString = "Typed text";
    const searchResults = [account1, account2];
    const props = {
        label: "Search bar label",
        searchResults,
        searchFunction: jest.fn(),
        getKey: jest.fn((result: IAccount) => String(result.account_id)),
        getPrimaryText: jest.fn((result: IAccount) => result.name),
        getSecondaryText: jest.fn((result: IAccount) => result.email),
        onResultClick: jest.fn(),
    };

    const wrapper = shallow(<SearchBar {...props} />);

    expect(props.getKey).toBeCalledTimes(searchResults.length);
    expect(props.getKey.mock.calls[0][0]).toBe(account1);
    expect(props.getKey.mock.calls[1][0]).toBe(account2);

    expect(props.getPrimaryText).toBeCalledTimes(searchResults.length);
    expect(props.getPrimaryText.mock.calls[0][0]).toBe(account1);
    expect(props.getPrimaryText.mock.calls[1][0]).toBe(account2);

    expect(props.getSecondaryText).toBeCalledTimes(searchResults.length);
    expect(props.getSecondaryText.mock.calls[0][0]).toBe(account1);
    expect(props.getSecondaryText.mock.calls[1][0]).toBe(account2);

    const listItemTexts = wrapper.find(ListItemText);
    expect(listItemTexts).toHaveLength(searchResults.length);
    expect(listItemTexts.at(0).props().primary).toEqual(account1.name);
    expect(listItemTexts.at(0).props().secondary).toEqual(account1.email);
    expect(listItemTexts.at(1).props().primary).toEqual(account2.name);
    expect(listItemTexts.at(1).props().secondary).toEqual(account2.email);

    const listItem1 = wrapper.find(ListItem).at(0);
    expect(listItem1.key()).toBe(String(account1.account_id));
    listItem1.simulate("click");
    expect(props.onResultClick).toBeCalledWith(account1);

    const input = wrapper.find(OutlinedInput).at(0);
    expect(input.props().label).toBe(props.label);
    input.simulate("change", { target: { value: searchedString } });
    jest.runAllTimers();
    expect(props.searchFunction).toBeCalledWith(searchedString);
});
