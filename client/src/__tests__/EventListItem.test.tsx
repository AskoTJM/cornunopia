const dateString = "01-02-2021";
jest.mock("../utils.ts", () => ({
    getDateString: jest.fn(() => dateString),
    getDateStringWithDistanceToNow: jest.fn(() => dateString),
    isEventInPast: jest.fn(() => false),
    isEventOwner: jest.fn(() => true),
}));

import React from "react";
import { shallow } from "enzyme";
import EventListItem from "../../src/components/event/EventListItem";
import { IEvent } from "../types";
import { Button } from "@material-ui/core";
import CheckCircleIcon from "@material-ui/icons/CheckCircle";
import { VISIBILITY_PUBLIC } from "../constants/constants";

describe("EventListItem testing", () => {
    const location = "Event location";
    const category = "Sport";
    const description = "Test description";
    const event: IEvent = {
        event_id: 1,
        name: "Name of test event",
        location,
        category,
        description,
        start_timedate: new Date(),
        owner: { account_id: 2, name: "Account Name", email: "mail@mail.com" },
        participants: [],
        visibility: VISIBILITY_PUBLIC,
    };
    const setExpandedItem = jest.fn();
    const openEventDetails = jest.fn();
    const openEventForEditing = jest.fn();
    const participateInEvent = jest.fn();
    const leaveEvent = jest.fn();
    const openCancellationDialog = jest.fn();

    const getWrapper = (evt: IEvent, isMainPage: boolean) =>
        shallow(
            <EventListItem
                event={evt}
                expandedItem={false}
                setExpandedItem={setExpandedItem}
                openEventDetails={openEventDetails}
                openEventForEditing={openEventForEditing}
                participateInEvent={participateInEvent}
                leaveEvent={leaveEvent}
                isMainPage={isMainPage}
                openCancellationDialog={openCancellationDialog}
            />,
        );

    it("Should render EventListItem and simulate button clicks", () => {
        const wrapper = getWrapper(event, false);

        expect(wrapper.contains(event.name)).toBe(true);
        expect(wrapper.contains(dateString)).toBe(true);
        expect(wrapper.contains(location)).toBe(true);

        const buttons = wrapper.find(Button);
        buttons.at(0).simulate("click");
        expect(openEventDetails).toBeCalledTimes(1);
        expect(openEventDetails).toHaveBeenCalledWith(event);

        const cancelButton = buttons.at(1);
        cancelButton.simulate("click");
        expect(participateInEvent).toBeCalledTimes(1);
        expect(participateInEvent).toHaveBeenCalledWith(event);
        expect(cancelButton.childAt(0).text().includes("Edit")).toBe(false);

        buttons.at(2).simulate("click");
        expect(openCancellationDialog).toBeCalledTimes(1);
        expect(openCancellationDialog).toHaveBeenCalledWith(event);

        buttons.at(3).simulate("click");
        expect(openEventForEditing).toBeCalledTimes(1);
        expect(openEventForEditing).toHaveBeenCalledWith(event);
    });

    it("Should leave event on button click when user is participating", () => {
        const wrapper = getWrapper(event, true);

        const buttons = wrapper.find(Button);
        buttons.at(1).simulate("click");
        expect(leaveEvent).toBeCalledTimes(1);
        expect(leaveEvent).toHaveBeenCalledWith(event);

        expect(wrapper.find(CheckCircleIcon)).toHaveLength(1);
    });

    it("Should test a list item for a cancelled event", () => {
        const cancelledEvent = { ...event, cancelled: "Reason for cancellation" };

        const wrapper = getWrapper(cancelledEvent, false);

        expect(wrapper.text().includes("CANCELLED")).toBe(true);
        const cancelButton = wrapper.find(Button).at(2);
        expect(cancelButton.childAt(0).text().includes("Edit")).toBe(true);
    });
});
