const dateString = "01-02-2021";
jest.mock("../utils.ts", () => ({
    getDateString: jest.fn(() => dateString),
    getDateStringWithDistanceToNow: jest.fn(() => dateString),
}));

import React from "react";
import { shallow } from "enzyme";
import EventInfoTexts from "../../src/components/event/EventInfoTexts";
import { IEvent } from "../types";

describe("EventInfoTexts testing", () => {
    const event = {
        event_id: 1,
        name: "Name of test event",
        location: "Event location",
        category: "Sport",
        description: "Test description",
        start_timedate: new Date(),
        owner: {},
        participants: [],
    } as unknown as IEvent;

    const handleClick = jest.fn();

    it("Should render EventInfoTexts and simulate a click event", () => {
        const wrapper = shallow(<EventInfoTexts event={event} onClick={handleClick} />);

        expect(wrapper.contains(event.location as string)).toBe(true);
        expect(wrapper.contains(dateString)).toBe(true);
        expect(wrapper.contains(event.description as string)).toBe(true);
        expect(wrapper.contains(event.category as string)).toBe(true);

        wrapper.find("div").at(0).simulate("click");
        expect(handleClick).toBeCalledTimes(1);
        expect(handleClick).toHaveBeenCalledWith(event);
    });
});
