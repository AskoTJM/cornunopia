import React from "react";
import { shallow } from "enzyme";
import EventFormControls from "../components/event/EventFormControls";
import { DEFAULT_AGE_RANGE, VISIBILITY_PRIVATE } from "../constants/constants";
import { FormikProps } from "formik";
import { IEventFormData } from "../components/event/EventForm";
import { Select, TextField } from "@material-ui/core";

describe("EventFormControls tests", () => {
    const event = {
        event_id: 1,
        name: "Test event",
        location: "Test location",
        category: "Sport",
        visibility: VISIBILITY_PRIVATE,
        description: "Description",
        start_timedate: new Date(),
        owner: { account_id: 2, name: "User Name", email: "mail@mail.com" },
        participants: [],
    };

    const eventFormData: IEventFormData = {
        eventName: event.name,
        eventDescription: event.description || "",
        visibility: event.visibility || "",
        category: event.category || "",
        startDateTime: event.start_timedate,
        endDateTime: null,
        location: event.location || "",
        ageRange: DEFAULT_AGE_RANGE,
    };

    it("Should render EventFormControls with an event for editing", () => {
        const mockFormik = {
            values: eventFormData,
            errors: {},
            touched: {},
        } as FormikProps<IEventFormData>;

        const wrapper = shallow(<EventFormControls formik={mockFormik} isEditMode={true} />);

        const textFields = wrapper.find(TextField);
        expect(textFields.at(0).props().value).toEqual(event.name);
        expect(textFields.at(1).props().value).toEqual(event.description);
        expect(textFields.at(2).props().value).toEqual(event.location);

        const selects = wrapper.find(Select);
        expect(selects.at(0).props().value).toEqual(event.visibility);
        expect(selects.at(1).props().value).toEqual(event.category);
    });
});
