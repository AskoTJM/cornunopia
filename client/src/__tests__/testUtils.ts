export const flushPromises = (): Promise<unknown> =>
    new Promise((resolve) => setImmediate(resolve));
