import { VISIBILITY_PRIVATE, VISIBILITY_PUBLIC } from "../constants/constants";
import { IAccount, IEvent } from "../types";
import { fixEventsData, getVisibilities, sortAccountsByName, visibilityToUIText } from "../utils";

test("Should get all event/group visibilities", () => {
    const visibilities = getVisibilities();

    expect(visibilities).toHaveLength(2);
    expect(visibilities).toContain(VISIBILITY_PRIVATE);
    expect(visibilities).toContain(VISIBILITY_PUBLIC);
});

test("Should get UI text for event/group visibility private", () => {
    const privateText = visibilityToUIText(VISIBILITY_PRIVATE);

    expect(typeof privateText).toBe("string");
    expect(privateText.length).toBeGreaterThan(0);
});

test("Should get UI text for event/group visibility public", () => {
    const publicText = visibilityToUIText(VISIBILITY_PUBLIC);

    expect(typeof publicText).toBe("string");
    expect(publicText.length).toBeGreaterThan(0);
});

test("Should fix dates in event objects", () => {
    const event = {
        event_id: 1,
        name: "Test event",
        start_timedate: "2021-07-17T08:30:00.000Z",
        end_timedate: "2021-08-17T08:45:00.000Z",
    } as unknown as IEvent;

    const fixedEvents = fixEventsData([event]);

    expect(fixedEvents).toHaveLength(1);
    expect(fixedEvents[0].start_timedate).toBeInstanceOf(Date);
    expect(fixedEvents[0].end_timedate).toBeInstanceOf(Date);
});

test("Should sort accounts alphabetically", () => {
    const accounts = [
        { name: "c" } as IAccount,
        { name: "d" } as IAccount,
        { name: "a" } as IAccount,
        { name: "b" } as IAccount,
        { name: "A" } as IAccount,
    ];

    const sortedAccounts = sortAccountsByName(accounts);

    expect(sortedAccounts).toHaveLength(accounts.length);
    expect(sortedAccounts[0].name).toBe("a");
    expect(sortedAccounts[1].name).toBe("A");
    expect(sortedAccounts[2].name).toBe("b");
    expect(sortedAccounts[3].name).toBe("c");
    expect(sortedAccounts[4].name).toBe("d");
});
