jest.mock("axios");

import React from "react";
import { shallow } from "enzyme";
import GroupPeople from "../../src/components/group/GroupPeople";
import { IGroup } from "../types";
import { Button, Tabs } from "@material-ui/core";
import { VISIBILITY_PRIVATE } from "../constants/constants";
import Invites from "../components/util/Invites";
import GroupMembersList from "../components/group/GroupMembersList";
import axios from "axios";

beforeEach(() => jest.resetAllMocks());

describe("GroupPeople rendering and tests", () => {
    const group = {
        group_id: 1,
        group_name: "Name of test group",
        category: "Sport",
        group_description: "Test description",
        visibility: VISIBILITY_PRIVATE,
        participants: [
            { participating: true },
            { participating: true },
            { invite: true },
            { invite: true },
        ],
    } as unknown as IGroup;

    const hide = jest.fn();
    const refreshData = jest.fn();

    const wrapper = shallow(<GroupPeople group={group} hide={hide} refreshData={refreshData} />);

    it("Should click Reset in order to load invites and members", () => {
        const mockAxiosGet = (axios.get as jest.Mock).mockResolvedValue({
            data: [{ account: { name: "a" } }, { account: { name: "b" } }],
        });
        const resetButton = wrapper.find(Button).at(1);
        resetButton.simulate("click");
        expect(mockAxiosGet).toHaveBeenCalled();
    });

    it("Should find texts", () => {
        expect(wrapper.contains(group.group_name)).toBe(true);
    });

    it("Should check if GroupMembersList component exists and has members", () => {
        const groupMembersLists = wrapper.find(GroupMembersList);
        expect(groupMembersLists).toHaveLength(1);
        expect(groupMembersLists.at(0).props().members).toHaveLength(2);
    });

    it("Should switch to Invites tab and check if Invites component exists with invites", () => {
        wrapper.find(Tabs).at(0).simulate("change", {}, "Invites");
        const invites = wrapper.find(Invites);
        expect(invites).toHaveLength(1);
        expect(invites.at(0).props().invites).toHaveLength(2);
    });

    it("Should test Cancel button click", () => {
        const cancelButton = wrapper.find(Button).at(2);
        cancelButton.simulate("click");
        expect(hide).toHaveBeenCalledTimes(1);
    });
});
