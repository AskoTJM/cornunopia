import React from "react";
import { shallow } from "enzyme";
import EventInvitesList from "../components/main/EventInvitesList";

test("Should render EventInvitesList", () => {
    const setRefreshParticipationsList = jest.fn();

    const wrapper = shallow(
        <EventInvitesList setRefreshParticipationsList={setRefreshParticipationsList} />,
    );

    expect(wrapper.contains("Pending Event Invites")).toBeTruthy();
});
