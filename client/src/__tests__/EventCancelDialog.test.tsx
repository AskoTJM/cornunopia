jest.mock("axios");

const REASON_FOR_CANCELLATION = "Reason for cancellation";
const mockFormikValues = { isCancelled: "Cancelled", textValue: REASON_FOR_CANCELLATION };
jest.mock("formik", () => ({
    useFormik: jest.fn((config: { onSubmit: (values: typeof mockFormikValues) => void }) => ({
        values: mockFormikValues,
        touched: {},
        errors: {},
        handleSubmit: () => config.onSubmit(mockFormikValues),
    })),
}));

import React from "react";
import { shallow } from "enzyme";
import { IEvent } from "../types";
import { Button, Dialog } from "@material-ui/core";
import EventCancelDialog from "../components/event/EventCancelDialog";
import axios from "axios";
import { flushPromises } from "./testUtils";

beforeEach(() => jest.resetAllMocks());

describe("EventCancelDialog tests", () => {
    const event: IEvent = {
        event_id: 1,
        name: "Name of test event",
        location: "Event location",
        category: "Sport",
        description: "Test description",
        start_timedate: new Date(),
        owner: { account_id: 2, name: "Account Name", email: "mail@mail.com" },
        participants: [],
        cancelled: REASON_FOR_CANCELLATION,
    };
    const hide = jest.fn();
    const setRefreshEventsList = jest.fn();

    const wrapper = shallow(
        <EventCancelDialog
            isOpen={true}
            event={event}
            hide={hide}
            setRefreshEventsList={setRefreshEventsList}
        />,
    );

    it("Should test component property values and simulate Close button click", () => {
        const dialogs = wrapper.find(Dialog);
        expect(dialogs.at(0).props().open).toBeTruthy();

        const closeButton = wrapper.find(Button).at(1);
        closeButton.simulate("click");
        expect(hide).toBeCalledTimes(1);
        expect(setRefreshEventsList).toBeCalledTimes(0);
    });

    it("Should test form submission", async () => {
        const mockAxiosPatch = (axios.patch as jest.Mock).mockResolvedValue({});

        wrapper.find("form").at(0).simulate("submit");
        await flushPromises();

        expect(hide).toBeCalledTimes(1);
        expect(setRefreshEventsList).toHaveBeenCalledWith(true);
        expect(mockAxiosPatch).toHaveBeenCalledTimes(1);
        const patchArg1 = (mockAxiosPatch.mock.calls[0] as unknown[])[1] as {
            reasonToCancel: string;
        };
        expect(patchArg1.reasonToCancel).toBe(event.cancelled);
    });
});
