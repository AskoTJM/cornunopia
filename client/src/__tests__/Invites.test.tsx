import React from "react";
import { shallow } from "enzyme";
import Invites from "../components/util/Invites";
import { IAccount } from "../types";
import { ListItemText, IconButton } from "@material-ui/core";

test("Should render Invites and test button clicks", () => {
    const account1: IAccount = {
        account_id: 1,
        name: "Test Name 1",
        email: "test1@mail.not",
        sex: "male",
        description: "Test description 1",
        date_of_birth: new Date("01-02-1991"),
    };
    const account2: IAccount = {
        account_id: 2,
        name: "Test Name 2",
        email: "test2@mail.not",
        sex: "male",
        description: "Test description 2",
        date_of_birth: new Date("02-03-1992"),
    };
    const invites = [account1, account2];
    const setInvites = jest.fn();

    const wrapper = shallow(
        <Invites invites={invites} setInvites={setInvites} originalInvites={[...invites]} />,
    );

    const listItemTexts = wrapper.find(ListItemText);
    expect(listItemTexts).toHaveLength(invites.length);
    expect(listItemTexts.at(0).props().primary).toEqual(account1.name);
    expect(listItemTexts.at(0).props().secondary).toEqual(account1.email);
    expect(listItemTexts.at(1).props().primary).toEqual(account2.name);
    expect(listItemTexts.at(1).props().secondary).toEqual(account2.email);

    const removeInviteButton1 = wrapper.find(IconButton).at(0);
    removeInviteButton1.simulate("click");
    expect(setInvites).toBeCalledTimes(1);
    expect(setInvites).toBeCalledWith([account2]);
});
