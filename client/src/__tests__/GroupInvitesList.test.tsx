import React from "react";
import { shallow } from "enzyme";
import GroupInvitesList from "../components/main/GroupInvitesList";

test("Should render GroupInvitesList", () => {
    const setRefreshParticipationsList = jest.fn();

    const wrapper = shallow(
        <GroupInvitesList setRefreshParticipationsList={setRefreshParticipationsList} />,
    );

    expect(wrapper.contains("Pending Group Invites")).toBeTruthy();
});
