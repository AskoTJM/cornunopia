import React from "react";
import { shallow } from "enzyme";
import GroupFormControls from "../components/group/GroupFormControls";
import { VISIBILITY_PRIVATE } from "../constants/constants";
import { FormikProps } from "formik";
import { IGroupFormData } from "../components/group/GroupForm";
import { Select, TextField } from "@material-ui/core";

describe("GroupFormControls tests", () => {
    const group = {
        group_id: 1,
        group_name: "Test group",
        category: "Sport",
        visibility: VISIBILITY_PRIVATE,
        group_description: "Description",
    };

    const groupFormData: IGroupFormData = {
        groupName: group.group_name,
        groupDescription: group.group_description || "",
        visibility: group.visibility || "",
        category: group.category || "",
    };

    it("Should render GroupFormControls with a group for editing", () => {
        const mockFormik = {
            values: groupFormData,
            errors: {},
            touched: {},
        } as FormikProps<IGroupFormData>;

        const wrapper = shallow(<GroupFormControls formik={mockFormik} />);

        const textFields = wrapper.find(TextField);
        expect(textFields.at(0).props().value).toEqual(group.group_name);
        expect(textFields.at(1).props().value).toEqual(group.group_description);

        const selects = wrapper.find(Select);
        expect(selects.at(0).props().value).toEqual(group.visibility);
        expect(selects.at(1).props().value).toEqual(group.category);
    });
});
