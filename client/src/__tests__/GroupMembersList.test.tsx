import React from "react";
import { shallow } from "enzyme";
import GroupMembersList from "../../src/components/group/GroupMembersList";
import { IAccount } from "../types";
import { ListItemText, FormControlLabel } from "@material-ui/core";

beforeEach(() => jest.resetAllMocks());

describe("GroupMembersList rendering and tests", () => {
    const account1 = {
        account_id: 1,
        name: "User Name 1",
        email: "user1@name.net",
    } as unknown as IAccount;

    const account2 = {
        account_id: 2,
        name: "User Name 2",
        email: "user2@name.net",
    } as unknown as IAccount;

    type SetStateCallback = (prev: number[]) => number[];

    const members = [account1, account2];
    const membersToRemove = [account2.account_id];
    const setMembersToRemove = jest.fn();

    const wrapper = shallow(
        <GroupMembersList
            members={members}
            membersToRemove={membersToRemove}
            setMembersToRemove={setMembersToRemove}
            membersLoadError={false}
        />,
    );

    const formControlLabels = wrapper.find(FormControlLabel);
    const checkbox1 = shallow(formControlLabels.at(0).prop("control"));
    const checkbox2 = shallow(formControlLabels.at(1).prop("control"));

    it("Should find texts", () => {
        const listItemTexts = wrapper.find(ListItemText);
        const listItemText1 = listItemTexts.at(0);
        const listItemText2 = listItemTexts.at(1);

        expect(listItemTexts).toHaveLength(members.length);
        expect(listItemText1.props().primary).toBe(account1.name);
        expect(listItemText1.props().secondary).toBe(account1.email);
        expect(listItemText2.props().primary).toBe(account2.name);
        expect(listItemText2.props().secondary).toBe(account2.email);
    });

    test.each([
        [checkbox1, false, true, [account2.account_id, account1.account_id]],
        [checkbox2, true, false, []],
    ])("Test checkbox %#", (checkbox, expectedChecked, eventSetChecked, expectedCallbackResult) => {
        expect(checkbox.props().checked).toEqual(expectedChecked);

        checkbox.simulate("change", { target: { checked: eventSetChecked } });

        expect(setMembersToRemove).toHaveBeenCalledTimes(1);
        const callback = (setMembersToRemove.mock.calls[0] as unknown[])[0] as SetStateCallback;
        expect(typeof callback).toBe("function");
        const callbackReturn = callback(membersToRemove);
        expect(callbackReturn).toStrictEqual(expectedCallbackResult);
    });
});
