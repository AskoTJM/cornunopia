import React from "react";
import { shallow } from "enzyme";
import PersonalData from "../components/profile/PersonalData";
import { IAccount } from "../types";

test("Should render PersonalData", () => {
    const testAccount: IAccount = {
        account_id: 1,
        email: "test@mail.not",
        sex: "male",
        name: "Test Name",
        description: "Very boring test description",
        date_of_birth: new Date("1/2/2003"),
    };
    const wrapper = shallow(<PersonalData accountData={testAccount} />);

    expect(wrapper.contains(testAccount.email)).toBe(true);
    expect(wrapper.contains(testAccount.name)).toBe(true);
    expect(wrapper.contains(testAccount.description as string)).toBe(true);
    expect(wrapper.contains(testAccount.sex as string)).toBe(true);
});
