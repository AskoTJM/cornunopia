export interface IEventCreate {
    name: string;
    description?: string;
    start_timedate: Date;
    end_timedate?: Date;
    category?: string;
    visibility?: string;
    location?: string;
}

export interface IGroup {
    group_id: number;
    owner: IAccount;
    group_name: string;
    group_description?: string;
    category: string;
    visibility: string;
    participants: IGroupAccount[];
}

export interface IEvent extends IEventCreate {
    event_id: number;
    owner: IAccount;
    participants: IEventAccount[];
    cancelled?: string;
}

export interface IFixEventsData extends IEvent {
    start_timedate: Date;
    end_timedate: Date | undefined;
}

export interface IAccount {
    account_id: number;
    email: string;
    name: string;
    date_of_birth?: Date;
    sex?: string;
    description?: string;
    last_login?: Date;
    profile_image?: string;
    events_participated?: IEvent[];
    events_owned?: IEvent[];
    groups_participated?: IGroup[];
    groups_owned?: IGroup[];
}

export interface IEventAccount {
    account: IAccount;
    event: IEvent;
    inviter?: IAccount;
    invite?: boolean;
    participating?: boolean;
    blacklisted?: boolean;
}

export interface IGroupAccount {
    account: IAccount;
    group: IGroup;
    inviter?: IAccount;
    invite?: boolean;
    participating?: boolean;
    blacklisted?: boolean;
}

export interface IEventGroup {
    group: IGroup;
    event: IEvent;
    participating_accounts: IAccount[];
}
