import React from "react";
import "./App.css";
import { MuiPickersUtilsProvider } from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import { HashRouter } from "react-router-dom";
import CornuAppbar from "./components/navigation/CornuAppbar";
import CornuRoutes from "./routes";
import locale from "date-fns/locale/en-US";
import { SnackbarProvider } from "./contexts/SnackbarContext";
import CornuSnackbar from "./components/util/CornuSnackbar";
import { LoginProvider } from "./contexts/LoginContext";
import axios from "axios";
import { AXIOS_BASE_URL } from "./constants/constants";

if (locale && locale.options) locale.options.weekStartsOn = 1;
axios.defaults.baseURL = AXIOS_BASE_URL;

const App: React.FC = () => (
    <HashRouter>
        <MuiPickersUtilsProvider utils={DateFnsUtils} locale={locale}>
            <SnackbarProvider>
                <LoginProvider>
                    <CornuAppbar />
                    <CornuRoutes />
                    <CornuSnackbar />
                </LoginProvider>
            </SnackbarProvider>
        </MuiPickersUtilsProvider>
    </HashRouter>
);

export default App;
