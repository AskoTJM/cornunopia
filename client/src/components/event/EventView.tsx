import React, { useState } from "react";
import EventList from "./EventList";
import EventTaskbar from "./EventTaskbar";

interface IEventView {}

const EventView: React.FC<IEventView> = () => {
    const [refreshEventsList, setRefreshEventsList] = useState(true);

    return (
        <>
            <EventTaskbar setRefreshEventsList={setRefreshEventsList} />
            <br />
            <EventList
                refreshEventsList={refreshEventsList}
                setRefreshEventsList={setRefreshEventsList}
                isMainPage={false}
            />
        </>
    );
};

export default EventView;
