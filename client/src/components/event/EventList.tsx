import React, { useState, useEffect, useContext } from "react";
import { IEvent } from "../../types";
import EventListItem from "./EventListItem";
import axios from "axios";
import { AXIOS_BASE_URL, AXIOS_CREATE_EVENT_URL, AXIOS_USER_URL } from "../../constants/constants";
import EventDetails from "./EventDetails";
import EventForm from "./EventForm";
import SnackbarContext from "../../contexts/SnackbarContext";
import { fixEventsData, isDateInPast } from "../../utils";
import LoginContext from "../../contexts/LoginContext";
import { Container } from "@material-ui/core";
import EventCancelDialog from "./EventCancelDialog";

interface IEventList {
    refreshEventsList: boolean;
    setRefreshEventsList: React.Dispatch<React.SetStateAction<boolean>>;
    isMainPage: boolean;
}

enum EVENT_LIST_STRINGS {
    ERROR_NO_EVENTS = "No Events",
    ERROR_CANT_GET_EVENTS = "Error, Can't Get Events",
    ERROR_CANT_PARTICIPATE = "Participation failed",
    PARTICIPATION_ADDED = "Participation added",
    PARTICIPATION_DELETED = "Participation deleted",
    UPCOMING = "Upcoming",
    UPCOMING_MAIN = "Upcoming Events You Are Participating in",
    PAST = "Past",
    PAST_MAIN = "Past Events You Participated in",
}

const EventList: React.FC<IEventList> = (props) => {
    const { refreshEventsList, setRefreshEventsList, isMainPage } = props;
    const [events, setEvents] = useState<IEvent[]>([]);
    const [errorStatus, setErrorStatus] = useState(false);
    const [expandedItem, setExpandedItem] = useState<string | false>(false);
    const [eventForDetails, setEventForDetails] = useState<IEvent | null>(null);
    const [eventForEditing, setEventForEditing] = useState<IEvent | null>(null);
    const [eventForCancellation, setEventForCancellation] = useState<IEvent | null>(null);
    const { setSnackbarText } = useContext(SnackbarContext);

    const userAccountId = useContext(LoginContext).loginAccountId as number;

    const getEvents = () => {
        axios.defaults.baseURL = AXIOS_BASE_URL;
        axios
            .get<IEvent[]>(
                isMainPage
                    ? `${AXIOS_USER_URL}/${userAccountId}/events_participated`
                    : AXIOS_CREATE_EVENT_URL,
            )
            .then((response) => {
                const fixedEvents = fixEventsData(response.data);
                setEvents(fixedEvents);
                setErrorStatus(false);
            })
            .catch(() => {
                setErrorStatus(true);
                setEvents([]);
            });
    };

    useEffect(() => {
        if (refreshEventsList) {
            getEvents();
            setRefreshEventsList(false);
        }
    }, [refreshEventsList]);

    const startEventEditingFromDetailsModal = (event: IEvent | null) => {
        setEventForDetails(null);
        setEventForEditing(event);
    };

    const participateInEvent = (event: IEvent) => {
        axios.defaults.baseURL = AXIOS_BASE_URL;
        axios
            .post<IEvent>(`/event_account/participate/${event.event_id}`)
            .then(() => {
                setSnackbarText(EVENT_LIST_STRINGS.PARTICIPATION_ADDED);
                setRefreshEventsList(true);
            })
            .catch(() => {
                setSnackbarText(EVENT_LIST_STRINGS.ERROR_CANT_PARTICIPATE);
            });
    };

    const leaveEvent = (event: IEvent) => {
        axios.defaults.baseURL = AXIOS_BASE_URL;
        axios
            .delete<IEvent>(`/event_account/participate/${event.event_id}`)
            .then(() => {
                setSnackbarText(EVENT_LIST_STRINGS.PARTICIPATION_DELETED);
                setRefreshEventsList(true);
            })
            .catch(() => {
                setSnackbarText(EVENT_LIST_STRINGS.ERROR_CANT_PARTICIPATE);
            });
    };

    const upcomingEvents = events
        ?.filter((event: IEvent) => !isDateInPast(event.start_timedate) === true)
        .reverse();

    const pastEvents = events?.filter(
        (event: IEvent) => !isDateInPast(event.start_timedate) === false,
    );

    const mapListItem = (events: IEvent[], isMainPage: boolean) =>
        events.map((event) => (
            <EventListItem
                event={event}
                key={String(event.event_id)}
                expandedItem={expandedItem}
                setExpandedItem={setExpandedItem}
                openEventDetails={(event) => setEventForDetails(event)}
                openEventForEditing={(event) => setEventForEditing(event)}
                participateInEvent={participateInEvent}
                leaveEvent={leaveEvent}
                isMainPage={isMainPage}
                openCancellationDialog={setEventForCancellation}
            />
        ));

    return (
        <>
            <EventCancelDialog
                isOpen={!!eventForCancellation}
                event={eventForCancellation}
                hide={() => setEventForCancellation(null)}
                setRefreshEventsList={setRefreshEventsList}
            />
            <EventDetails
                event={eventForDetails}
                hide={() => setEventForDetails(null)}
                openEventForEditing={startEventEditingFromDetailsModal}
            />
            <EventForm
                isOpen={!!eventForEditing}
                setOpen={() => setEventForEditing(null)}
                eventForEditing={eventForEditing}
                setRefreshEventsList={setRefreshEventsList}
            />
            <Container>
                <h2>
                    {isMainPage ? EVENT_LIST_STRINGS.UPCOMING_MAIN : EVENT_LIST_STRINGS.UPCOMING}
                </h2>
                {upcomingEvents?.length > 0
                    ? mapListItem(upcomingEvents, isMainPage)
                    : errorStatus
                    ? EVENT_LIST_STRINGS.ERROR_CANT_GET_EVENTS
                    : EVENT_LIST_STRINGS.ERROR_NO_EVENTS}

                <h2>{isMainPage ? EVENT_LIST_STRINGS.PAST_MAIN : EVENT_LIST_STRINGS.PAST}</h2>
                {pastEvents?.length > 0
                    ? mapListItem(pastEvents, isMainPage)
                    : errorStatus
                    ? EVENT_LIST_STRINGS.ERROR_CANT_GET_EVENTS
                    : EVENT_LIST_STRINGS.ERROR_NO_EVENTS}
            </Container>
        </>
    );
};

export default EventList;
