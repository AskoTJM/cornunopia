import React, { useState } from "react";
import { AppBar, Toolbar, Button, makeStyles, createStyles } from "@material-ui/core";
import EventForm from "./EventForm";

interface IEventTaskbar {
    setRefreshEventsList: React.Dispatch<React.SetStateAction<boolean>>;
}

enum EVENT_TASKBAR_STRINGS {
    BUTTON_NEW_EVENT = "New Event",
}

const useStyles = makeStyles((theme) =>
    createStyles({
        taskBar: {
            backgroundColor: theme.palette.primary.light,
        },
    }),
);

const EventTaskbar: React.FC<IEventTaskbar> = (props) => {
    const { setRefreshEventsList } = props;
    const [isEventModalOpen, setIsEventModalOpen] = useState(false);
    const classes = useStyles();

    return (
        <AppBar position="static" className={classes.taskBar}>
            <Toolbar>
                <Button variant="contained" onClick={() => setIsEventModalOpen(true)}>
                    {EVENT_TASKBAR_STRINGS.BUTTON_NEW_EVENT}
                </Button>
                <EventForm
                    isOpen={isEventModalOpen}
                    setOpen={setIsEventModalOpen}
                    eventForEditing={null}
                    setRefreshEventsList={setRefreshEventsList}
                />
            </Toolbar>
        </AppBar>
    );
};

export default EventTaskbar;
