import React from "react";
import {
    Select,
    MenuItem,
    TextField,
    InputLabel,
    FormControl,
    Slider,
    Typography,
    createStyles,
    makeStyles,
    Theme,
} from "@material-ui/core";
import { KeyboardDateTimePicker } from "@material-ui/pickers";
import { FormikProps } from "formik";
import { DATE_FORMAT, getVisibilities, isDateInPast, visibilityToUIText } from "../../utils";
import { CATEGORY_OPTIONS, MAX_AGE_RANGE, MIN_AGE_RANGE } from "../../constants/constants";
import { IEventFormData } from "./EventForm";

interface IEventFormControls {
    formik: FormikProps<IEventFormData>;
    isEditMode: boolean;
}

enum EVENT_FORM_CONTROLS_STRINGS {
    LABEL_NAME = "Event name",
    LABEL_DESCRIPTION = "Description",
    LABEL_LOCATION = "Location",
    LABEL_START_DATE = "Start date and time",
    LABEL_VISIBILITY = "Who can participate in this event",
    LABEL_CATEGORY = "Category",
    LABEL_AGE_RECOMMENDATION = "Age range recommendation",
    LABEL_END_DATE = "End date and time",
    ERROR_END_DATE_BEFORE_START_DATE = "End date should not be before start date",
}

const AGE_RANGE_MARKS = [
    {
        value: 0,
        label: "0",
    },
    {
        value: 18,
        label: "18",
    },
    {
        value: 100,
        label: "100",
    },
];

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        formControl: {
            margin: theme.spacing(1, 0, 0.5, 0),
        },
    }),
);

const EventFormControls: React.FC<IEventFormControls> = (props) => {
    const { formik, isEditMode } = props;
    const classes = useStyles();

    return (
        <>
            <TextField
                variant="outlined"
                margin="dense"
                fullWidth
                id="eventName"
                label={EVENT_FORM_CONTROLS_STRINGS.LABEL_NAME}
                value={formik.values.eventName}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                error={formik.touched.eventName && Boolean(formik.errors.eventName)}
                helperText={formik.touched.eventName && formik.errors.eventName}
            />
            <TextField
                variant="outlined"
                margin="dense"
                fullWidth
                id="eventDescription"
                label={EVENT_FORM_CONTROLS_STRINGS.LABEL_DESCRIPTION}
                multiline={true}
                rows="2"
                rowsMax="4"
                value={formik.values.eventDescription}
                onChange={formik.handleChange}
            />
            <TextField
                variant="outlined"
                margin="dense"
                fullWidth
                id="location"
                label={EVENT_FORM_CONTROLS_STRINGS.LABEL_LOCATION}
                value={formik.values.location}
                onChange={formik.handleChange}
            />
            <KeyboardDateTimePicker
                inputVariant="outlined"
                margin="dense"
                fullWidth
                id="startDateTime"
                label={EVENT_FORM_CONTROLS_STRINGS.LABEL_START_DATE}
                disablePast
                value={formik.values.startDateTime}
                onChange={(date) => formik.setFieldValue("startDateTime", date, true)}
                onBlur={formik.handleBlur}
                error={formik.touched.startDateTime && Boolean(formik.errors.startDateTime)}
                helperText={formik.touched.startDateTime && formik.errors.startDateTime}
                disabled={isEditMode && isDateInPast(formik.values.startDateTime)}
                format={DATE_FORMAT}
                ampm={false}
            />
            <KeyboardDateTimePicker
                inputVariant="outlined"
                margin="dense"
                fullWidth
                id="endDateTime"
                label={EVENT_FORM_CONTROLS_STRINGS.LABEL_END_DATE}
                disablePast
                value={formik.values.endDateTime}
                onChange={(date) => formik.setFieldValue("endDateTime", date, true)}
                minDate={formik.values.startDateTime}
                minDateMessage={EVENT_FORM_CONTROLS_STRINGS.ERROR_END_DATE_BEFORE_START_DATE}
                strictCompareDates
                disabled={isEditMode && isDateInPast(formik.values.endDateTime)}
                format={DATE_FORMAT}
                ampm={false}
            />
            <FormControl variant="outlined" fullWidth className={classes.formControl}>
                <InputLabel id="visibilityLabel">
                    {EVENT_FORM_CONTROLS_STRINGS.LABEL_VISIBILITY}
                </InputLabel>
                <Select
                    variant="outlined"
                    fullWidth
                    margin="dense"
                    id="visibility"
                    name="visibility"
                    label={EVENT_FORM_CONTROLS_STRINGS.LABEL_VISIBILITY}
                    labelId="visibilityLabel"
                    value={formik.values.visibility}
                    onChange={formik.handleChange}
                >
                    {getVisibilities().map((visibility, index) => (
                        <MenuItem key={index} value={visibility}>
                            {visibilityToUIText(visibility)}
                        </MenuItem>
                    ))}
                </Select>
            </FormControl>
            <FormControl variant="outlined" fullWidth className={classes.formControl}>
                <InputLabel id="categoryLabel">
                    {EVENT_FORM_CONTROLS_STRINGS.LABEL_CATEGORY}
                </InputLabel>
                <Select
                    variant="outlined"
                    fullWidth
                    margin="dense"
                    id="category"
                    name="category"
                    labelId="categoryLabel"
                    label={EVENT_FORM_CONTROLS_STRINGS.LABEL_CATEGORY}
                    value={formik.values.category}
                    onChange={formik.handleChange}
                >
                    {CATEGORY_OPTIONS.map((category, index) => (
                        <MenuItem key={index} value={category}>
                            {category}
                        </MenuItem>
                    ))}
                </Select>
            </FormControl>
            <FormControl variant="outlined" fullWidth className={classes.formControl}>
                <Typography>{EVENT_FORM_CONTROLS_STRINGS.LABEL_AGE_RECOMMENDATION}</Typography>
                <Slider
                    id="ageRange"
                    name="ageRange"
                    min={MIN_AGE_RANGE}
                    max={MAX_AGE_RANGE}
                    value={formik.values.ageRange}
                    onChange={(event, value) => formik.setFieldValue("ageRange", value, false)}
                    valueLabelDisplay="auto"
                    marks={AGE_RANGE_MARKS}
                />
            </FormControl>
        </>
    );
};

export default EventFormControls;
