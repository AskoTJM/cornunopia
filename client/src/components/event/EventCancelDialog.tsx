import React, { useContext } from "react";
import {
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle,
    TextField,
    Radio,
    FormControlLabel,
    makeStyles,
    createStyles,
} from "@material-ui/core";
import { useFormik } from "formik";
import { IEvent } from "../../types";
import { AXIOS_BASE_URL, AXIOS_EVENT_URL } from "../../constants/constants";
import axios from "axios";
import SnackbarContext from "../../contexts/SnackbarContext";

interface IEventCancelDialog {
    event: IEvent | null;
    isOpen: boolean;
    hide: () => void;
    setRefreshEventsList: React.Dispatch<React.SetStateAction<boolean>>;
}

enum RADIO_VALUES {
    CANCELLED = "Cancelled",
    NOT_CANCELLED = "Not cancelled",
}

interface IEventCancelFormData {
    textValue: string;
    isCancelled: RADIO_VALUES;
}

enum EVENT_CANCEL_DIALOG_STRINGS {
    TITLE = "Event Cancellation",
    REASON_LABEL = "Reason",
    DESCRIPTION = "Select if you want to cancel this event or not. " +
        "To cancel it, you need to add a short text explaining the reason for the cancellation.",
    REASON_REQUIRED = "Reason is required",
    OK_BUTTON_TEXT = "OK",
    CLOSE_BUTTON_TEXT = "Close",
    STATUS_OK_CANCELLED = "Event cancellation saved",
    STATUS_OK_UNCANCELLED = "Event uncancelled",
    STATUS_ERROR_CANT_CANCEL = "Failed to change cancellation status",
    RADIO_LABEL_CANCELLED = "Cancelled",
    RADIO_LABEL_NOT_CANCELLED = "Not Cancelled",
}

const useStyles = makeStyles((theme) =>
    createStyles({
        textField: {
            marginLeft: theme.spacing(3.5),
            width: "90%",
        },
    }),
);

const EventCancelDialog: React.FC<IEventCancelDialog> = (props) => {
    const { event, isOpen, hide, setRefreshEventsList } = props;
    const { setSnackbarText } = useContext(SnackbarContext);
    const classes = useStyles();

    const cancelOrUncancelEvent = (reasonToCancel?: string | null) => {
        axios.defaults.baseURL = AXIOS_BASE_URL;
        if (event)
            axios
                .patch<IEvent>(`${AXIOS_EVENT_URL}/${event.event_id}/cancel`, {
                    reasonToCancel: reasonToCancel || null,
                })
                .then(() => {
                    setSnackbarText(
                        reasonToCancel
                            ? EVENT_CANCEL_DIALOG_STRINGS.STATUS_OK_CANCELLED
                            : EVENT_CANCEL_DIALOG_STRINGS.STATUS_OK_UNCANCELLED,
                    );
                    setRefreshEventsList(true);
                })
                .catch(() => {
                    setSnackbarText(EVENT_CANCEL_DIALOG_STRINGS.STATUS_ERROR_CANT_CANCEL);
                });
    };

    const validate = (values: IEventCancelFormData) => {
        const errors: Partial<IEventCancelFormData> =
            values.isCancelled === RADIO_VALUES.CANCELLED && values.textValue.trim() === ""
                ? { textValue: EVENT_CANCEL_DIALOG_STRINGS.REASON_REQUIRED }
                : {};
        return errors;
    };

    const formik = useFormik<IEventCancelFormData>({
        initialValues: {
            textValue: event?.cancelled || "",
            isCancelled: event?.cancelled ? RADIO_VALUES.CANCELLED : RADIO_VALUES.NOT_CANCELLED,
        },
        onSubmit: (values) => {
            if (event)
                cancelOrUncancelEvent(
                    values.isCancelled === RADIO_VALUES.CANCELLED ? values.textValue : null,
                );
            hide();
        },
        validate,
        enableReinitialize: true,
    });

    const handleDismiss = () => {
        hide();
    };

    return (
        <Dialog open={isOpen} onClose={hide} disableBackdropClick>
            <form onSubmit={formik.handleSubmit}>
                <DialogTitle>{EVENT_CANCEL_DIALOG_STRINGS.TITLE}</DialogTitle>
                <DialogContent>
                    <DialogContentText>{EVENT_CANCEL_DIALOG_STRINGS.DESCRIPTION}</DialogContentText>
                    <FormControlLabel
                        value={RADIO_VALUES.CANCELLED}
                        control={
                            <Radio
                                checked={formik.values.isCancelled === RADIO_VALUES.CANCELLED}
                                onChange={formik.handleChange}
                                name="isCancelled"
                                size="small"
                                color="primary"
                            />
                        }
                        label={EVENT_CANCEL_DIALOG_STRINGS.RADIO_LABEL_CANCELLED}
                    />
                    <TextField
                        variant="outlined"
                        margin="dense"
                        id="textInputDialogInput"
                        name="textValue"
                        label={EVENT_CANCEL_DIALOG_STRINGS.REASON_LABEL}
                        value={formik.values.textValue}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        error={formik.touched.textValue && Boolean(formik.errors.textValue)}
                        helperText={formik.touched.textValue && formik.errors.textValue}
                        className={classes.textField}
                        disabled={formik.values.isCancelled === RADIO_VALUES.NOT_CANCELLED}
                    />
                    <FormControlLabel
                        value={RADIO_VALUES.NOT_CANCELLED}
                        control={
                            <Radio
                                checked={formik.values.isCancelled === RADIO_VALUES.NOT_CANCELLED}
                                onChange={formik.handleChange}
                                name="isCancelled"
                                size="small"
                                color="primary"
                            />
                        }
                        label={EVENT_CANCEL_DIALOG_STRINGS.RADIO_LABEL_NOT_CANCELLED}
                    />
                </DialogContent>
                <DialogActions>
                    <Button type="submit" color="primary">
                        {EVENT_CANCEL_DIALOG_STRINGS.OK_BUTTON_TEXT}
                    </Button>
                    <Button onClick={handleDismiss}>
                        {EVENT_CANCEL_DIALOG_STRINGS.CLOSE_BUTTON_TEXT}
                    </Button>
                </DialogActions>
            </form>
        </Dialog>
    );
};

export default EventCancelDialog;
