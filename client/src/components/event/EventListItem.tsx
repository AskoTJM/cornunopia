import {
    Accordion,
    Typography,
    makeStyles,
    createStyles,
    Theme,
    Button,
    AccordionSummary,
    AccordionDetails,
    AccordionActions,
    Divider,
} from "@material-ui/core";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import React, { useContext } from "react";
import { IEvent } from "../../types";
import { isEventOwner, isEventInPast, getDateStringWithDistanceToNow } from "../../utils";
import LoginContext from "../../contexts/LoginContext";
import EventInfoTexts from "./EventInfoTexts";
import CheckCircleIcon from "@material-ui/icons/CheckCircle";
import WarningIcon from "@material-ui/icons/Warning";
import { VISIBILITY_PUBLIC } from "../../constants/constants";

export interface IEventListItem {
    event: IEvent;
    expandedItem: string | false;
    setExpandedItem: React.Dispatch<React.SetStateAction<string | false>>;
    openEventDetails: (event: IEvent) => void;
    openEventForEditing: (event: IEvent) => void;
    participateInEvent: (event: IEvent) => void;
    leaveEvent: (event: IEvent) => void;
    isMainPage: boolean;
    openCancellationDialog: (event: IEvent) => void;
}

enum EVENT_LIST_ITEM_STRINGS {
    BUTTON_DETAILS = "Details",
    BUTTON_PARTICIPATE = "Participate",
    BUTTON_LEAVE_EVENT = "Leave event",
    BUTTON_CANCEL = "Cancel Event",
    BUTTON_EDIT_CANCEL = "Edit Cancellation",
    BUTTON_EDIT = "Edit Event",
    CANCELLED = ", CANCELLED",
    YOU_PARTICIPATE = "You are participating",
    YOU_PARTICIPATE_CANCELLED_EVENT = "You are participating in a cancelled event",
}

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        heading: {
            fontSize: theme.typography.pxToRem(15),
            fontWeight: "bold",
            flexBasis: "33.33%",
            flexShrink: 0,
        },
        details: {
            fontSize: theme.typography.pxToRem(15),
            color: theme.palette.text.secondary,
        },
        cancelled: {
            textDecoration: "line-through",
            color: theme.palette.text.disabled,
        },
        participationIcon: {
            marginRight: "0.5em",
            color: theme.palette.success.dark,
        },
        warningIcon: {
            marginRight: "0.5em",
            color: theme.palette.warning.dark,
        },
    }),
);

const EventListItem: React.FC<IEventListItem> = (props) => {
    const {
        event,
        expandedItem,
        setExpandedItem,
        openEventDetails,
        openEventForEditing,
        participateInEvent,
        leaveEvent,
        isMainPage,
        openCancellationDialog,
    } = props;
    const classes = useStyles();
    const { loginAccountId } = useContext(LoginContext);

    const showEventDetails = () => {
        openEventDetails(event);
    };

    const isUserPatricipateInEvent = isMainPage
        ? true
        : !!event?.participants?.find(
              (participant) =>
                  participant.account.account_id === loginAccountId && participant.participating,
          );

    const eventId = String(event.event_id);
    const isItemExpanded = expandedItem === eventId;
    const isOwnedByLoggedInUser = isEventOwner(event, loginAccountId as number);

    return (
        <Accordion
            expanded={isItemExpanded}
            onChange={(event, isExpanded) => setExpandedItem(isExpanded ? eventId : false)}
        >
            <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                {event.cancelled && isUserPatricipateInEvent ? (
                    <span
                        title={EVENT_LIST_ITEM_STRINGS.YOU_PARTICIPATE_CANCELLED_EVENT}
                        className={classes.warningIcon}
                    >
                        <WarningIcon />
                    </span>
                ) : isUserPatricipateInEvent ? (
                    <span
                        title={EVENT_LIST_ITEM_STRINGS.YOU_PARTICIPATE}
                        className={classes.participationIcon}
                    >
                        <CheckCircleIcon />
                    </span>
                ) : null}
                <Typography
                    className={
                        event.cancelled
                            ? `${classes.heading} ${classes.cancelled}`
                            : classes.heading
                    }
                >
                    {event.name}
                </Typography>
                <Typography className={classes.details}>
                    {event.location}
                    {event.location ? ", " : null}
                    {getDateStringWithDistanceToNow(event.start_timedate)}{" "}
                    {event.cancelled ? EVENT_LIST_ITEM_STRINGS.CANCELLED : null}
                </Typography>
            </AccordionSummary>
            <AccordionDetails>
                <EventInfoTexts event={event} onClick={showEventDetails} />
            </AccordionDetails>
            <Divider />
            <AccordionActions>
                <Button size="small" color="primary" onClick={showEventDetails}>
                    {EVENT_LIST_ITEM_STRINGS.BUTTON_DETAILS}
                </Button>

                {isMainPage || isUserPatricipateInEvent ? (
                    <Button size="small" color="primary" onClick={() => leaveEvent(event)}>
                        {EVENT_LIST_ITEM_STRINGS.BUTTON_LEAVE_EVENT}
                    </Button>
                ) : event.visibility === VISIBILITY_PUBLIC ? (
                    <Button size="small" color="primary" onClick={() => participateInEvent(event)}>
                        {EVENT_LIST_ITEM_STRINGS.BUTTON_PARTICIPATE}
                    </Button>
                ) : null}
                {isOwnedByLoggedInUser && (
                    <Button
                        size="small"
                        color="primary"
                        onClick={() => openCancellationDialog(event)}
                    >
                        {event.cancelled
                            ? EVENT_LIST_ITEM_STRINGS.BUTTON_EDIT_CANCEL
                            : EVENT_LIST_ITEM_STRINGS.BUTTON_CANCEL}
                    </Button>
                )}
                {isOwnedByLoggedInUser && !isEventInPast(event) && (
                    <Button size="small" color="primary" onClick={() => openEventForEditing(event)}>
                        {EVENT_LIST_ITEM_STRINGS.BUTTON_EDIT}
                    </Button>
                )}
            </AccordionActions>
        </Accordion>
    );
};

export default EventListItem;
