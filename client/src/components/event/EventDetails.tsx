import React, { useContext, useState, useEffect } from "react";
import axios from "axios";
import { AXIOS_BASE_URL, AXIOS_EVENT_URL } from "../../constants/constants";
import {
    Button,
    Dialog,
    DialogTitle,
    DialogContent,
    DialogActions,
    Grid,
    createStyles,
    makeStyles,
    Theme,
    Typography,
} from "@material-ui/core";
import { IEvent } from "../../types";
import { getDateString, isEventInPast, isEventOwner, visibilityToUIText } from "../../utils";
import LoginContext from "../../contexts/LoginContext";

interface IEventDetails {
    event: IEvent | null;
    hide: () => void;
    openEventForEditing: ((event: IEvent | null) => void) | null;
}

enum EVENT_DETAILS_STRINGS {
    TITLE_STRING = "Event Details",
    NAME = "Event Name",
    DESCRIPTION = "Description",
    LOCATION = "Location",
    OWNER = "Event Owner",
    START_TIME = "Start Date and Time",
    END_TIME = "End Date and Time",
    VISIBILITY = "Who can participate",
    CATEGORY = "Category",
    AGE_RANGE = "Age range recommendation",
    NUM_OF_PARTICIPANTS = "Number of Participants",
    CLOSE_BUTTON = "Close",
    EDIT_BUTTON = "Edit Event",
}

const EMPTY_VALUE = <span>&mdash;</span>;
const DIALOG_EXIT_TRANSITION_DURATION = 0;

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        fieldValue: {
            color: theme.palette.text.secondary,
            margin: theme.spacing(0, 0, 0.5, 2.5),
        },
        containerGrid: {
            minWidth: "25vw",
        },
    }),
);

const EventDetails: React.FC<IEventDetails> = (props) => {
    const { event, hide, openEventForEditing } = props;
    const classes = useStyles();
    const [numParticipants, setNumParticipants] = useState(0);
    const { loginAccountId } = useContext(LoginContext);

    const getNumberOfParticipants = (evt: IEvent) => {
        axios.defaults.baseURL = AXIOS_BASE_URL;
        axios
            .get<{ numParticipants: number }>(`${AXIOS_EVENT_URL}/${evt.event_id}/num_participants`)
            .then((response) => setNumParticipants(response.data.numParticipants))
            .catch(() => setNumParticipants(0));
    };

    useEffect(() => {
        if (event) getNumberOfParticipants(event);
    }, [event]);

    return (
        <Dialog
            open={!!event}
            onClose={hide}
            maxWidth="sm"
            transitionDuration={{ exit: DIALOG_EXIT_TRANSITION_DURATION }}
        >
            <DialogTitle>{EVENT_DETAILS_STRINGS.TITLE_STRING}</DialogTitle>
            <DialogContent dividers>
                <Grid container direction="column" spacing={0} className={classes.containerGrid}>
                    <Grid item>
                        <Typography variant="body2">{EVENT_DETAILS_STRINGS.NAME}:</Typography>
                    </Grid>
                    <Grid item>
                        <Typography variant="body2" className={classes.fieldValue}>
                            {event?.name || EMPTY_VALUE}
                        </Typography>
                    </Grid>

                    <Grid item>
                        <Typography variant="body2">
                            {EVENT_DETAILS_STRINGS.DESCRIPTION}:
                        </Typography>
                    </Grid>
                    <Grid item>
                        <Typography variant="body2" className={classes.fieldValue}>
                            {event?.description || EMPTY_VALUE}
                        </Typography>
                    </Grid>

                    <Grid item>
                        <Typography variant="body2">{EVENT_DETAILS_STRINGS.LOCATION}:</Typography>
                    </Grid>
                    <Grid item>
                        <Typography variant="body2" className={classes.fieldValue}>
                            {event?.location || EMPTY_VALUE}
                        </Typography>
                    </Grid>

                    <Grid item>
                        <Typography variant="body2">{EVENT_DETAILS_STRINGS.OWNER}:</Typography>
                    </Grid>
                    <Grid item>
                        <Typography variant="body2" className={classes.fieldValue}>
                            {event?.owner?.name || EMPTY_VALUE}
                        </Typography>
                    </Grid>

                    <Grid item>
                        <Typography variant="body2">{EVENT_DETAILS_STRINGS.START_TIME}:</Typography>
                    </Grid>
                    <Grid item>
                        <Typography variant="body2" className={classes.fieldValue}>
                            {getDateString(event?.start_timedate) || EMPTY_VALUE}
                        </Typography>
                    </Grid>

                    <Grid item>
                        <Typography variant="body2">{EVENT_DETAILS_STRINGS.END_TIME}:</Typography>
                    </Grid>
                    <Grid item>
                        <Typography variant="body2" className={classes.fieldValue}>
                            {getDateString(event?.end_timedate) || EMPTY_VALUE}
                        </Typography>
                    </Grid>

                    <Grid item>
                        <Typography variant="body2">{EVENT_DETAILS_STRINGS.VISIBILITY}:</Typography>
                    </Grid>
                    <Grid item>
                        <Typography variant="body2" className={classes.fieldValue}>
                            {visibilityToUIText(event?.visibility) || EMPTY_VALUE}
                        </Typography>
                    </Grid>

                    <Grid item>
                        <Typography variant="body2">{EVENT_DETAILS_STRINGS.CATEGORY}:</Typography>
                    </Grid>
                    <Grid item>
                        <Typography variant="body2" className={classes.fieldValue}>
                            {event?.category || EMPTY_VALUE}
                        </Typography>
                    </Grid>

                    <Grid item>
                        <Typography variant="body2">{EVENT_DETAILS_STRINGS.AGE_RANGE}:</Typography>
                    </Grid>
                    <Grid item>
                        <Typography variant="body2" className={classes.fieldValue}>
                            {EMPTY_VALUE}
                            {/* TODO Show age min and max when backend starts to support them */}
                        </Typography>
                    </Grid>

                    <Grid item>
                        <Typography variant="body2">
                            {EVENT_DETAILS_STRINGS.NUM_OF_PARTICIPANTS}:
                        </Typography>
                    </Grid>
                    <Grid item>
                        <Typography variant="body2" className={classes.fieldValue}>
                            {numParticipants}
                        </Typography>
                    </Grid>
                </Grid>
            </DialogContent>

            <DialogActions>
                <Button variant="contained" color="primary" onClick={hide}>
                    {EVENT_DETAILS_STRINGS.CLOSE_BUTTON}
                </Button>
                {openEventForEditing &&
                    !isEventInPast(event) &&
                    isEventOwner(event, loginAccountId as number) && (
                        <Button variant="contained" onClick={() => openEventForEditing(event)}>
                            {EVENT_DETAILS_STRINGS.EDIT_BUTTON}
                        </Button>
                    )}
            </DialogActions>
        </Dialog>
    );
};

export default EventDetails;
