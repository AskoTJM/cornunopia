import {
    Button,
    Dialog,
    DialogTitle,
    DialogContent,
    DialogActions,
    Tabs,
    Tab,
    makeStyles,
} from "@material-ui/core";
import React, { useState, useContext, useEffect } from "react";
import { useFormik } from "formik";
import {
    MIN_NAME_LENGTH,
    MAX_NAME_LENGTH,
    CATEGORY_OPTIONS,
    AXIOS_BASE_URL,
    AXIOS_CREATE_EVENT_URL,
    AXIOS_PUT_EVENT_URL,
    AXIOS_DELETE_EVENT_URL,
    VISIBILITY_PRIVATE,
    DEFAULT_AGE_RANGE,
    AXIOS_EVENT_GROUP_URL,
} from "../../constants/constants";
import { IAccount, IEvent, IEventCreate, IEventGroup, IGroup } from "../../types";
import axios, { AxiosResponse } from "axios";
import DeleteIcon from "@material-ui/icons/Delete";
import EventDeleteDialog from "./EventDeleteDialog";
import SnackbarContext from "../../contexts/SnackbarContext";
import LoginContext from "../../contexts/LoginContext";
import EventFormControls from "./EventFormControls";
import Invites from "../util/Invites";

interface IEventForm {
    eventForEditing: IEvent | null;
    isOpen: boolean;
    setOpen: React.Dispatch<React.SetStateAction<boolean>>;
    setRefreshEventsList: React.Dispatch<React.SetStateAction<boolean>>;
    groupAsParticipants?: IGroup;
}

export interface IEventFormData {
    eventName: string;
    eventDescription: string;
    visibility: string;
    category: string;
    startDateTime: Date | string | null;
    endDateTime: Date | string | null;
    location: string;
    ageRange: number[];
}

enum EVENT_FORM_STRINGS {
    TITLE_CREATE = "Create Event",
    TITLE_EDIT = "Edit Event",
    BUTTON_SAVE = "Save",
    BUTTON_RESET = "Reset",
    BUTTON_DELETE = "Delete",
    BUTTON_CANCEL = "Cancel",
    BUTTON_CREATE = "Create",
    SNACKBAR_SAVED = "Event saved",
    SNACKBAR_CREATED = "Event created",
    SNACKBAR_DELETED = "Event deleted",
    ERROR_SAVING = "Error saving event",
    ERROR_DELETING = "Error deleting event",
    ERROR_NAME_ALL_SPACES = "Event name should not be all spaces",
    ERROR_NAME_LENGTH = "Event name length must be between",
    ERROR_START_DATE_REQUIRED = "Start date and time is required",
    ERROR_DELETE_NO_EVENT = "No event to delete",
    ERROR_DELETE_NOT_YOURS = "You can delete only events that were created by you",
    ERROR_DELETE_HAS_PARTICIPANTS = "You can't delete an event that has participants",
    ERROR_END_DATE_BEFORE_START_DATE = "End date should not be before start date",
    EMPTY = "",
}

enum TAB_VALUES {
    GENERAL = "General",
    INVITES = "Invites",
}

const useStyles = makeStyles({
    tabs: {
        marginBottom: "0.70em",
    },
    dialogContent: {
        paddingTop: "0.25em",
    },
    scrollPaper: {
        alignItems: "baseline",
    },
});

const sendEventGroup = (groupId: number, eventId: number) =>
    axios.post<IEventGroup>(`${AXIOS_EVENT_GROUP_URL}/${groupId}/${eventId}`);

const sendBasicEvent = (
    event: IEventCreate,
    invitedAccountIds: number[],
    editEvent: IEvent | null,
) => {
    const url = editEvent ? `${AXIOS_PUT_EVENT_URL}/${editEvent.event_id}` : AXIOS_CREATE_EVENT_URL;
    const data = { event, invitedAccountIds };
    return editEvent ? axios.put<IEvent>(url, data) : axios.post<IEvent>(url, data);
};

const EventForm: React.FC<IEventForm> = (props) => {
    const { isOpen, setOpen, eventForEditing, setRefreshEventsList, groupAsParticipants } = props;
    const classes = useStyles();
    const isEditMode = !!eventForEditing;
    const [eventForDeletionDialog, setEventForDeletionDialog] = useState<IEvent | null>(null);
    const [tabValue, setTabValue] = useState<string>(TAB_VALUES.GENERAL);
    const [invites, setInvites] = useState<IAccount[]>([]);
    const [originalInvites, setOriginalInvites] = useState<IAccount[]>([]);
    const { setSnackbarText } = useContext(SnackbarContext);
    const { loginAccountId } = useContext(LoginContext);

    useEffect(() => {
        if (isOpen) setTabValue(TAB_VALUES.GENERAL);
    }, [isOpen]);

    const loadInvites = () => {
        const invitedAccounts =
            eventForEditing && !groupAsParticipants
                ? eventForEditing.participants
                      .filter((participant) => participant.invite && !participant.participating)
                      .map((participant) => participant.account)
                : [];
        setInvites(invitedAccounts);
        setOriginalInvites(invitedAccounts);
    };

    useEffect(() => {
        loadInvites();
    }, [eventForEditing]);

    const getInvitedAccountIds = (event: IEventCreate) =>
        event.visibility === VISIBILITY_PRIVATE ? invites.map((account) => account.account_id) : [];

    const sendCreateEvent = (event: IEventCreate) => {
        axios.defaults.baseURL = AXIOS_BASE_URL;
        const invitedAccountIds = groupAsParticipants ? [] : getInvitedAccountIds(event);
        sendBasicEvent(event, invitedAccountIds, eventForEditing)
            .then((response) => {
                const sentEvent = response.data;
                return groupAsParticipants
                    ? sendEventGroup(groupAsParticipants.group_id, sentEvent.event_id)
                    : Promise.resolve({} as AxiosResponse<IEventGroup>);
            })
            .then(() => {
                setSnackbarText(
                    isEditMode
                        ? EVENT_FORM_STRINGS.SNACKBAR_SAVED
                        : EVENT_FORM_STRINGS.SNACKBAR_CREATED,
                );
                setRefreshEventsList(true);
            })
            .catch(() => setSnackbarText(EVENT_FORM_STRINGS.ERROR_SAVING));
    };

    const validateForm = (values: IEventFormData) => {
        const errors: Partial<IEventFormData> = {};

        const eventNameError =
            values.eventName.length < MIN_NAME_LENGTH || values.eventName.length > MAX_NAME_LENGTH
                ? `${EVENT_FORM_STRINGS.ERROR_NAME_LENGTH} ${MIN_NAME_LENGTH} -> ${MAX_NAME_LENGTH}`
                : /^\s+$/.test(values.eventName)
                ? EVENT_FORM_STRINGS.ERROR_NAME_ALL_SPACES
                : EVENT_FORM_STRINGS.EMPTY;
        if (eventNameError) errors.eventName = eventNameError;

        if (!values.startDateTime)
            errors.startDateTime = EVENT_FORM_STRINGS.ERROR_START_DATE_REQUIRED;
        if (
            values.startDateTime instanceof Date &&
            values.endDateTime instanceof Date &&
            values.startDateTime.getTime() > values.endDateTime.getTime()
        )
            errors.endDateTime = EVENT_FORM_STRINGS.ERROR_END_DATE_BEFORE_START_DATE;

        if (tabValue !== TAB_VALUES.GENERAL && Object.keys(errors).length > 0)
            setTabValue(TAB_VALUES.GENERAL);
        return errors;
    };

    const getInitialValues = (): IEventFormData =>
        eventForEditing
            ? {
                  eventName: eventForEditing.name,
                  eventDescription: eventForEditing.description || "",
                  visibility: eventForEditing.visibility || "",
                  category: eventForEditing.category || "",
                  startDateTime: eventForEditing.start_timedate,
                  endDateTime: eventForEditing.end_timedate || null,
                  location: eventForEditing.location || "",
                  ageRange: DEFAULT_AGE_RANGE, // TODO ageRange
              }
            : {
                  eventName: "",
                  eventDescription: "",
                  visibility: VISIBILITY_PRIVATE,
                  category: CATEGORY_OPTIONS[0],
                  startDateTime: null,
                  endDateTime: null,
                  location: "",
                  ageRange: DEFAULT_AGE_RANGE,
              };

    const formik = useFormik<IEventFormData>({
        initialValues: getInitialValues(),
        onSubmit: (values, { resetForm }) => {
            sendCreateEvent({
                name: values.eventName,
                description: values.eventDescription,
                start_timedate:
                    values.startDateTime instanceof Date ? values.startDateTime : new Date(),
                end_timedate: values.endDateTime instanceof Date ? values.endDateTime : undefined,
                category: values.category,
                visibility: values.visibility,
                location: values.location,
                // TODO ageRange
            });
            resetForm();
            setOpen(false);
        },
        validate: validateForm,
        enableReinitialize: true,
    });

    const handleCancel = () => {
        formik.resetForm();
        setOpen(false);
    };

    const handleDialogClose = (event: unknown, reason: string) => {
        if (reason !== "backdropClick") handleCancel();
    };

    const handleDeleteOKClick = (event: IEvent) => {
        setEventForDeletionDialog(null);
        setOpen(false);
        axios.defaults.baseURL = AXIOS_BASE_URL;
        axios
            .delete<IEvent>(`${AXIOS_DELETE_EVENT_URL}/${event.event_id}`)
            .then(() => {
                setRefreshEventsList(true);
                setSnackbarText(EVENT_FORM_STRINGS.SNACKBAR_DELETED);
            })
            .catch(() => {
                setSnackbarText(EVENT_FORM_STRINGS.ERROR_DELETING);
            });
    };

    const handleDelete = () =>
        !eventForEditing
            ? setSnackbarText(EVENT_FORM_STRINGS.ERROR_DELETE_NO_EVENT)
            : eventForEditing.owner?.account_id !== loginAccountId
            ? setSnackbarText(EVENT_FORM_STRINGS.ERROR_DELETE_NOT_YOURS)
            : Array.isArray(eventForEditing.participants) && eventForEditing.participants.length > 0
            ? setSnackbarText(EVENT_FORM_STRINGS.ERROR_DELETE_HAS_PARTICIPANTS)
            : setEventForDeletionDialog(eventForEditing);

    const handleResetClick = () => {
        formik.resetForm();
        loadInvites();
    };

    return (
        <>
            <EventDeleteDialog
                eventForDeletion={eventForDeletionDialog}
                hide={() => setEventForDeletionDialog(null)}
                handleDeleteOKClick={handleDeleteOKClick}
            />
            <Dialog
                open={isOpen}
                onClose={handleDialogClose}
                disableEscapeKeyDown
                classes={{ scrollPaper: classes.scrollPaper }}
                fullWidth
            >
                <form onSubmit={formik.handleSubmit}>
                    <DialogTitle>
                        {isEditMode
                            ? EVENT_FORM_STRINGS.TITLE_EDIT
                            : EVENT_FORM_STRINGS.TITLE_CREATE}
                    </DialogTitle>
                    <DialogContent dividers className={classes.dialogContent}>
                        <Tabs
                            value={tabValue}
                            onChange={(event, newValue) => setTabValue(newValue)}
                            className={classes.tabs}
                        >
                            <Tab label={TAB_VALUES.GENERAL} value={TAB_VALUES.GENERAL} />
                            <Tab
                                label={TAB_VALUES.INVITES}
                                value={TAB_VALUES.INVITES}
                                disabled={
                                    Boolean(groupAsParticipants) ||
                                    formik.values.visibility !== VISIBILITY_PRIVATE
                                }
                            />
                        </Tabs>
                        {tabValue === TAB_VALUES.GENERAL && (
                            <EventFormControls formik={formik} isEditMode={!!eventForEditing} />
                        )}
                        {tabValue === TAB_VALUES.INVITES && (
                            <Invites
                                originalInvites={originalInvites}
                                invites={invites}
                                setInvites={setInvites}
                            />
                        )}
                    </DialogContent>

                    <DialogActions>
                        <Button
                            variant="contained"
                            color="primary"
                            type="submit"
                            disabled={formik.isSubmitting}
                        >
                            {isEditMode
                                ? EVENT_FORM_STRINGS.BUTTON_SAVE
                                : EVENT_FORM_STRINGS.BUTTON_CREATE}
                        </Button>
                        <Button variant="contained" onClick={handleResetClick}>
                            {EVENT_FORM_STRINGS.BUTTON_RESET}
                        </Button>
                        {isEditMode && (
                            <Button
                                variant="contained"
                                onClick={handleDelete}
                                startIcon={<DeleteIcon />}
                            >
                                {EVENT_FORM_STRINGS.BUTTON_DELETE}
                            </Button>
                        )}
                        <Button variant="contained" onClick={handleCancel}>
                            {EVENT_FORM_STRINGS.BUTTON_CANCEL}
                        </Button>
                    </DialogActions>
                </form>
            </Dialog>
        </>
    );
};

export default EventForm;
