import React from "react";
import {
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    DialogContentText,
    Button,
} from "@material-ui/core";
import { IEvent } from "../../types";

enum EVENT_DELETE_STRINGS {
    TITLE_STRING = "Delete Event",
    DELETE_BUTTON_STRING = "Delete",
    CANCEL_BUTTON_STRING = "Cancel",
    CONFIRMATION = "Do you want to delete event ",
}

interface IEventDeleteDialog {
    eventForDeletion: IEvent | null;
    hide: () => void;
    handleDeleteOKClick: (event: IEvent) => void;
}

const EventDeleteDialog: React.FC<IEventDeleteDialog> = (props) => {
    const { eventForDeletion, hide, handleDeleteOKClick } = props;

    return (
        <Dialog open={!!eventForDeletion} onClose={hide} disableBackdropClick>
            <DialogTitle>{EVENT_DELETE_STRINGS.TITLE_STRING}</DialogTitle>
            <DialogContent>
                <DialogContentText>
                    {EVENT_DELETE_STRINGS.CONFIRMATION + String(eventForDeletion?.name) + " ?"}
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button
                    onClick={() => eventForDeletion && handleDeleteOKClick(eventForDeletion)}
                    color="primary"
                >
                    {EVENT_DELETE_STRINGS.DELETE_BUTTON_STRING}
                </Button>
                <Button onClick={hide}>{EVENT_DELETE_STRINGS.CANCEL_BUTTON_STRING}</Button>
            </DialogActions>
        </Dialog>
    );
};

export default EventDeleteDialog;
