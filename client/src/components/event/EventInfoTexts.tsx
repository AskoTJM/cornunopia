import { Grid, Typography, makeStyles, createStyles, Theme } from "@material-ui/core";
import React from "react";
import { IEvent } from "../../types";
import { getDateString, getDateStringWithDistanceToNow } from "../../utils";

interface IEventInfoTexts {
    event: IEvent;
    onClick: (event: IEvent) => void;
}

enum EVENT_INFO_TEXTS_STRINGS {
    TITLE_LOCATION = "Location",
    TITLE_TIME = "Time",
    TITLE_DESCRIPTION = "Description",
    TITLE_CATEGORY = "Category",
    TITLE_REASON_OF_CANCEL = "Cancelled because of",
    MISSING_END_TIME = "(No end time)",
}

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        detailsClickable: {
            cursor: "pointer",
        },
        fieldValue: {
            color: theme.palette.text.secondary,
            margin: theme.spacing(0, 0, 0, 2.5),
        },
    }),
);

const EventInfoTexts: React.FC<IEventInfoTexts> = (props) => {
    const { event, onClick } = props;
    const classes = useStyles();

    return (
        <Grid container direction="column" spacing={0}>
            <div onClick={() => onClick(event)} className={classes.detailsClickable}>
                <Grid item>
                    <Typography variant="body2">
                        {EVENT_INFO_TEXTS_STRINGS.TITLE_LOCATION}:
                    </Typography>
                </Grid>
                <Grid item>
                    {event.location ? (
                        <Typography variant="body2" className={classes.fieldValue}>
                            {event.location}
                        </Typography>
                    ) : null}
                </Grid>

                <Grid item>
                    <Typography variant="body2">{EVENT_INFO_TEXTS_STRINGS.TITLE_TIME}:</Typography>
                </Grid>
                <Grid item>
                    <Typography variant="body2" className={classes.fieldValue}>
                        {getDateStringWithDistanceToNow(event.start_timedate)} &mdash;{" "}
                        {event.end_timedate
                            ? getDateString(event.end_timedate)
                            : EVENT_INFO_TEXTS_STRINGS.MISSING_END_TIME}
                    </Typography>
                </Grid>

                <Grid item>
                    <Typography variant="body2">
                        {EVENT_INFO_TEXTS_STRINGS.TITLE_DESCRIPTION}:
                    </Typography>
                </Grid>
                <Grid item>
                    {event.description ? (
                        <Typography variant="body2" className={classes.fieldValue}>
                            {event.description}
                        </Typography>
                    ) : null}
                </Grid>

                <Grid item>
                    <Typography variant="body2">
                        {EVENT_INFO_TEXTS_STRINGS.TITLE_CATEGORY}:
                    </Typography>
                </Grid>
                <Grid item>
                    <Typography variant="body2" className={classes.fieldValue}>
                        {event.category}
                    </Typography>
                </Grid>

                {event.cancelled && (
                    <>
                        <Grid item>
                            <Typography variant="body2">
                                {EVENT_INFO_TEXTS_STRINGS.TITLE_REASON_OF_CANCEL}:
                            </Typography>
                        </Grid>
                        <Grid item>
                            <Typography variant="body2" className={classes.fieldValue}>
                                {event.cancelled}
                            </Typography>
                        </Grid>
                    </>
                )}
            </div>
        </Grid>
    );
};

export default EventInfoTexts;
