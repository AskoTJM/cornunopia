import React, { useContext } from "react";
import Snackbar from "@material-ui/core/Snackbar";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import SnackbarContext from "../../contexts/SnackbarContext";
import { TEXT_FOR_HIDDEN_SNACKBAR } from "../../constants/constants";

const SNACKBAR_AUTO_HIDE_DURATION = 6000;

const CornuSnackbar: React.FC = () => {
    const { snackbarText, setSnackbarText } = useContext(SnackbarContext);

    const handleClose = () => {
        setSnackbarText(TEXT_FOR_HIDDEN_SNACKBAR);
    };

    return (
        <Snackbar
            open={!!snackbarText}
            autoHideDuration={SNACKBAR_AUTO_HIDE_DURATION}
            onClose={handleClose}
            message={snackbarText}
            action={
                <IconButton size="small" color="inherit" onClick={handleClose}>
                    <CloseIcon fontSize="small" />
                </IconButton>
            }
        />
    );
};

export default CornuSnackbar;
