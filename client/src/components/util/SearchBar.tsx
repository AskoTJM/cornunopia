import React, { useState, useRef, useEffect } from "react";
import {
    createStyles,
    FormControl,
    IconButton,
    InputAdornment,
    InputLabel,
    makeStyles,
    List,
    ListItem,
    ListItemText,
    OutlinedInput,
    Popper,
    Theme,
    ClickAwayListener,
} from "@material-ui/core";
import SearchIcon from "@material-ui/icons/Search";
import ClearIcon from "@material-ui/icons/Clear";

interface ISearchBar<T> {
    label: string;
    searchResults: T[];
    searchFunction: (search: string) => void;
    getKey: (result: T) => string;
    getPrimaryText: (result: T) => string;
    getSecondaryText: (result: T) => string | null;
    onResultClick: (result: T) => void;
}

const TYPING_DELAY_MILLISEC = 500;

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        paper: {
            border: "1px solid",
            borderRadius: theme.shape.borderRadius,
            padding: theme.spacing(1),
            backgroundColor: theme.palette.background.paper,
            marginTop: "7px",
        },
        popper: {
            zIndex: 10000,
        },
        grey: {
            color: theme.palette.action.disabled,
        },
    }),
);

const SearchBar = <T extends unknown>(props: ISearchBar<T>): JSX.Element => {
    const {
        label,
        searchResults,
        searchFunction,
        getKey,
        getPrimaryText,
        getSecondaryText,
        onResultClick,
    } = props;
    const classes = useStyles();
    const [searchString, setSearchString] = useState("");
    const [showList, setShowList] = useState(false);
    const [typingTimeout, setTypingTimeout] = useState<number | null>(null);
    const popperAnchor = useRef<HTMLElement | null>(null);

    useEffect(() => {
        setShowList(searchString.length > 0 && searchResults?.length > 0);
    }, [searchString, searchResults]);

    const handleTextFieldChange = (
        event: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>,
    ) => {
        if (typingTimeout) window.clearTimeout(typingTimeout);
        const text = event.target.value;
        setSearchString(text);
        const timeout = window.setTimeout(() => searchFunction(text), TYPING_DELAY_MILLISEC);
        setTypingTimeout(timeout);
    };

    const handleClearClick = () => {
        setSearchString("");
    };

    const handleResultClick = (result: T) => {
        handleClearClick();
        onResultClick(result);
    };

    const handleClickAway = () => {
        setShowList(false);
    };

    return (
        <ClickAwayListener onClickAway={handleClickAway}>
            <div>
                <FormControl variant="outlined" fullWidth>
                    <InputLabel htmlFor="search-field">{label}</InputLabel>
                    <OutlinedInput
                        id="search-field"
                        type="text"
                        inputRef={popperAnchor}
                        value={searchString}
                        onChange={handleTextFieldChange}
                        startAdornment={
                            <InputAdornment position="start">
                                <SearchIcon className={classes.grey} />
                            </InputAdornment>
                        }
                        endAdornment={
                            <InputAdornment position="end">
                                <IconButton edge="end" onClick={handleClearClick}>
                                    <ClearIcon />
                                </IconButton>
                            </InputAdornment>
                        }
                        margin="dense"
                        notched
                        label={label}
                    />
                </FormControl>

                <Popper
                    id="search-results-popper"
                    open={showList}
                    anchorEl={popperAnchor.current}
                    placement="bottom-start"
                    className={classes.popper}
                >
                    <div className={classes.paper}>
                        <List dense component="nav">
                            {searchResults.map((result) => (
                                <ListItem
                                    key={getKey(result)}
                                    onClick={() => handleResultClick(result)}
                                    button
                                    component="a"
                                >
                                    <ListItemText
                                        primary={getPrimaryText(result)}
                                        secondary={getSecondaryText(result)}
                                    />
                                </ListItem>
                            ))}
                        </List>
                    </div>
                </Popper>
            </div>
        </ClickAwayListener>
    );
};

export default SearchBar;
