import React, { useState } from "react";
import {
    createStyles,
    IconButton,
    List,
    ListItem,
    ListItemIcon,
    ListItemText,
    makeStyles,
    Theme,
    Typography,
} from "@material-ui/core";
import { IAccount } from "../../types";
import DeleteIcon from "@material-ui/icons/Delete";
import axios from "axios";
import { AXIOS_BASE_URL } from "../../constants/constants";
import SearchBar from "./SearchBar";

interface IInvites {
    originalInvites: IAccount[];
    invites: IAccount[];
    setInvites: React.Dispatch<React.SetStateAction<IAccount[]>>;
}

enum INVITES_STRINGS {
    LABEL_SEARCH_USERS = "Search Users",
    NO_INVITES = "No Invites Added",
    DUPLICATE_INVITE = "Can't add the same invite twice",
}

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        listItem: {
            marginBottom: "3px",
            borderRadius: theme.shape.borderRadius,
            backgroundColor: "#f8f8f8",
        },
        newInvite: {
            backgroundColor: "rgb(212, 247, 205)",
        },
    }),
);

const removeDuplicateInvites = (searchResults: IAccount[], invitedAccounts: IAccount[]) =>
    searchResults.filter(
        (account) => !invitedAccounts.some((invited) => invited.account_id === account.account_id),
    );

const Invites: React.FC<IInvites> = (props) => {
    const { originalInvites, invites, setInvites } = props;
    const classes = useStyles();
    const [userSearchResults, setUserSearchResults] = useState<IAccount[]>([]);

    const removeInvite = (accountToRemove: IAccount) => {
        const filterFunction = (account: IAccount) =>
            account.account_id !== accountToRemove.account_id;
        setInvites(invites.filter(filterFunction));
    };

    const searchUsers = (search: string) => {
        axios.defaults.baseURL = AXIOS_BASE_URL;
        axios
            .get<IAccount[]>(`/search_user?search=${search}`)
            .then((response) => {
                setUserSearchResults(removeDuplicateInvites(response.data, invites));
            })
            .catch(() => undefined);
    };

    const addInvite = (account: IAccount) => setInvites((prev) => [...prev, account]);

    const isOriginalInvite = (accountId: number) =>
        originalInvites.some((account) => account.account_id === accountId);

    return (
        <>
            <SearchBar
                label={INVITES_STRINGS.LABEL_SEARCH_USERS}
                searchResults={userSearchResults}
                searchFunction={searchUsers}
                getKey={(result) => `${result.account_id}`}
                getPrimaryText={(result) => result.name}
                getSecondaryText={(result) => result.email}
                onResultClick={addInvite}
            />

            <List dense>
                {invites.length > 0 ? (
                    invites.map((account) => {
                        const isOriginal = isOriginalInvite(account.account_id);
                        return (
                            <ListItem
                                key={account.account_id}
                                className={
                                    isOriginal
                                        ? classes.listItem
                                        : `${classes.listItem} ${classes.newInvite}`
                                }
                            >
                                <ListItemText primary={account.name} secondary={account.email} />
                                <ListItemIcon>
                                    <IconButton onClick={() => removeInvite(account)}>
                                        <DeleteIcon fontSize="small" />
                                    </IconButton>
                                </ListItemIcon>
                            </ListItem>
                        );
                    })
                ) : (
                    <Typography variant="body2">{INVITES_STRINGS.NO_INVITES}</Typography>
                )}
            </List>
        </>
    );
};

export default Invites;
