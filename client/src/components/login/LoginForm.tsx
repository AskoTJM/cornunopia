import React, { useState, useContext } from "react";
import {
    Button,
    CssBaseline,
    TextField,
    FormControlLabel,
    Checkbox,
    Link,
    Grid,
    Typography,
    makeStyles,
    Container,
} from "@material-ui/core/";
import { useHistory } from "react-router-dom";
import LoginContext from "../../contexts/LoginContext";
import SnackbarContext from "../../contexts/SnackbarContext";
import { AXIOS_LOGIN_URL, REACT_ROUTES } from "../../constants/constants";
import axios from "axios";
import { saveAccessToken } from "../../utils";
import { IAccount } from "../../types";

interface ILogin {}
interface IAccountAndToken {
    account: IAccount;
    token: string;
}

enum LOGIN_FORM_STRINGS {
    TITLE_SIGNIN = "Sign in",
    LABEL_EMAIL = "Email address",
    LABEL_PASSWORD = "Password",
    LABEL_REMEMBER = "Remember me",
    BUTTON_SIGNIN = "Sign In",
    FORGOT_PASSWORD = "Forgot password?",
    REGISTER = "Don't have an account? Register here.",
    INVALID_LOGIN = "Invalid email or password",
}

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: "100%",
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

const LoginForm: React.FC<ILogin> = () => {
    const classes = useStyles();
    const history = useHistory();
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const { saveAccount } = useContext(LoginContext);
    const { setSnackbarText } = useContext(SnackbarContext);

    const handleLoginSubmit = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        axios
            .post<IAccountAndToken>(AXIOS_LOGIN_URL, { email, password })
            .then((response) => {
                saveAccessToken(response.data.token);
                saveAccount(response.data.account.account_id);
                history.push(REACT_ROUTES.MAIN);
            })
            .catch(() => setSnackbarText(LOGIN_FORM_STRINGS.INVALID_LOGIN));
    };

    return (
        <Container component="main" maxWidth="xs" className={classes.paper}>
            <CssBaseline />
            <Typography component="h1" variant="h5">
                {LOGIN_FORM_STRINGS.TITLE_SIGNIN}
            </Typography>
            <form className={classes.form} noValidate onSubmit={handleLoginSubmit}>
                <TextField
                    variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    id="emailLogin"
                    label={LOGIN_FORM_STRINGS.LABEL_EMAIL}
                    name="email"
                    autoComplete="email"
                    autoFocus
                    value={email}
                    onChange={(event) => setEmail(event.target.value)}
                />
                <TextField
                    variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    name="password"
                    label={LOGIN_FORM_STRINGS.LABEL_PASSWORD}
                    type="password"
                    id="passwordLogin"
                    autoComplete="current-password"
                    value={password}
                    onChange={(event) => setPassword(event.target.value)}
                />
                <FormControlLabel
                    control={<Checkbox value="remember" color="primary" />}
                    label={LOGIN_FORM_STRINGS.LABEL_REMEMBER}
                />
                <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    color="primary"
                    className={classes.submit}
                >
                    {LOGIN_FORM_STRINGS.BUTTON_SIGNIN}
                </Button>
                <Grid container>
                    <Grid item xs>
                        <Link variant="body2">{LOGIN_FORM_STRINGS.FORGOT_PASSWORD}</Link>
                    </Grid>
                    <Grid item>
                        <Link onClick={() => history.push("register")} variant="body2">
                            {LOGIN_FORM_STRINGS.REGISTER}
                        </Link>
                    </Grid>
                </Grid>
            </form>
        </Container>
    );
};

export default LoginForm;
