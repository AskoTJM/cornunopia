import {
    Button,
    CssBaseline,
    TextField,
    Typography,
    Container,
    makeStyles,
} from "@material-ui/core";
import React, { useEffect, useState, useContext } from "react";
import { KeyboardDatePicker } from "@material-ui/pickers";
import { MaterialUiPickersDate } from "@material-ui/pickers/typings/date";
import axios from "axios";
import { differenceInCalendarYears } from "date-fns";
import { useHistory } from "react-router-dom";
import SnackbarContext from "../../contexts/SnackbarContext";

interface IRegistrationForm {
    email: string;
    username: string;
    birthdate: MaterialUiPickersDate | null;
    password: string;
    password_retyped?: string;
}

interface IRegistration {}
interface IErrorMessage {
    error: boolean;
    message: string;
}

enum REGISTRATION_FORM_STRINGS {
    BUTTON_REGISTER = "Register",
    ERROR_EMAIL_REQUIRED = "Email is required",
    ERROR_EMAIL_FORMAT = "Not a valid email format.",
    ERROR_USERNAME_REQUIRED = "username is required.",
    ERROR_USERNAME_LENGTH = "Maximum length for username is",
    ERROR_PASSWORD_RETYPE = "You need to type in password two times.",
    ERROR_PASSWORD_DONT_MATCH = "Passwords do not match.",
    ERROR_AGE_FUTURE_HUMAN = "You haven`t born yet.",
    ERROR_AGE_LIMIT = "Age limit for the service is",
    SUCCESS_EMAIL_FORMAT = "Email format valid.",
    SUCCESS_USERNAME_FORMAT = "Username has valid format.",
    SUCCESS_PASSWORD_MATCH = "Passwords match.",
    SUCCESS_AGE = "Age requirement fulfilled.",
    LABEL_EMAIL = "Email Address",
    LABEL_ACCOUNTNAME = "Username",
    LABEL_BIRTHDATE = "Date of Birth",
    LABEL_PASSWORD = "Password",
    LABEL_RETYPE_PASSWORD = "Retype your Password",
}

const DATEFORMAT = "dd/MM/yyyy";

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: "100%",
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

const validateEmailFormat = (
    email: string,
    setEmailError: React.Dispatch<React.SetStateAction<IErrorMessage>>,
) => {
    const emailCheck =
        /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/i;
    const result: IErrorMessage =
        email === ""
            ? { error: true, message: REGISTRATION_FORM_STRINGS.ERROR_EMAIL_REQUIRED }
            : emailCheck.test(email)
            ? { error: false, message: REGISTRATION_FORM_STRINGS.SUCCESS_EMAIL_FORMAT }
            : { error: true, message: REGISTRATION_FORM_STRINGS.ERROR_EMAIL_FORMAT };
    setEmailError({ error: result.error, message: result.message });
};

const validateUserNameFormat = (
    username: string,
    setUserNameError: React.Dispatch<React.SetStateAction<IErrorMessage>>,
) => {
    const maxUserNameLength: number = 20;
    const result: IErrorMessage =
        username === ""
            ? { error: true, message: REGISTRATION_FORM_STRINGS.ERROR_USERNAME_REQUIRED }
            : username.length > maxUserNameLength
            ? {
                  error: true,
                  message: `${REGISTRATION_FORM_STRINGS.ERROR_USERNAME_LENGTH} ${maxUserNameLength}`,
              }
            : { error: false, message: REGISTRATION_FORM_STRINGS.SUCCESS_USERNAME_FORMAT };
    setUserNameError({ error: result.error, message: result.message });
};

const validateReTypedPasswordFormat = (
    password: string,
    passwordRetyped: string,
    setPasswordError: React.Dispatch<React.SetStateAction<IErrorMessage>>,
) => {
    const result: IErrorMessage =
        passwordRetyped === ""
            ? { error: true, message: REGISTRATION_FORM_STRINGS.ERROR_PASSWORD_RETYPE }
            : password !== passwordRetyped
            ? { error: true, message: REGISTRATION_FORM_STRINGS.ERROR_PASSWORD_DONT_MATCH }
            : { error: false, message: REGISTRATION_FORM_STRINGS.SUCCESS_PASSWORD_MATCH };
    setPasswordError({ error: result.error, message: result.message });
};

const validateAge = (
    birthDate: Date | null,
    setBirthDateError: React.Dispatch<React.SetStateAction<IErrorMessage>>,
) => {
    const minimumAge: number = 16;
    const result: IErrorMessage =
        birthDate === null
            ? { error: true, message: REGISTRATION_FORM_STRINGS.ERROR_AGE_FUTURE_HUMAN }
            : differenceInCalendarYears(new Date(), birthDate) < minimumAge
            ? {
                  error: true,
                  message: `${REGISTRATION_FORM_STRINGS.ERROR_AGE_LIMIT} ${minimumAge}.`,
              }
            : { error: false, message: REGISTRATION_FORM_STRINGS.SUCCESS_AGE };
    setBirthDateError({ error: result.error, message: result.message });
};

function sendRegistrationData(
    regForm: IRegistrationForm,
    setRegistrationSuccessfull: React.Dispatch<React.SetStateAction<boolean>>,
    setSnackbarText: React.Dispatch<React.SetStateAction<string>>,
): void {
    axios.defaults.baseURL = "http://localhost:5000";
    axios
        .post("/user", {
            email: regForm.email,
            name: regForm.username,
            date_of_birth: regForm.birthdate,
            password: regForm.password,
        })
        .then(() => {
            setRegistrationSuccessfull(true);
        })
        .catch(() => {
            setSnackbarText("Unable to register user");
        });
}

const RegistrationForm: React.FC<IRegistration> = () => {
    const classes = useStyles();
    const history = useHistory();

    const [regEmail, setRegEmail] = useState("");
    const [regUserName, setRegUserName] = useState("");
    const [regSelectedDate, setRegSelectedDate] = useState<Date | null>(null);
    const [regPassword, setRegPassword] = useState("");
    const [regPasswordRetyped, setRegPasswordRetyped] = useState("");
    const [registrationSuccessfull, setRegistrationSuccessfull] = useState(false);

    const [emailError, setEmailError] = useState<IErrorMessage>({ error: false, message: "" });
    const [userNameError, setUserNameError] = useState<IErrorMessage>({
        error: false,
        message: "",
    });
    const [passwordError, setPasswordError] = useState<IErrorMessage>({
        error: false,
        message: "",
    });
    const [birthDateError, setBirthDateError] = useState<IErrorMessage>({
        error: false,
        message: "",
    });
    const { setSnackbarText } = useContext(SnackbarContext);

    const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        if (
            !emailError.error &&
            !userNameError.error &&
            !passwordError.error &&
            !birthDateError.error
        )
            sendRegistrationData(
                {
                    email: regEmail,
                    username: regUserName,
                    birthdate: regSelectedDate,
                    password: regPassword,
                },
                setRegistrationSuccessfull,
                setSnackbarText,
            );
    };

    useEffect(() => {
        if (registrationSuccessfull === true) {
            setRegEmail("");
            setRegUserName("");
            setRegSelectedDate(null);
            setRegPassword("");
            setRegPasswordRetyped("");
            history.push("login");
        }
    }, [registrationSuccessfull]);

    return (
        <Container component="main" maxWidth="xs">
            <CssBaseline />
            <div className={classes.paper}>
                <Typography component="h1" variant="h5">
                    Register to Cornunopia
                </Typography>
                <form onSubmit={handleSubmit} className={classes.form}>
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        id="email"
                        label={REGISTRATION_FORM_STRINGS.LABEL_EMAIL}
                        onChange={(e) => {
                            setRegEmail(e.target.value);
                            validateEmailFormat(e.target.value, setEmailError);
                        }}
                        name="email"
                        autoComplete="email"
                        autoFocus
                        value={regEmail}
                        error={emailError.error}
                    />
                    {emailError.error ? <Typography>{emailError.message}</Typography> : <></>}
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        id="username"
                        label={REGISTRATION_FORM_STRINGS.LABEL_ACCOUNTNAME}
                        name="username"
                        autoComplete=""
                        onChange={(e) => {
                            setRegUserName(e.target.value);
                            validateUserNameFormat(e.target.value, setUserNameError);
                        }}
                        value={regUserName}
                        error={userNameError.error}
                    />
                    {userNameError.error ? <Typography>{userNameError.message}</Typography> : <></>}

                    <KeyboardDatePicker
                        inputVariant="outlined"
                        required
                        fullWidth
                        id="birthdate"
                        placeholder="Birthdate"
                        name="birthdate"
                        value={regSelectedDate}
                        label={REGISTRATION_FORM_STRINGS.LABEL_BIRTHDATE}
                        onChange={(date) => {
                            if (date !== null) {
                                setRegSelectedDate(date);
                                validateAge(date, setBirthDateError);
                            }
                        }}
                        format={DATEFORMAT}
                        maxDate={Date()}
                        error={birthDateError.error}
                    />
                    {birthDateError.error ? (
                        <Typography>{birthDateError.message}</Typography>
                    ) : (
                        <></>
                    )}
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        name="password"
                        label={REGISTRATION_FORM_STRINGS.LABEL_PASSWORD}
                        type="password"
                        id="password"
                        autoComplete=""
                        onChange={(e) => setRegPassword(e.target.value)}
                        value={regPassword}
                        error={passwordError.error}
                    />
                    {passwordError.error ? <Typography>{passwordError.message}</Typography> : <></>}
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        name="password_retyped"
                        label={REGISTRATION_FORM_STRINGS.LABEL_RETYPE_PASSWORD}
                        type="password"
                        id="password_retyped"
                        autoComplete=""
                        onChange={(e) => {
                            setRegPasswordRetyped(e.target.value);
                            validateReTypedPasswordFormat(
                                e.target.value,
                                regPassword,
                                setPasswordError,
                            );
                        }}
                        value={regPasswordRetyped}
                        error={passwordError.error}
                    />
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                    >
                        {REGISTRATION_FORM_STRINGS.BUTTON_REGISTER}
                    </Button>
                </form>
            </div>
        </Container>
    );
};

export default RegistrationForm;
