import React, { useContext } from "react";
import { Route, Redirect } from "react-router-dom";
import LoginContext from "../../contexts/LoginContext";
import { getAccessToken } from "../../utils";

interface IProtectedRoute {
    [key: string]: unknown;
    publicContent?: React.ReactNode;
}

const ProtectedRoute: React.FC<IProtectedRoute> = (props) => {
    const { children, publicContent, ...rest } = props;
    const { loginAccountId } = useContext(LoginContext);

    return (
        <Route
            {...rest}
            render={() =>
                loginAccountId && getAccessToken()
                    ? children
                    : publicContent || <Redirect to="/login" />
            }
        />
    );
};

export default ProtectedRoute;
