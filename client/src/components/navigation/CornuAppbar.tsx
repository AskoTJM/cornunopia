import { Tabs, Tab, AppBar, Toolbar, IconButton, Menu, MenuItem } from "@material-ui/core";
import { Link, useHistory } from "react-router-dom";
import React, { useState, useEffect, useContext } from "react";
import Logo from "../../cornunopia_logo.png";
import LoginContext from "../../contexts/LoginContext";
import { setAxiosInterceptors, logoutUser, getAccessToken } from "../../utils";
import { AccountCircle } from "@material-ui/icons";
import { REACT_ROUTES } from "../../constants/constants";

interface ICornuAppbar {}

enum CORNUAPPBAR_STRINGS {
    LABEL_MAIN = "Main",
    LABEL_EVENTS = "Events",
    LABEL_GROUPS = "Groups",
    LABEL_POSTS = "Posts",
    LABEL_LOGIN = "Login",
    LABEL_PROFILE = "Profile",
    LABEL_LOGOUT = "Log out",
}

const allTabs = [
    { label: CORNUAPPBAR_STRINGS.LABEL_MAIN, to: REACT_ROUTES.MAIN },
    { label: CORNUAPPBAR_STRINGS.LABEL_EVENTS, to: REACT_ROUTES.EVENT },
    { label: CORNUAPPBAR_STRINGS.LABEL_GROUPS, to: REACT_ROUTES.GROUP },
    { label: CORNUAPPBAR_STRINGS.LABEL_LOGIN, to: REACT_ROUTES.LOGIN },
    { label: CORNUAPPBAR_STRINGS.LABEL_POSTS, to: REACT_ROUTES.POST },
];

const checkTabValue = (newValue: string) =>
    allTabs.some((tab) => tab.to === newValue) ? newValue : false;

const CornuAppbar: React.FC<ICornuAppbar> = () => {
    const history = useHistory();
    const [tab, setTab] = useState<string | false>(REACT_ROUTES.MAIN);
    const { loginAccountId } = useContext(LoginContext);
    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
    const open = Boolean(anchorEl);

    const logOut = () => {
        localStorage.removeItem("token");
        localStorage.removeItem("accountID");
    };

    const handleMenu = (event: React.MouseEvent<HTMLElement>) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const handleLogout = () => {
        logoutUser(history, () => logOut());
        handleClose();
    };

    const handleTabChange = (event: React.ChangeEvent<unknown>, newValue: string) => {
        setTab(newValue);
    };

    const handleGoToProfile = () => {
        history.push(`${REACT_ROUTES.PROFILE}`);
        handleClose();
    };

    useEffect(() => {
        const unlistenCleanUp = history.listen((listener) => {
            setTab(checkTabValue(listener.pathname));
        });
        return unlistenCleanUp;
    }, []);

    useEffect(() => {
        setAxiosInterceptors(history, () => logOut());
    }, []);

    const isLoggedIn = (): boolean => !!(loginAccountId && getAccessToken());

    return (
        <AppBar position="static">
            <Toolbar>
                <img src={Logo as string} alt="Cornunopia logo" />
                <Tabs value={tab} onChange={handleTabChange}>
                    {allTabs.map((tab) =>
                        tab.to === REACT_ROUTES.LOGIN ? null : (
                            <Tab
                                key={tab.to}
                                label={tab.label}
                                component={Link}
                                to={tab.to}
                                value={tab.to}
                            />
                        ),
                    )}
                    {!isLoggedIn() && (
                        <Tab
                            label={CORNUAPPBAR_STRINGS.LABEL_LOGIN}
                            component={Link}
                            to="/login"
                            value="/login"
                        />
                    )}
                </Tabs>
                {isLoggedIn() && (
                    <div>
                        <IconButton
                            aria-label="account of current user"
                            aria-controls="menu-appbar"
                            aria-haspopup="true"
                            onClick={handleMenu}
                            color="inherit"
                        >
                            <AccountCircle />
                        </IconButton>
                        <Menu
                            id="menu-appbar"
                            anchorEl={anchorEl}
                            anchorOrigin={{
                                vertical: "top",
                                horizontal: "right",
                            }}
                            keepMounted
                            transformOrigin={{
                                vertical: "top",
                                horizontal: "right",
                            }}
                            open={open}
                            onClose={handleClose}
                        >
                            <MenuItem onClick={handleGoToProfile}>
                                {CORNUAPPBAR_STRINGS.LABEL_PROFILE}
                            </MenuItem>
                            <MenuItem onClick={handleLogout}>
                                {CORNUAPPBAR_STRINGS.LABEL_LOGOUT}
                            </MenuItem>
                        </Menu>
                    </div>
                )}
            </Toolbar>
        </AppBar>
    );
};

export default CornuAppbar;
