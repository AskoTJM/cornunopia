import {
    createStyles,
    FormControl,
    InputLabel,
    makeStyles,
    MenuItem,
    Select,
    TextField,
} from "@material-ui/core";
import { FormikProps } from "formik";
import React from "react";
import { CATEGORY_OPTIONS } from "../../constants/constants";
import { getVisibilities, visibilityToUIText } from "../../utils";
import { IGroupFormData } from "./GroupForm";

interface IGroupFormControls {
    formik: FormikProps<IGroupFormData>;
}

enum GROUP_FORM_CONTROLS_STRINGS {
    NAME = "Group name",
    DESCRIPTION = "Description",
    VISIBILITY = "Who can be a member in the group",
    CATEGORY = "Category",
}

const useStyles = makeStyles((theme) =>
    createStyles({
        formControl: {
            margin: theme.spacing(1, 0, 0.5, 0),
        },
    }),
);

const GroupFormControls: React.FC<IGroupFormControls> = (props) => {
    const { formik } = props;
    const classes = useStyles();

    return (
        <>
            <TextField
                fullWidth
                margin="dense"
                id="groupName"
                label={GROUP_FORM_CONTROLS_STRINGS.NAME}
                variant="outlined"
                value={formik.values.groupName}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                error={formik.touched.groupName && Boolean(formik.errors.groupName)}
                helperText={formik.touched.groupName && formik.errors.groupName}
            />
            <TextField
                variant="outlined"
                fullWidth
                margin="dense"
                id="groupDescription"
                label={GROUP_FORM_CONTROLS_STRINGS.DESCRIPTION}
                multiline={true}
                rows="4"
                rowsMax="4"
                value={formik.values.groupDescription}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                error={formik.touched.groupDescription && Boolean(formik.errors.groupDescription)}
                helperText={formik.touched.groupDescription && formik.errors.groupDescription}
            />
            <FormControl variant="outlined" fullWidth className={classes.formControl}>
                <InputLabel id="visibilityLabel">
                    {GROUP_FORM_CONTROLS_STRINGS.VISIBILITY}
                </InputLabel>
                <Select
                    variant="outlined"
                    id="visibility"
                    name="visibility"
                    margin="dense"
                    fullWidth
                    label={GROUP_FORM_CONTROLS_STRINGS.VISIBILITY}
                    value={formik.values.visibility}
                    onChange={formik.handleChange}
                >
                    {getVisibilities().map((visibility, index) => (
                        <MenuItem key={index} value={visibility}>
                            {visibilityToUIText(visibility)}
                        </MenuItem>
                    ))}
                </Select>
            </FormControl>

            <FormControl variant="outlined" fullWidth className={classes.formControl}>
                <InputLabel id="categoryLabel">{GROUP_FORM_CONTROLS_STRINGS.CATEGORY}</InputLabel>
                <Select
                    variant="outlined"
                    id="category"
                    name="category"
                    margin="dense"
                    fullWidth
                    label={GROUP_FORM_CONTROLS_STRINGS.CATEGORY}
                    value={formik.values.category}
                    onChange={formik.handleChange}
                >
                    {CATEGORY_OPTIONS.map((category, index) => (
                        <MenuItem key={index} value={category}>
                            {category}
                        </MenuItem>
                    ))}
                </Select>
            </FormControl>
        </>
    );
};

export default GroupFormControls;
