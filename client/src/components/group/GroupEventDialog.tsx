import React, { useState } from "react";
import {
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    DialogContentText,
    Button,
} from "@material-ui/core";
import { IGroup } from "../../types";
import EventForm from "../event/EventForm";

interface IGroupEventDialog {
    group: IGroup | null;
    hide: () => void;
}

enum GROUP_DELETE_DIALOG_STRINGS {
    TITLE = "Event for Group",
    PROMPT = "Do you want to create a new event, in which all members of the group will be added as participants?",
    BUTTON_CREATE = "Create",
    BUTTON_CANCEL = "Cancel",
}

const GroupEventDialog: React.FC<IGroupEventDialog> = (props) => {
    const { group, hide } = props;
    const [groupAsParticipants, setGroupAsParticipants] = useState<IGroup | null>(null);

    const handleCreateClick = () => {
        if (group) setGroupAsParticipants(group);
        hide();
    };

    return (
        <>
            <EventForm
                eventForEditing={null}
                isOpen={!!groupAsParticipants}
                setOpen={() => setGroupAsParticipants(null)}
                setRefreshEventsList={() => undefined}
                groupAsParticipants={groupAsParticipants ? groupAsParticipants : undefined}
            />

            <Dialog open={!!group} onClose={hide} disableBackdropClick>
                <DialogTitle>{`${GROUP_DELETE_DIALOG_STRINGS.TITLE} '${String(
                    group?.group_name,
                )}'`}</DialogTitle>

                <DialogContent>
                    <DialogContentText>{GROUP_DELETE_DIALOG_STRINGS.PROMPT}</DialogContentText>
                </DialogContent>

                <DialogActions>
                    <Button onClick={handleCreateClick} color="primary">
                        {GROUP_DELETE_DIALOG_STRINGS.BUTTON_CREATE}
                    </Button>
                    <Button onClick={hide} color="primary">
                        {GROUP_DELETE_DIALOG_STRINGS.BUTTON_CANCEL}
                    </Button>
                </DialogActions>
            </Dialog>
        </>
    );
};

export default GroupEventDialog;
