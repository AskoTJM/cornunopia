import React from "react";
import { createStyles, Grid, makeStyles, Typography } from "@material-ui/core";
import { IGroup, IGroupAccount } from "../../types";
import { visibilityToUIText } from "../../utils";

interface IGroupInfoTexts {
    group: IGroup;
}

enum GROUP_INFO_TEXTS_STRINGS {
    LABEL_PARTICIPANTS = "Number of Members:",
    LABEL_DESCRIPTION = "Description:",
    LABEL_CATEGORY = "Category:",
    LABEL_VISIBILITY = "Who Can Be a Member:",
}

const useStyles = makeStyles((theme) =>
    createStyles({
        details: {
            fontSize: theme.typography.pxToRem(15),
            color: theme.palette.text.secondary,
        },
        fieldValue: {
            color: theme.palette.text.secondary,
            margin: theme.spacing(0, 0, 0, 2.5),
        },
    }),
);

const EMPTY_VALUE = <span>&mdash;</span>;

const getNumberOfParticipants = (participants?: IGroupAccount[]) =>
    Array.isArray(participants)
        ? participants.reduce(
              (count, participant) => (participant.participating ? count + 1 : count),
              0,
          )
        : 0;

const GroupInfoTexts: React.FC<IGroupInfoTexts> = (props) => {
    const { group } = props;
    const classes = useStyles();

    return (
        <Grid container direction="column" spacing={0}>
            <Grid item>
                <Typography variant="body2">
                    {GROUP_INFO_TEXTS_STRINGS.LABEL_DESCRIPTION}
                </Typography>
            </Grid>
            <Grid item>
                <Typography variant="body2" className={classes.fieldValue}>
                    {group.group_description || EMPTY_VALUE}
                </Typography>
            </Grid>

            <Grid item>
                <Typography variant="body2">{GROUP_INFO_TEXTS_STRINGS.LABEL_CATEGORY}</Typography>
            </Grid>
            <Grid item>
                <Typography variant="body2" className={classes.fieldValue}>
                    {group.category || EMPTY_VALUE}
                </Typography>
            </Grid>

            <Grid item>
                <Typography variant="body2">{GROUP_INFO_TEXTS_STRINGS.LABEL_VISIBILITY}</Typography>
            </Grid>
            <Grid item>
                <Typography variant="body2" className={classes.fieldValue}>
                    {visibilityToUIText(group.visibility)}
                </Typography>
            </Grid>

            <Grid item>
                <Typography variant="body2">
                    {GROUP_INFO_TEXTS_STRINGS.LABEL_PARTICIPANTS}
                </Typography>
            </Grid>
            <Grid item>
                <Typography variant="body2" className={classes.fieldValue}>
                    {getNumberOfParticipants(group.participants)}
                </Typography>
            </Grid>
        </Grid>
    );
};

export default GroupInfoTexts;
