import {
    Button,
    createStyles,
    makeStyles,
    Dialog,
    DialogTitle,
    DialogContent,
    DialogActions,
} from "@material-ui/core";
import axios from "axios";
import React, { useState } from "react";
import { useContext } from "react";
import {
    AXIOS_BASE_URL,
    AXIOS_CREATE_GROUP_URL,
    AXIOS_PUT_GROUP_URL,
    CATEGORY_OPTIONS,
    MAX_NAME_LENGTH,
    MIN_NAME_LENGTH,
    AXIOS_DELETE_GROUP_URL,
    VISIBILITY_PRIVATE,
} from "../../constants/constants";
import { IGroup } from "../../types";
import DeleteIcon from "@material-ui/icons/Delete";
import GroupDeleteDialog from "./GroupDeleteDialog";
import SnackbarContext from "../../contexts/SnackbarContext";
import LoginContext from "../../contexts/LoginContext";
import { useFormik } from "formik";
import GroupFormControls from "./GroupFormControls";

interface IGroupForm {
    open: boolean;
    closeModal: () => void;
    groupForEditing: IGroup | null;
    setRefreshGroupList: React.Dispatch<React.SetStateAction<boolean>>;
}

export interface IGroupFormData {
    groupName: string;
    groupDescription: string;
    visibility: string;
    category: string;
}

enum GROUP_FORM_STRINGS {
    CANCEL = "Cancel",
    RESET = "Reset",
    CREATE = "Create",
    SAVE = "Save",
    DELETE = "Delete",
    TITLE_EDIT = "Edit Group",
    TITLE_CREATE = "Create Group",
    ERROR_NAME_LENGTH = "Name length must be",
    SNACKBAR_SUCCESS_CREATE = "Group successfully created.",
    SNACKBAR_SUCCESS_SAVE = "Group successfully saved.",
    SNACKBAR_ERROR_SAVING_GROUP = "Error saving group",
    SNACKBAR_ERROR_NO_GROUP_TO_DELETE = "No group to delete",
    SNACKBAR_ERROR_NOT_YOURS = "You can delete only groups that were created by you",
    SNACKBAR_ERROR_DELETE = "Error deleting group",
    SNACKBAR_ERROR_DELETE_HAS_PARTICIPANTS = "You can't delete a group that has participants",
    SNACKBAR_DELETED = "Group deleted",
}

const useStyles = makeStyles(() =>
    createStyles({
        dialogContent: {
            paddingTop: "0.25em",
        },
        scrollPaper: {
            alignItems: "baseline",
        },
    }),
);

const GroupForm: React.FC<IGroupForm> = (props) => {
    const classes = useStyles();
    const { open, closeModal, groupForEditing, setRefreshGroupList } = props;

    const [groupForDeletionDialog, setGroupForDeletionDialog] = useState<IGroup | null>(null);
    const { setSnackbarText } = useContext(SnackbarContext);
    const { loginAccountId } = useContext(LoginContext);

    const validateForm = (values: IGroupFormData) => {
        const errors: Partial<IGroupFormData> = {};

        if (values.groupName.length < MIN_NAME_LENGTH || values.groupName.length > MAX_NAME_LENGTH)
            errors.groupName = `${GROUP_FORM_STRINGS.ERROR_NAME_LENGTH} ${MIN_NAME_LENGTH}-${MAX_NAME_LENGTH}`;

        return errors;
    };

    const getGroupDataToSend = (group: IGroupFormData, isUpdate: boolean) => ({
        group: {
            group_name: group.groupName,
            group_description: group.groupDescription,
            category: group.category,
            visibility: group.visibility,
        },
        invitedAccountIds: isUpdate ? false : [],
        participatingAccountIds: isUpdate ? false : [],
    });

    const sendCreateGroup = (group: IGroupFormData) => {
        const isUpdate = !!groupForEditing;
        axios.defaults.baseURL = AXIOS_BASE_URL;
        axios({
            method: isUpdate ? "PUT" : "POST",
            url: groupForEditing
                ? `${AXIOS_PUT_GROUP_URL}/${groupForEditing.group_id}`
                : AXIOS_CREATE_GROUP_URL,
            data: getGroupDataToSend(group, isUpdate),
        })
            .then(() => {
                setSnackbarText(
                    isUpdate
                        ? GROUP_FORM_STRINGS.SNACKBAR_SUCCESS_SAVE
                        : GROUP_FORM_STRINGS.SNACKBAR_SUCCESS_CREATE,
                );
                setRefreshGroupList(true);
                formik.resetForm();
                closeModal();
            })
            .catch(() => setSnackbarText(GROUP_FORM_STRINGS.SNACKBAR_ERROR_SAVING_GROUP));
    };

    const getInitialValues = (): IGroupFormData =>
        groupForEditing
            ? {
                  groupName: groupForEditing.group_name || "",
                  groupDescription: groupForEditing.group_description || "",
                  visibility: groupForEditing.visibility,
                  category: groupForEditing.category,
              }
            : {
                  groupName: "",
                  groupDescription: "",
                  visibility: VISIBILITY_PRIVATE,
                  category: CATEGORY_OPTIONS[0],
              };

    const formik = useFormik<IGroupFormData>({
        initialValues: getInitialValues(),
        onSubmit: (values, { resetForm }) => {
            sendCreateGroup(values);
            resetForm();
            closeModal();
        },
        validate: validateForm,
        enableReinitialize: true,
    });

    const handleCancel = () => {
        formik.resetForm();
        closeModal();
    };

    const handleDeleteOKClick = (group: IGroup) => {
        setGroupForDeletionDialog(null);
        closeModal();
        axios.defaults.baseURL = AXIOS_BASE_URL;
        axios
            .delete<IGroup>(`${AXIOS_DELETE_GROUP_URL}/${group.group_id}`)
            .then(() => {
                setRefreshGroupList(true);
                setSnackbarText(GROUP_FORM_STRINGS.SNACKBAR_DELETED);
            })
            .catch(() => {
                setSnackbarText(GROUP_FORM_STRINGS.SNACKBAR_ERROR_DELETE);
            });
    };

    const handleDelete = () =>
        !groupForEditing
            ? setSnackbarText(GROUP_FORM_STRINGS.SNACKBAR_ERROR_NO_GROUP_TO_DELETE)
            : groupForEditing.owner?.account_id !== loginAccountId
            ? setSnackbarText(GROUP_FORM_STRINGS.SNACKBAR_ERROR_NOT_YOURS)
            : Array.isArray(groupForEditing.participants) && groupForEditing.participants.length > 0
            ? setSnackbarText(GROUP_FORM_STRINGS.SNACKBAR_ERROR_DELETE_HAS_PARTICIPANTS)
            : setGroupForDeletionDialog(groupForEditing);

    const handleResetClick = () => {
        formik.resetForm();
    };

    const handleDialogClose = (event: unknown, reason: string) => {
        if (reason !== "backdropClick") handleCancel();
    };

    return (
        <>
            <GroupDeleteDialog
                groupForDeletion={groupForDeletionDialog}
                hide={() => setGroupForDeletionDialog(null)}
                handleDeleteOKClick={handleDeleteOKClick}
            />
            <Dialog
                open={open}
                onClose={handleDialogClose}
                disableEscapeKeyDown
                classes={{ scrollPaper: classes.scrollPaper }}
                fullWidth
            >
                <form onSubmit={formik.handleSubmit}>
                    <DialogTitle>
                        {groupForEditing
                            ? GROUP_FORM_STRINGS.TITLE_EDIT
                            : GROUP_FORM_STRINGS.TITLE_CREATE}
                    </DialogTitle>
                    <DialogContent dividers className={classes.dialogContent}>
                        <GroupFormControls formik={formik} />
                    </DialogContent>

                    <DialogActions>
                        <Button type="submit" variant="contained" color="primary">
                            {groupForEditing ? GROUP_FORM_STRINGS.SAVE : GROUP_FORM_STRINGS.CREATE}
                        </Button>
                        <Button variant="contained" onClick={handleCancel}>
                            {GROUP_FORM_STRINGS.CANCEL}
                        </Button>
                        <Button variant="contained" onClick={handleResetClick}>
                            {GROUP_FORM_STRINGS.RESET}
                        </Button>
                        {groupForEditing && (
                            <Button
                                variant="contained"
                                startIcon={<DeleteIcon />}
                                onClick={handleDelete}
                            >
                                {GROUP_FORM_STRINGS.DELETE}
                            </Button>
                        )}
                    </DialogActions>
                </form>
            </Dialog>
        </>
    );
};
export default GroupForm;
