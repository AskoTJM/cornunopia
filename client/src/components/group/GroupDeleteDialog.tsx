import React from "react";
import {
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    DialogContentText,
    Button,
} from "@material-ui/core";
import { IGroup } from "../../types";

interface IGroupDeleteDialog {
    groupForDeletion: IGroup | null;
    hide: () => void;
    handleDeleteOKClick: (group: IGroup) => void;
}

enum GROUP_DELETE_DIALOG_STRINGS {
    TITLE = "Delete Group",
    CONFIRMATION = "Do you want to delete group ",
    BUTTON_DELETE = "Delete",
    BUTTON_CANCEL = "Cancel",
}

const GroupDeleteDialog: React.FC<IGroupDeleteDialog> = (props) => {
    const { groupForDeletion, hide, handleDeleteOKClick } = props;

    return (
        <Dialog open={!!groupForDeletion} onClose={hide} disableBackdropClick>
            <DialogTitle>{GROUP_DELETE_DIALOG_STRINGS.TITLE}</DialogTitle>
            <DialogContent>
                <DialogContentText>
                    {`${GROUP_DELETE_DIALOG_STRINGS.CONFIRMATION} '${
                        groupForDeletion?.group_name as string
                    }'?`}
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button
                    onClick={() => groupForDeletion && handleDeleteOKClick(groupForDeletion)}
                    color="primary"
                >
                    {GROUP_DELETE_DIALOG_STRINGS.BUTTON_DELETE}
                </Button>
                <Button onClick={hide} color="primary">
                    {GROUP_DELETE_DIALOG_STRINGS.BUTTON_CANCEL}
                </Button>
            </DialogActions>
        </Dialog>
    );
};

export default GroupDeleteDialog;
