import { Container } from "@material-ui/core";
import axios from "axios";
import React, { useContext, useState } from "react";
import { useEffect } from "react";
import {
    AXIOS_BASE_URL,
    AXIOS_CREATE_GROUP_URL,
    AXIOS_GROUP_ACCOUNT_URL,
} from "../../constants/constants";
import SnackbarContext from "../../contexts/SnackbarContext";
import { IGroup } from "../../types";
import GroupEventDialog from "./GroupEventDialog";
import GroupForm from "./GroupForm";
import GroupListItem from "./GroupListItem";
import GroupPeople from "./GroupPeople";

enum GROUP_LIST_STRINGS {
    ERROR_NO_GROUPS = "No groups found.",
    ERROR_CANT_GET_GROUPS = "Error: Can't Get Groups.",
    PARTICIPATION_ADDED = "Participation added",
    ERROR_CANT_PARTICIPATE = "Participation failed",
}

interface IGroupList {
    refreshList: boolean;
    setRefreshGroupList: React.Dispatch<React.SetStateAction<boolean>>;
}

const GroupList: React.FC<IGroupList> = (props) => {
    const { refreshList, setRefreshGroupList } = props;

    const [groups, setGroups] = useState<IGroup[]>([]);
    const [errorStatus, setErrorStatus] = useState(false);
    const [groupForEditing, setGroupForEditing] = useState<IGroup | null>(null);
    const [groupForEditingPeople, setGroupForEditingPeople] = useState<IGroup | null>(null);
    const [expandedItem, setExpandedItem] = useState<string | false>(false);
    const [groupForCreateEventDialog, setGroupForCreateEventDialog] = useState<IGroup | null>(null);
    const { setSnackbarText } = useContext(SnackbarContext);

    useEffect(() => {
        getGroups();
        setRefreshGroupList(false);
    }, [refreshList]);

    const getGroups = () => {
        axios.defaults.baseURL = AXIOS_BASE_URL;
        axios
            .get<IGroup[]>(AXIOS_CREATE_GROUP_URL)
            .then((response) => {
                setGroups(response.data);
                setErrorStatus(false);
            })
            .catch(() => {
                setErrorStatus(true);
                setGroups([]);
            });
    };

    const participateInPublicGroup = (group: IGroup) => {
        axios.defaults.baseURL = AXIOS_BASE_URL;
        axios
            .post<IGroup>(`${AXIOS_GROUP_ACCOUNT_URL}/participate/${group.group_id}`)
            .then(() => {
                setSnackbarText(GROUP_LIST_STRINGS.PARTICIPATION_ADDED);
                setRefreshGroupList(true);
            })
            .catch(() => {
                setSnackbarText(GROUP_LIST_STRINGS.ERROR_CANT_PARTICIPATE);
            });
    };

    return (
        <>
            <GroupForm
                open={!!groupForEditing}
                closeModal={() => setGroupForEditing(null)}
                groupForEditing={groupForEditing}
                setRefreshGroupList={setRefreshGroupList}
            />
            <GroupPeople
                group={groupForEditingPeople}
                hide={() => setGroupForEditingPeople(null)}
                refreshData={() => setRefreshGroupList(true)}
            />
            <GroupEventDialog
                group={groupForCreateEventDialog}
                hide={() => setGroupForCreateEventDialog(null)}
            />
            <Container>
                {groups.length > 0
                    ? groups.map((group) => (
                          <GroupListItem
                              group={group}
                              key={String(group.group_id)}
                              openGroupForEditing={(group) => setGroupForEditing(group)}
                              expandedItem={expandedItem}
                              setExpandedItem={setExpandedItem}
                              participateInPublicGroup={participateInPublicGroup}
                              openGroupMembers={(group) => setGroupForEditingPeople(group)}
                              createEventForGroup={() => setGroupForCreateEventDialog(group)}
                          />
                      ))
                    : errorStatus
                    ? GROUP_LIST_STRINGS.ERROR_CANT_GET_GROUPS
                    : GROUP_LIST_STRINGS.ERROR_NO_GROUPS}
            </Container>
        </>
    );
};
export default GroupList;
