import { AppBar, Box, Button, Toolbar, makeStyles, createStyles } from "@material-ui/core";
import React, { useEffect } from "react";
import GroupForm from "./GroupForm";

enum GroupTaskbarStrings {
    BUTTON_CREATE = "Create group",
    BUTTON_SEARCH = "Search",
}

interface IGroupTaskbar {
    setRefreshGroupList: React.Dispatch<React.SetStateAction<boolean>>;
}

const useStyles = makeStyles((theme) =>
    createStyles({
        taskBar: {
            backgroundColor: theme.palette.primary.light,
        },
    }),
);

const GroupTaskbar: React.FC<IGroupTaskbar> = (props) => {
    const [open, setOpen] = React.useState(false);
    const { setRefreshGroupList } = props;
    const classes = useStyles();

    useEffect(() => {
        setRefreshGroupList(true);
    }, [open]);

    return (
        <>
            <AppBar position="static" className={classes.taskBar}>
                <Toolbar>
                    <Box m={2}>
                        <Button variant="contained" onClick={() => setRefreshGroupList(true)}>
                            {GroupTaskbarStrings.BUTTON_SEARCH}
                        </Button>
                    </Box>
                    <Box m={2}>
                        <Button variant="contained" onClick={() => setOpen(true)}>
                            {GroupTaskbarStrings.BUTTON_CREATE}
                        </Button>
                    </Box>
                    <GroupForm
                        open={open}
                        closeModal={() => setOpen(false)}
                        groupForEditing={null}
                        setRefreshGroupList={setRefreshGroupList}
                    />
                </Toolbar>
            </AppBar>
        </>
    );
};

export default GroupTaskbar;
