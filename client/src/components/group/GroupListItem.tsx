import React, { useContext } from "react";
import {
    Accordion,
    Typography,
    makeStyles,
    createStyles,
    Theme,
    Button,
    AccordionSummary,
    AccordionDetails,
    AccordionActions,
    Divider,
} from "@material-ui/core";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import { IGroup } from "../../types";
import { isGroupOwner } from "../../utils";
import LoginContext from "../../contexts/LoginContext";
import { VISIBILITY_PUBLIC } from "../../constants/constants";
import GroupInfoTexts from "./GroupInfoTexts";

export interface IGroupListItem {
    group: IGroup;
    openGroupForEditing: (event: IGroup) => void;
    expandedItem: string | false;
    setExpandedItem: React.Dispatch<React.SetStateAction<string | false>>;
    participateInPublicGroup: (group: IGroup) => void;
    openGroupMembers: (group: IGroup) => void;
    createEventForGroup: () => void;
}

enum GROUP_LIST_ITEM_STRINGS {
    BUTTON_PARTICIPATE = "Participate",
    BUTTON_EDIT = "Edit",
    BUTTON_MEMBERS = "Members",
    BUTTON_EVENT_FOR_GROUP = "Create Event",
}

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        heading: {
            fontSize: theme.typography.pxToRem(15),
            fontWeight: "bold",
            flexBasis: "33.33%",
            flexShrink: 0,
        },
        details: {
            fontSize: theme.typography.pxToRem(15),
            color: theme.palette.text.secondary,
        },
    }),
);

const GroupListItem: React.FC<IGroupListItem> = (props) => {
    const classes = useStyles();
    const {
        group,
        openGroupForEditing,
        expandedItem,
        setExpandedItem,
        participateInPublicGroup,
        openGroupMembers,
        createEventForGroup,
    } = props;
    const { loginAccountId } = useContext(LoginContext);

    const groupId = String(group.group_id);
    const isItemExpanded = expandedItem === groupId;

    return (
        <Accordion
            expanded={isItemExpanded}
            onChange={(event, isExpanded) => setExpandedItem(isExpanded ? groupId : false)}
        >
            <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                <Typography className={classes.heading}>{group.group_name}</Typography>
                <Typography className={classes.details}>{group.group_description}</Typography>
            </AccordionSummary>

            <AccordionDetails>
                <GroupInfoTexts group={group} />
            </AccordionDetails>

            <Divider />

            <AccordionActions>
                {group.visibility === VISIBILITY_PUBLIC && (
                    <Button
                        size="small"
                        color="primary"
                        onClick={() => participateInPublicGroup(group)}
                    >
                        {GROUP_LIST_ITEM_STRINGS.BUTTON_PARTICIPATE}
                    </Button>
                )}
                {isGroupOwner(group, loginAccountId as number) && (
                    <>
                        <Button
                            size="small"
                            color="primary"
                            onClick={() => openGroupMembers(group)}
                        >
                            {GROUP_LIST_ITEM_STRINGS.BUTTON_MEMBERS}
                        </Button>
                        <Button
                            size="small"
                            color="primary"
                            onClick={() => openGroupForEditing(group)}
                        >
                            {GROUP_LIST_ITEM_STRINGS.BUTTON_EDIT}
                        </Button>
                        <Button size="small" color="primary" onClick={createEventForGroup}>
                            {GROUP_LIST_ITEM_STRINGS.BUTTON_EVENT_FOR_GROUP}
                        </Button>
                    </>
                )}
            </AccordionActions>
        </Accordion>
    );
};

export default GroupListItem;
