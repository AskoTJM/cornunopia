import React, { useContext } from "react";
import {
    Checkbox,
    createStyles,
    FormControlLabel,
    List,
    ListItem,
    ListItemIcon,
    ListItemText,
    makeStyles,
    Typography,
} from "@material-ui/core";
import { IAccount } from "../../types";
import LoginContext from "../../contexts/LoginContext";

interface IGroupMembersList {
    members: IAccount[];
    membersToRemove: number[];
    setMembersToRemove: React.Dispatch<React.SetStateAction<number[]>>;
    membersLoadError: boolean;
}

const useStyles = makeStyles((theme) =>
    createStyles({
        tabs: {
            marginBottom: "0.70em",
        },
        listItem: {
            marginBottom: "3px",
            borderRadius: theme.shape.borderRadius,
            backgroundColor: "#f8f8f8",
        },
        scrollPaper: {
            alignItems: "baseline",
        },
        dialogContent: {
            paddingTop: "0.25em",
        },
        invitesLabel: {
            margin: theme.spacing(0, 0, 2, 0),
        },
    }),
);

enum GROUP_MEMBERS_LIST_STRINGS {
    NO_MEMBERS = "Group has no members",
    GROUP_OWNER = "Owner",
    LABEL_REMOVE = "Remove",
    ERROR_GETTING_MEMBERS = "Error, can't get members",
    OWNER_TOOLTIP = "Owner of the group. Can't be removed.",
}

const GroupMembersList: React.FC<IGroupMembersList> = (props) => {
    const { members, membersToRemove, setMembersToRemove, membersLoadError } = props;
    const classes = useStyles();
    const { loginAccountId } = useContext(LoginContext);

    const handleCheckboxChange = (member: IAccount, isChecked: boolean) => {
        const memberId = member.account_id;
        return isChecked
            ? setMembersToRemove((prev) => [...prev, memberId])
            : setMembersToRemove((prev) => prev.filter((id) => id !== memberId));
    };

    return (
        <>
            <List dense>
                {members.length > 0 ? (
                    members.map((account) => {
                        const checked = membersToRemove.includes(account.account_id);
                        return (
                            <ListItem key={account.account_id} className={classes.listItem}>
                                <ListItemText primary={account.name} secondary={account.email} />
                                <ListItemIcon>
                                    {loginAccountId === account.account_id ? (
                                        <span title={GROUP_MEMBERS_LIST_STRINGS.OWNER_TOOLTIP}>
                                            {GROUP_MEMBERS_LIST_STRINGS.GROUP_OWNER}
                                        </span>
                                    ) : (
                                        <FormControlLabel
                                            control={
                                                <Checkbox
                                                    checked={checked}
                                                    onChange={(event) =>
                                                        handleCheckboxChange(
                                                            account,
                                                            event.target.checked,
                                                        )
                                                    }
                                                />
                                            }
                                            label={GROUP_MEMBERS_LIST_STRINGS.LABEL_REMOVE}
                                        />
                                    )}
                                </ListItemIcon>
                            </ListItem>
                        );
                    })
                ) : (
                    <Typography variant="body2">{GROUP_MEMBERS_LIST_STRINGS.NO_MEMBERS}</Typography>
                )}
            </List>
            {membersLoadError && (
                <Typography variant="body2">
                    {GROUP_MEMBERS_LIST_STRINGS.ERROR_GETTING_MEMBERS}
                </Typography>
            )}
        </>
    );
};

export default GroupMembersList;
