import React, { useState } from "react";
import GroupList from "./GroupList";
import GroupTaskbar from "./GroupTaskbar";

const GroupView: React.FC = () => {
    const [refreshList, setRefreshGroupList] = useState(true);

    return (
        <>
            <GroupTaskbar setRefreshGroupList={setRefreshGroupList} />
            <br />
            <GroupList refreshList={refreshList} setRefreshGroupList={setRefreshGroupList} />
        </>
    );
};

export default GroupView;
