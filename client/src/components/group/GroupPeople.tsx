import React, { useState, useEffect, useContext } from "react";
import {
    Button,
    createStyles,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    makeStyles,
    Tab,
    Tabs,
    Theme,
    Typography,
} from "@material-ui/core";
import { IAccount, IGroup, IGroupAccount } from "../../types";
import axios from "axios";
import {
    AXIOS_BASE_URL,
    AXIOS_GROUP_URL,
    AXIOS_PUT_GROUP_URL,
    VISIBILITY_PRIVATE,
} from "../../constants/constants";
import { sortAccountsByName } from "../../utils";
import SnackbarContext from "../../contexts/SnackbarContext";
import Invites from "../util/Invites";
import GroupMembersList from "./GroupMembersList";

interface IGroupPeople {
    group: IGroup | null;
    hide: () => void;
    refreshData: () => void;
}

enum TAB_VALUES {
    MEMBERS = "Members",
    INVITES = "Invites",
}

enum GROUP_PEOPLE_STRINGS {
    DIALOG_TITLE = "Members and Invites of Group",
    BUTTON_SAVE = "Save",
    BUTTON_RESET = "Reset",
    BUTTON_CANCEL = "Cancel",
    LABEL_ADD_INVITES = "Invite people to join the group",
    SAVE_OK = "Members and invites saved",
    SAVE_ERROR = "Failed to save members and invites",
}

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        tabs: {
            marginBottom: "0.70em",
        },
        scrollPaper: {
            alignItems: "baseline",
        },
        dialogContent: {
            paddingTop: "0.25em",
        },
        invitesLabel: {
            margin: theme.spacing(0, 0, 2, 0),
        },
    }),
);

const GroupPeople: React.FC<IGroupPeople> = (props) => {
    const { group, hide, refreshData } = props;
    const classes = useStyles();
    const [members, setMembers] = useState<IAccount[]>([]);
    const [membersToRemove, setMembersToRemove] = useState<number[]>([]);
    const [membersLoadError, setMembersLoadError] = useState(false);
    const [invites, setInvites] = useState<IAccount[]>([]);
    const [tabValue, setTabValue] = useState<string>(TAB_VALUES.MEMBERS);
    const [originalInvites, setOriginalInvites] = useState<IAccount[]>([]);

    const { setSnackbarText } = useContext(SnackbarContext);

    const loadInvites = () => {
        const invitedAccounts = group
            ? group.participants
                  .filter((participant) => participant.invite && !participant.participating)
                  .map((participant) => participant.account)
            : [];
        setInvites(invitedAccounts);
        setOriginalInvites(invitedAccounts);
    };

    const loadMembers = (group: IGroup) => {
        axios.defaults.baseURL = AXIOS_BASE_URL;
        axios
            .get<IGroupAccount[]>(`${AXIOS_GROUP_URL}/${group.group_id}/participants`)
            .then((response) => {
                const memberAccounts = response.data.map((groupAccount) => groupAccount.account);
                setMembers(sortAccountsByName(memberAccounts));
                setMembersLoadError(false);
            })
            .catch(() => {
                setMembers([]);
                setMembersLoadError(true);
            });
    };

    useEffect(() => {
        setMembersToRemove([]);
        if (group) {
            setTabValue(TAB_VALUES.MEMBERS);
            loadMembers(group);
            loadInvites();
        }
    }, [group]);

    const handleDialogClose = (event: unknown, reason: string) => {
        if (reason !== "backdropClick") hide();
    };

    const saveGroupMembers = (group: IGroup) => {
        const participatingAccountIds = members
            .filter((account) => !membersToRemove.includes(account.account_id))
            .map((account) => account.account_id);
        const invitedAccountIds =
            group.visibility === VISIBILITY_PRIVATE
                ? invites.map((account) => account.account_id)
                : [];

        axios.defaults.baseURL = AXIOS_BASE_URL;
        axios
            .put<IGroup>(`${AXIOS_PUT_GROUP_URL}/${group.group_id}`, {
                group,
                invitedAccountIds,
                participatingAccountIds,
            })
            .then(() => {
                setSnackbarText(GROUP_PEOPLE_STRINGS.SAVE_OK);
                refreshData();
                hide();
            })
            .catch(() => setSnackbarText(GROUP_PEOPLE_STRINGS.SAVE_ERROR));
    };

    const handleSaveClick = () => {
        if (group) saveGroupMembers(group);
    };

    const handleResetClick = () => {
        if (group) {
            setMembersToRemove([]);
            loadMembers(group);
            loadInvites();
        }
    };

    return (
        <Dialog
            open={!!group}
            onClose={handleDialogClose}
            disableEscapeKeyDown
            classes={{ scrollPaper: classes.scrollPaper }}
            fullWidth
        >
            <DialogTitle>
                {GROUP_PEOPLE_STRINGS.DIALOG_TITLE} &apos;{group?.group_name}&apos;
            </DialogTitle>

            <DialogContent dividers className={classes.dialogContent}>
                <Tabs
                    value={tabValue}
                    onChange={(event, newValue) => setTabValue(newValue)}
                    className={classes.tabs}
                >
                    <Tab label={TAB_VALUES.MEMBERS} value={TAB_VALUES.MEMBERS} />
                    <Tab
                        label={TAB_VALUES.INVITES}
                        value={TAB_VALUES.INVITES}
                        disabled={group?.visibility !== VISIBILITY_PRIVATE}
                    />
                </Tabs>

                {tabValue === TAB_VALUES.MEMBERS && (
                    <GroupMembersList
                        members={members}
                        membersToRemove={membersToRemove}
                        setMembersToRemove={setMembersToRemove}
                        membersLoadError={membersLoadError}
                    />
                )}
                {tabValue === TAB_VALUES.INVITES && (
                    <>
                        <Typography variant="body2" className={classes.invitesLabel}>
                            {GROUP_PEOPLE_STRINGS.LABEL_ADD_INVITES}
                        </Typography>
                        <Invites
                            originalInvites={originalInvites}
                            invites={invites}
                            setInvites={setInvites}
                        />
                    </>
                )}
            </DialogContent>

            <DialogActions>
                <Button variant="contained" color="primary" onClick={handleSaveClick}>
                    {GROUP_PEOPLE_STRINGS.BUTTON_SAVE}
                </Button>
                <Button variant="contained" onClick={handleResetClick}>
                    {GROUP_PEOPLE_STRINGS.BUTTON_RESET}
                </Button>
                <Button variant="contained" onClick={hide}>
                    {GROUP_PEOPLE_STRINGS.BUTTON_CANCEL}
                </Button>
            </DialogActions>
        </Dialog>
    );
};

export default GroupPeople;
