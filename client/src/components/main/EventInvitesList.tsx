import React, { useState, useContext, useEffect } from "react";
import axios from "axios";
import { AXIOS_BASE_URL, AXIOS_EVENT_ACCOUNT_URL, AXIOS_USER_URL } from "../../constants/constants";
import { IEvent } from "../../types";
import LoginContext from "../../contexts/LoginContext";
import InvitesListItem from "./InvitesListItem";
import { Container } from "@material-ui/core";
import { fixEventsData } from "../../utils";
import EventDetails from "../event/EventDetails";
import SnackbarContext from "../../contexts/SnackbarContext";
import { getDateStringWithDistanceToNow } from "../../utils";
import EventInfoTexts from "../event/EventInfoTexts";

interface IInvitesList {
    setRefreshParticipationsList: React.Dispatch<React.SetStateAction<boolean>>;
}

enum INVITES_LIST_STRINGS {
    PENDING_INVITES = "Pending Event Invites",
    INVITE_ACCEPTED = "Invite accepted, participation added",
    INVITE_DECLINED = "Answered 'no thanks' to invite",
    INVITE_ERROR = "Failed to handle invite",
    NO_INVITES = "No Invites",
}

const InvitesList: React.FC<IInvitesList> = (props) => {
    const { setRefreshParticipationsList } = props;

    const [pendingInvites, setPendingInvites] = useState<IEvent[]>([]);
    const [expandedItem, setExpandedItem] = useState<string | false>(false);
    const [eventForDetails, setEventForDetails] = useState<IEvent | null>(null);
    const [refreshInvitesList, setRefreshInvitesList] = useState(true);

    const userAccountId = useContext(LoginContext).loginAccountId as number;
    const { setSnackbarText } = useContext(SnackbarContext);

    const getPendingInvites = () => {
        axios.defaults.baseURL = AXIOS_BASE_URL;
        axios
            .get<IEvent[]>(`${AXIOS_USER_URL}/${userAccountId}/pending_event_invites`)
            .then((response) => {
                setPendingInvites(fixEventsData(response.data));
            })
            .catch(() => {
                setPendingInvites([]);
            });
    };

    useEffect(() => {
        if (refreshInvitesList) {
            getPendingInvites();
            setRefreshInvitesList(false);
        }
    }, [refreshInvitesList]);

    const acceptInvite = (event: IEvent) => {
        axios.defaults.baseURL = AXIOS_BASE_URL;
        axios
            .post<IEvent>(`${AXIOS_EVENT_ACCOUNT_URL}/participate/${event.event_id}`)
            .then(() => {
                setSnackbarText(INVITES_LIST_STRINGS.INVITE_ACCEPTED);
                setRefreshInvitesList(true);
                setRefreshParticipationsList(true);
            })
            .catch(() => {
                setSnackbarText(INVITES_LIST_STRINGS.INVITE_ERROR);
            });
    };

    const declineInvite = (event: IEvent) => {
        axios.defaults.baseURL = AXIOS_BASE_URL;
        axios
            .delete<IEvent>(`${AXIOS_EVENT_ACCOUNT_URL}/decline_invite/${event.event_id}`)
            .then(() => {
                setSnackbarText(INVITES_LIST_STRINGS.INVITE_DECLINED);
                setRefreshInvitesList(true);
            })
            .catch(() => {
                setSnackbarText(INVITES_LIST_STRINGS.INVITE_ERROR);
            });
    };

    const getEventSecondaryText = (event: IEvent) =>
        `${event.location || ""}${event.location ? ", " : ""}${
            event.start_timedate instanceof Date
                ? getDateStringWithDistanceToNow(event.start_timedate)
                : ""
        }`;

    return (
        <>
            <EventDetails
                event={eventForDetails}
                hide={() => setEventForDetails(null)}
                openEventForEditing={null}
            />
            <Container>
                <h2>{INVITES_LIST_STRINGS.PENDING_INVITES}</h2>
                {pendingInvites.length > 0
                    ? pendingInvites.map((event) => (
                          <InvitesListItem
                              key={event.event_id}
                              itemId={event.event_id}
                              name={event.name}
                              secondaryText={getEventSecondaryText(event)}
                              expandedItem={expandedItem}
                              setExpandedItem={setExpandedItem}
                              openDetails={() => setEventForDetails(event)}
                              acceptInvite={() => acceptInvite(event)}
                              declineInvite={() => declineInvite(event)}
                              info={<EventInfoTexts event={event} onClick={setEventForDetails} />}
                          />
                      ))
                    : INVITES_LIST_STRINGS.NO_INVITES}
            </Container>
        </>
    );
};

export default InvitesList;
