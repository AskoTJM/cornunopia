import React from "react";
import {
    Accordion,
    AccordionActions,
    AccordionDetails,
    AccordionSummary,
    Button,
    createStyles,
    makeStyles,
    Theme,
    Typography,
} from "@material-ui/core";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import CheckCircleIcon from "@material-ui/icons/CheckCircle";
import CancelIcon from "@material-ui/icons/Cancel";

interface IInvitesListItem {
    itemId: number;
    name: string;
    secondaryText: string;
    expandedItem: string | false;
    setExpandedItem: React.Dispatch<React.SetStateAction<string | false>>;
    openDetails?: () => void;
    acceptInvite: () => void;
    declineInvite: () => void;
    info?: React.ReactNode;
}

enum INVITES_LIST_ITEM_STRINGS {
    BUTTON_ACCEPT = "Accept",
    BUTTON_DECLINE = "No thanks",
    BUTTON_DETAILS = "Details",
}

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        heading: {
            fontSize: theme.typography.pxToRem(15),
            fontWeight: "bold",
            flexBasis: "33.33%",
            flexShrink: 0,
        },
        details: {
            fontSize: theme.typography.pxToRem(15),
            color: theme.palette.text.secondary,
        },
        detailsClickable: {
            cursor: "pointer",
        },
        fieldValue: {
            color: theme.palette.text.secondary,
            margin: theme.spacing(0, 0, 0, 2.5),
        },
    }),
);

const InvitesListItem: React.FC<IInvitesListItem> = (props) => {
    const {
        itemId,
        name,
        expandedItem,
        setExpandedItem,
        openDetails,
        acceptInvite,
        declineInvite,
        info,
        secondaryText,
    } = props;
    const classes = useStyles();

    const itemIdString = String(itemId);
    const isItemExpanded = expandedItem === itemIdString;

    return (
        <Accordion
            expanded={isItemExpanded}
            onChange={(event, isExpanded) => setExpandedItem(isExpanded ? itemIdString : false)}
        >
            <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                <Typography className={classes.heading}>{name}</Typography>
                <Typography className={classes.details}>{secondaryText}</Typography>
            </AccordionSummary>
            <AccordionDetails>{info}</AccordionDetails>
            <AccordionActions>
                <Button
                    size="small"
                    color="primary"
                    startIcon={<CheckCircleIcon />}
                    onClick={acceptInvite}
                >
                    {INVITES_LIST_ITEM_STRINGS.BUTTON_ACCEPT}
                </Button>
                <Button
                    size="small"
                    color="primary"
                    startIcon={<CancelIcon />}
                    onClick={declineInvite}
                >
                    {INVITES_LIST_ITEM_STRINGS.BUTTON_DECLINE}
                </Button>
                {openDetails && (
                    <Button size="small" color="primary" onClick={openDetails}>
                        {INVITES_LIST_ITEM_STRINGS.BUTTON_DETAILS}
                    </Button>
                )}
            </AccordionActions>
        </Accordion>
    );
};

export default InvitesListItem;
