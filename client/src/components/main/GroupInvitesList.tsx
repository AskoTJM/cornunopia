import React, { useState, useContext, useEffect } from "react";
import axios from "axios";
import { AXIOS_BASE_URL, AXIOS_GROUP_ACCOUNT_URL, AXIOS_USER_URL } from "../../constants/constants";
import { IGroup } from "../../types";
import LoginContext from "../../contexts/LoginContext";
import InvitesListItem from "./InvitesListItem";
import { Container } from "@material-ui/core";
import SnackbarContext from "../../contexts/SnackbarContext";
import GroupInfoTexts from "../group/GroupInfoTexts";

interface IGroupInvitesList {
    setRefreshParticipationsList: React.Dispatch<React.SetStateAction<boolean>>;
}

enum GROUP_INVITES_LIST_STRINGS {
    PENDING_INVITES = "Pending Group Invites",
    INVITE_ACCEPTED = "Invite accepted, participation added",
    INVITE_DECLINED = "Answered 'no thanks' to invite",
    INVITE_ERROR = "Failed to handle invite",
    NO_INVITES = "No Invites",
}

const GroupInvitesList: React.FC<IGroupInvitesList> = (props) => {
    const { setRefreshParticipationsList } = props;

    const [pendingInvites, setPendingInvites] = useState<IGroup[]>([]);
    const [expandedItem, setExpandedItem] = useState<string | false>(false);
    const [refreshInvitesList, setRefreshInvitesList] = useState(true);

    const userAccountId = useContext(LoginContext).loginAccountId as number;
    const { setSnackbarText } = useContext(SnackbarContext);

    const getPendingInvites = () => {
        axios.defaults.baseURL = AXIOS_BASE_URL;
        axios
            .get<IGroup[]>(`${AXIOS_USER_URL}/${userAccountId}/pending_group_invites`)
            .then((response) => setPendingInvites(response.data))
            .catch(() => setPendingInvites([]));
    };

    useEffect(() => {
        if (refreshInvitesList) {
            getPendingInvites();
            setRefreshInvitesList(false);
        }
    }, [refreshInvitesList]);

    const acceptInvite = (group: IGroup) => {
        axios.defaults.baseURL = AXIOS_BASE_URL;
        axios
            .post<IGroup>(`${AXIOS_GROUP_ACCOUNT_URL}/participate/${group.group_id}`)
            .then(() => {
                setSnackbarText(GROUP_INVITES_LIST_STRINGS.INVITE_ACCEPTED);
                setRefreshInvitesList(true);
                setRefreshParticipationsList(true);
            })
            .catch(() => setSnackbarText(GROUP_INVITES_LIST_STRINGS.INVITE_ERROR));
    };

    const declineInvite = (group: IGroup) => {
        axios.defaults.baseURL = AXIOS_BASE_URL;
        axios
            .delete<IGroup>(`${AXIOS_GROUP_ACCOUNT_URL}/decline_invite/${group.group_id}`)
            .then(() => {
                setSnackbarText(GROUP_INVITES_LIST_STRINGS.INVITE_DECLINED);
                setRefreshInvitesList(true);
            })
            .catch(() => {
                setSnackbarText(GROUP_INVITES_LIST_STRINGS.INVITE_ERROR);
            });
    };

    return (
        <>
            <Container>
                <h2>{GROUP_INVITES_LIST_STRINGS.PENDING_INVITES}</h2>
                {pendingInvites.length > 0
                    ? pendingInvites.map((group) => (
                          <InvitesListItem
                              key={group.group_id}
                              itemId={group.group_id}
                              name={group.group_name}
                              secondaryText={group.group_description || ""}
                              expandedItem={expandedItem}
                              setExpandedItem={setExpandedItem}
                              acceptInvite={() => acceptInvite(group)}
                              declineInvite={() => declineInvite(group)}
                              info={<GroupInfoTexts group={group} />}
                          />
                      ))
                    : GROUP_INVITES_LIST_STRINGS.NO_INVITES}
            </Container>
        </>
    );
};

export default GroupInvitesList;
