import React, { useState } from "react";
import EventList from "./../event/EventList";
import EventInvitesList from "./EventInvitesList";
import GroupInvitesList from "./GroupInvitesList";

const MainPageView: React.FC = () => {
    const [refreshList, setRefreshEventList] = useState(true);

    return (
        <>
            <EventList
                refreshEventsList={refreshList}
                setRefreshEventsList={setRefreshEventList}
                isMainPage={true}
            />
            <EventInvitesList setRefreshParticipationsList={setRefreshEventList} />
            <GroupInvitesList setRefreshParticipationsList={setRefreshEventList} />
        </>
    );
};

export default MainPageView;
