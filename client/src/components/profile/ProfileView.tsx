import axios from "axios";
import React, { useState } from "react";
import { useEffect } from "react";
import { AXIOS_BASE_URL, AXIOS_USER_URL } from "../../constants/constants";
import { IAccount } from "../../types";
import LoginContext from "../../contexts/LoginContext";
import PersonalData from "./PersonalData";
import { useContext } from "react";
import SnackbarContext from "../../contexts/SnackbarContext";

enum PROFILE_VIEW_STRINGS {
    ERROR_GETTING_ACCOUNT = "Failed to get accout data",
}

const ProfileView: React.FC = () => {
    const [account, setAccount] = useState<IAccount | undefined>();
    const accountToGet = useContext(LoginContext).loginAccountId as number;
    const { setSnackbarText } = useContext(SnackbarContext);

    useEffect(() => {
        getAccount();
    }, [accountToGet]);

    const getAccount = () => {
        axios.defaults.baseURL = AXIOS_BASE_URL;
        axios
            .get<IAccount>(`${AXIOS_USER_URL}/${accountToGet}`)
            .then((response) => {
                setAccount(response.data);
            })
            .catch(() => setSnackbarText(PROFILE_VIEW_STRINGS.ERROR_GETTING_ACCOUNT));
    };
    return (
        <>
            <PersonalData accountData={account} />
        </>
    );
};
export default ProfileView;
