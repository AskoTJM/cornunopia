import {
    Box,
    Button,
    Card,
    CardActions,
    CardContent,
    CardHeader,
    CardMedia,
    Container,
    createStyles,
    Grid,
    Typography,
} from "@material-ui/core";
import React from "react";
import { IAccount } from "../../types";
import { makeStyles } from "@material-ui/core/styles";
import DefaultProfileImage from "../../resources/defaultProfileImage.png";
import { useHistory } from "react-router";
import { getDateStringWithoutTimeFromAny } from "../../utils";

const placeHolderProfilePicture = DefaultProfileImage as string;

interface IPersonalData {
    accountData: IAccount | undefined;
}

enum PERSONAL_DATA_STRINGS {
    HEADER = "Your Profile",
    NAME = "Name:",
    DESCRIPTION = "Description:",
    DATE_OF_BIRTH = "Date of Birth:",
    EMAIL = "Email:",
    GENDER = "Gender:",
    BUTTON_EDIT = "Edit Profile",
    BUTTON_CLOSE = "Close",
}

const useStyles = makeStyles((theme) =>
    createStyles({
        media: {
            height: 250,
            width: "33%",
            marginLeft: "33%",
            alignItems: "right",
        },
        fieldValue: {
            color: theme.palette.text.secondary,
            margin: theme.spacing(0, 0, 0, 2.5),
        },
        card: {
            marginTop: "1.5em",
            padding: theme.spacing(1),
        },
        cardHeader: {
            textAlign: "center",
        },
        cardActions: {
            textAlign: "right",
        },
    }),
);

const PersonalData: React.FC<IPersonalData> = (props) => {
    const { accountData } = props;

    const profileImage = accountData?.profile_image || placeHolderProfilePicture;
    const name = accountData?.name;
    const email = accountData?.email;
    const gender = accountData?.sex;
    const description = accountData?.description;
    const dateOfBirth = accountData?.date_of_birth;

    const classes = useStyles();
    const history = useHistory();

    const handleCloseClick = () => {
        history.goBack();
    };

    return (
        <Container maxWidth={"md"}>
            <Card className={classes.card} raised>
                <CardHeader title={PERSONAL_DATA_STRINGS.HEADER} className={classes.cardHeader} />
                <CardContent>
                    <Grid container>
                        <Grid item xs={6}>
                            <Box margin="2%">
                                <CardMedia
                                    className={classes.media}
                                    image={profileImage}
                                    title="Test"
                                />
                            </Box>
                        </Grid>
                        <Grid item xs={6}>
                            <Grid item>
                                <Typography variant="body1">
                                    {PERSONAL_DATA_STRINGS.NAME}
                                </Typography>
                            </Grid>
                            <Grid item>
                                <Typography variant="body1" className={classes.fieldValue}>
                                    {name}
                                </Typography>
                            </Grid>

                            <Grid item>
                                <Typography variant="body1">
                                    {PERSONAL_DATA_STRINGS.DESCRIPTION}
                                </Typography>
                            </Grid>
                            <Grid item>
                                <Typography variant="body1" className={classes.fieldValue}>
                                    {description}
                                </Typography>
                            </Grid>

                            <Grid item>
                                <Typography variant="body1">
                                    {PERSONAL_DATA_STRINGS.GENDER}
                                </Typography>
                            </Grid>
                            <Grid item>
                                <Typography variant="body1" className={classes.fieldValue}>
                                    {gender}
                                </Typography>
                            </Grid>

                            <Grid item>
                                <Typography variant="body1">
                                    {PERSONAL_DATA_STRINGS.EMAIL}
                                </Typography>
                            </Grid>
                            <Grid item>
                                <Typography variant="body1" className={classes.fieldValue}>
                                    {email}
                                </Typography>
                            </Grid>

                            <Grid item>
                                <Typography variant="body1">
                                    {PERSONAL_DATA_STRINGS.DATE_OF_BIRTH}
                                </Typography>
                            </Grid>
                            <Grid item>
                                <Typography variant="body1" className={classes.fieldValue}>
                                    {getDateStringWithoutTimeFromAny(dateOfBirth)}
                                </Typography>
                            </Grid>
                        </Grid>
                    </Grid>
                </CardContent>

                <CardActions className={classes.cardActions}>
                    <Button color="primary">{PERSONAL_DATA_STRINGS.BUTTON_EDIT}</Button>
                    <Button onClick={handleCloseClick}>{PERSONAL_DATA_STRINGS.BUTTON_CLOSE}</Button>
                </CardActions>
            </Card>
        </Container>
    );
};

export default PersonalData;
