export const AXIOS_BASE_URL = "http://localhost:5000";
export const AXIOS_CREATE_GROUP_URL = "/group";
export const AXIOS_PUT_GROUP_URL = "/group";
export const AXIOS_DELETE_GROUP_URL = "/group";
export const AXIOS_CREATE_EVENT_URL = "/event";
export const AXIOS_PUT_EVENT_URL = "/event";
export const AXIOS_DELETE_EVENT_URL = "/event";
export const AXIOS_LOGIN_URL = "/login";
export const AXIOS_USER_URL = "/user";
export const AXIOS_EVENT_ACCOUNT_URL = "/event_account";
export const AXIOS_EVENT_URL = "/event";
export const AXIOS_GROUP_ACCOUNT_URL = "/group_account";
export const AXIOS_GROUP_URL = "/group";
export const AXIOS_EVENT_GROUP_URL = "/event_group";

export const MAX_NAME_LENGTH = 100;
export const MIN_NAME_LENGTH = 3;

export const VISIBILITY_PUBLIC = "Public";
export const VISIBILITY_PRIVATE = "Private";
export const CATEGORY_OPTIONS = ["Other", "Sport", "Culture", "Shindings"];

export const TEXT_FOR_HIDDEN_SNACKBAR = "";

export enum REACT_ROUTES {
    MAIN = "/",
    LOGIN = "/login",
    PROFILE = "/profile",
    EVENT = "/event",
    GROUP = "/group",
    POST = "/post",
}

export enum HTTP_STATUS_CODES {
    UNAUTHORIZED = 401,
}

export const MIN_AGE_RANGE = 0;
export const MAX_AGE_RANGE = 100;
export const DEFAULT_AGE_RANGE = [MIN_AGE_RANGE, MAX_AGE_RANGE];
