import { format, formatDistanceToNow } from "date-fns";
import isPast from "date-fns/isPast";
import { IAccount, IEvent, IFixEventsData, IGroup } from "./types";
import axios, { AxiosRequestConfig } from "axios";
import {
    AXIOS_LOGIN_URL,
    HTTP_STATUS_CODES,
    REACT_ROUTES,
    VISIBILITY_PRIVATE,
    VISIBILITY_PUBLIC,
} from "./constants/constants";
import { History } from "history";

export const DATE_FORMAT = "MMM d, yyyy, kk:mm";
export const DATE_FORMAT_WITHOUT_TIME = "MMM d, yyyy";
const LOCAL_STORAGE_ACCESS_TOKEN_KEY = "token";
const AUTHORIZATION_HEADER_NAME = "Authorization";

export const getDateStringWithoutTime = (date: Date | undefined): string =>
    date ? format(date, DATE_FORMAT_WITHOUT_TIME) : "";

export const getDateStringWithoutTimeFromAny = (date: unknown): string =>
    date instanceof Date
        ? getDateStringWithoutTime(date)
        : typeof date === "string"
        ? getDateStringWithoutTime(new Date(date))
        : "";

export const getDateStringWithDistanceToNow = (date: Date | undefined): string =>
    date
        ? `${format(date, DATE_FORMAT)} (${formatDistanceToNow(date, {
              addSuffix: true,
          })})`
        : "";

export const getDateString = (date: Date | undefined): string =>
    date ? format(date, DATE_FORMAT) : "";

export const isEventInPast = (event: IEvent | null): boolean =>
    !!(event && isPast(event?.start_timedate) && event.end_timedate && isPast(event.end_timedate));

export const isDateInPast = (date: string | Date | null): boolean =>
    date instanceof Date ? isPast(date) : typeof date === "string" ? isPast(new Date(date)) : false;

export const isEventOwner = (event: IEvent | null, userId?: number): boolean =>
    !!(event && event.owner && event.owner.account_id === userId);

export const isGroupOwner = (group: IGroup | null, userId?: number): boolean =>
    !!(group && group.owner && group.owner.account_id === userId);

export const saveAccessToken = (token: string): void => {
    localStorage.setItem(LOCAL_STORAGE_ACCESS_TOKEN_KEY, token);
};

export const removeAccessToken = (): void => {
    localStorage.removeItem(LOCAL_STORAGE_ACCESS_TOKEN_KEY);
};

export const getAccessToken = (): string | null =>
    localStorage.getItem(LOCAL_STORAGE_ACCESS_TOKEN_KEY);

export const logoutUser = (history: History, clearLoginAccount: () => void): void => {
    removeAccessToken();
    clearLoginAccount();
    history.push(REACT_ROUTES.LOGIN);
};

const isTokenNeeded = (url: string | undefined) => url !== AXIOS_LOGIN_URL;

const addTokenToHeaders = (config: AxiosRequestConfig) => {
    if (isTokenNeeded(config.url)) config.headers[AUTHORIZATION_HEADER_NAME] = getAccessToken();
    return config;
};

export const setAxiosInterceptors = (history: History, clearLoginAccount: () => void): void => {
    axios.interceptors.request.use(
        (config) => addTokenToHeaders(config),
        (error) => Promise.reject(error),
    );
    axios.interceptors.response.use(undefined, (error) => {
        if (error.response.status === HTTP_STATUS_CODES.UNAUTHORIZED)
            logoutUser(history, clearLoginAccount);
        return Promise.reject(error);
    });
};

const VISIBILITY_TO_TEXT_MAP = {
    [VISIBILITY_PRIVATE]: "Only invited people",
    [VISIBILITY_PUBLIC]: "Anyone",
} as { [key: string]: string };

export const visibilityToUIText = (visibility?: string): string =>
    visibility ? VISIBILITY_TO_TEXT_MAP[visibility] : "";

export const getVisibilities = (): string[] => Object.keys(VISIBILITY_TO_TEXT_MAP);

export const fixEventsData = (events: IEvent[]): IFixEventsData[] =>
    events.map((event) => ({
        ...event,
        start_timedate: new Date(event.start_timedate),
        end_timedate: event.end_timedate ? new Date(event.end_timedate) : undefined,
    }));

export const sortAccountsByName = (accounts: IAccount[]): IAccount[] =>
    [...accounts].sort((account1, account2) =>
        account1.name.toLocaleLowerCase().localeCompare(account2.name.toLocaleLowerCase()),
    );
