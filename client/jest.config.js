module.exports = {
    preset: "ts-jest",
    roots: ["<rootDir>/src"],
    testRegex: "/__tests__/.*\\.test\\.tsx?$",
    moduleFileExtensions: ["ts", "tsx", "js", "jsx", "json", "node"],
    setupFilesAfterEnv: ["./src/__tests__/setUpTests.ts"],
    moduleNameMapper: {
        "^@$": "<rootDir>/src",
        "^@/(.*)\\.(?!jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$":
            "<rootDir>/src/$1",
        ".*\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$":
            "<rootDir>/src/__mocks__/fileMock.ts",
        ".*\\.(css|less)$": "identity-obj-proxy",
    }
};
